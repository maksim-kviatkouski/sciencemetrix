import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/5/13
 * Time: 12:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class ScholarClient {
    private HttpClient httpClient = new DefaultHttpClient();
    private HttpContext httpContext = new BasicHttpContext();
    private CookieStore cookieStore = new BasicCookieStore();
    private String scisig;
    private String inststart;
    private String inst;

    public void initSettings(){
        httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BEST_MATCH);
        httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
        try {
            //goes to main Scholar page to obtain initial cookies with response
            goToMain();
            //goes to settings page to get some more cookies and parse out some of parameters
            //required for saving settings
            goToSettings();
            //sends request to submit Scholar search settings
            submitSettings();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void  goToSettings() throws IOException {
        HttpResponse httpResponse;
        System.out.println();
        System.out.println("[INFO] Going to settings page...");
        HttpGet settingsPageGet = new HttpGet("http://scholar.google.com/scholar_settings");
        settingsPageGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.97 Safari/537.22");
        settingsPageGet.setHeader("Host","scholar.google.com");
        httpResponse = httpClient.execute(settingsPageGet, httpContext);
        String html = IOUtils.toString(httpResponse.getEntity().getContent());
        Document doc = NekoParser.getDocument(html);
        //System.out.println(html);
        scisig = NekoParser.queryText("//INPUT[@name='scisig']/@value", doc);
        inststart = NekoParser.queryText("//INPUT[@name='inststart']/@value", doc);
        inst = NekoParser.queryText("//INPUT[@name='inst']/@value", doc);
        System.out.println("scisig: " + scisig);
        System.out.println("inststart: " + inststart);
        System.out.println("inst: " + inst);
        settingsPageGet.releaseConnection();
    }

    private void submitSettings() throws IOException {
        HttpResponse httpResponse;
        System.out.println();
        System.out.println("[INFO] Trying to save settings...");
        //it seems that num param in the request below is for number of results per page
        //scisf is for BibTeX links
        //as_sdt=1%2C5 is for "articles only" setting
        HttpGet submitSettingsPageGet = new HttpGet("http://scholar.google.com/scholar_setprefs?scisig="+scisig+"&inststart="+inststart+"&as_sdt=1%2C5&num=20&scis=yes&scisf=4&hl=en&lang=all&instq=&inst="+inst+"&submit=");
        //set of headers below may not be a required minimum. it just emulates browser request as well as possible
        //I'm pretty sure some of those headers may not be set though their identification requires some more time
        submitSettingsPageGet.setHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        submitSettingsPageGet.setHeader("Accept-Charset","ISO-8859-1,utf-8;q=0.7,*;q=0.3");
        submitSettingsPageGet.setHeader("Accept-Encoding","gzip,deflate,sdch");
        submitSettingsPageGet.setHeader("Accept-Language","en-US,en;q=0.8");
        submitSettingsPageGet.setHeader("Connection","keep-alive");
        submitSettingsPageGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.97 Safari/537.22");
        submitSettingsPageGet.setHeader("Host","scholar.google.com");
        submitSettingsPageGet.setHeader("Referer","http://scholar.google.com/scholar_settings?hl=en&as_sdt=0,5");
        httpResponse = httpClient.execute(submitSettingsPageGet, httpContext);
    }

    private void goToMain() throws IOException {
        HttpGet mainPageGet = new HttpGet("http://scholar.google.com/");
        mainPageGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.97 Safari/537.22");
        System.out.println("[INFO] Going to main page to get initial cookies...");
        HttpResponse httpResponse = httpClient.execute(mainPageGet, httpContext);
        mainPageGet.releaseConnection();
    }

    /**
     * Method that emulates user's search request to Scholar and parses out results with links to PDFs
     * @param s
     * @throws IOException
     */
    public void executeQuery(String s) throws IOException {
        HttpGet queryPageGet = new HttpGet("http://scholar.google.com/scholar?hl=en&q=" + URLEncoder.encode(s,"UTF-8") + "&btnG=&as_sdt=1%2C5");
        queryPageGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.97 Safari/537.22");
        System.out.println();
        System.out.println("[INFO] Executing query...");
        httpClient = new DefaultHttpClient();
        HttpResponse httpResponse = httpClient.execute(queryPageGet, httpContext);
        String html = IOUtils.toString(httpResponse.getEntity().getContent());
        queryPageGet.releaseConnection();

        Document doc = NekoParser.getDocument(html);
        NodeList nodes = NekoParser.queryNodes(".//DIV[@class='gs_r'][.//SPAN/text()='[PDF]']", doc);

        System.out.println("[INFO] Total results with links to PDF found: " + nodes.getLength());
        System.out.println("");

        for (int i = 0; i < nodes.getLength(); i++){
            String link = NekoParser.queryText(".//DIV[@class='gs_md_wp']/A/@href", nodes.item(i));
            String citedBy = NekoParser.queryText(".//A[contains(.,'Cited by')]/text()", nodes.item(i));
            String linkToBibTex = NekoParser.queryText(".//A[contains(.,'Import into BibTeX')]/@href", nodes.item(i));
            HttpGet bibTexGet = new HttpGet("http://scholar.google.com" + linkToBibTex);
            HttpResponse response = httpClient.execute(bibTexGet, httpContext);
            String bibTexInfo = IOUtils.toString(response.getEntity().getContent());
            System.out.println(new ScholarResult(link, citedBy, bibTexInfo));
            bibTexGet.releaseConnection();
        }

    }
}
