/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/5/13
 * Time: 2:47 AM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Entity class
 */
public class ScholarResult {
    private String pdfLink;
    private String citedBy;
    private String bibTex;

    public String getPdfLink() {
        return pdfLink;
    }

    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }

    public String getCitedBy() {
        return citedBy;
    }

    public void setCitedBy(String citedBy) {
        this.citedBy = citedBy;
    }

    public String getBibTex() {
        return bibTex;
    }

    public void setBibTex(String bibTex) {
        this.bibTex = bibTex;
    }

    @Override
    public String toString() {
        return "ScholarResult{" +
                "pdfLink='" + pdfLink + '\'' +
                ", citedBy='" + citedBy + '\'' +
                ", bibTex='" + bibTex + '\'' +
                '}';
    }

    public ScholarResult(String pdfLink, String citedBy, String bibTex) {
        this.pdfLink = pdfLink;
        this.citedBy = citedBy;
        this.bibTex = bibTex;
    }
}
