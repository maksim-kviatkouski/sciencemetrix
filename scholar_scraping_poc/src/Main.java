import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        String query = args[0];
        System.out.println("Query for processing acknowledged: " + query);
        ScholarClient scholarClient = new ScholarClient();
        scholarClient.initSettings();
        scholarClient.executeQuery(query);
    }
}
