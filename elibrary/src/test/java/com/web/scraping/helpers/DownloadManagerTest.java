package com.web.scraping.helpers;

import com.web.scraping.elibrary.ElibraryDownloaderFactory;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Test;

import java.io.IOException;
import java.util.Calendar;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/2/13
 * Time: 7:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class DownloadManagerTest {

    @Test
    public void testDownloadThroughProxy() {
        HttpClient httpClient = new DefaultHttpClient();
        HttpHost proxy = new HttpHost("localhost", 9001);
        httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
        try {
            HttpResponse response = httpClient.execute(new HttpGet("http://www.whatsmyip.us/?" + Calendar.getInstance().getTimeInMillis()));
            String content = IOUtils.toString(response.getEntity().getContent());
            String myIP = ScrapeHelper.queryText("//TEXTAREA[@id='do']", ScrapeHelper.getDocument(content));
            myIP = myIP.replaceAll("\n", "").replace("\r", "").replaceAll("\t", "").replaceAll(" ", "");
            assertEquals("174.128.194.142", myIP);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetLinks() {
        DownloadManager downloadManager = new DownloadManager("src/test/java/com/web/scraping/helpers/linksList1.txt", new ElibraryDownloaderFactory());
        while (downloadManager.haveMoreLinks()) {
            String nextLink = downloadManager.getNextLink();
            System.out.println(nextLink);
            downloadManager.markLinkProcessed(nextLink);
        }
    }

    @Test
    public void testDowload() {
        DownloadManager downloadManager = new DownloadManager("src/test/java/com/web/scraping/helpers/linksList2.txt", new ElibraryDownloaderFactory());
        downloadManager.download();
    }
}
