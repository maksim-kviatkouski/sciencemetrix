package com.web.scraping.elibrary.translation.impl;

import com.web.scraping.helpers.translation.Translator;
import com.web.scraping.helpers.translation.impl.SimpleFileBasedTranslator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/13/13
 * Time: 8:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleFileBasedTranslatorTest {
    private static Translator translator;

    @Before
    public void setUp() throws Exception {
        translator = new SimpleFileBasedTranslator("src/test/java/com/web/scraping/elibrary/translation/impl/translation.txt");
    }

    @Test
    public void testTranslate() throws Exception {
        String expected = "yes";
        assertEquals(expected, translator.translate("да"));
    }

    @Test
    public void testNonTranslated() {
        String expected = "без перевода";
        assertEquals(expected, translator.translate("без перевода"));
    }

    @Test
    public void testCountryTranslation() {
        String expected = "Finland";
        assertEquals(expected, translator.translate("Финляндия"));
    }

    @Test
    public void testRegion() {
        String expected = "Khanty-Mansi Autonomous Okrug";
        assertEquals(expected, translator.translate("Ханты-Мансийский автономный округ"));
    }
}
