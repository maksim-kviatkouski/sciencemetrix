package com.web.scraping.elibrary;

import com.web.scraping.helpers.Downloader;
import com.web.scraping.helpers.ScrapeDownloader;
import com.web.scraping.helpers.ScrapeHelper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.net.ProxySelector;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 7/21/13
 * Time: 6:28 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Depends on ScrapeDownloader configuration. May need local/remote proxy to be enabled
 */
public class JournalHelperTest {
    //private static final DefaultHttpClient httpClient = new DefaultHttpClient();
    //httpClient.setRoutePlanner(new ProxySelectorRoutePlanner(httpClient.getConnectionManager().getSchemeRegistry(), ProxySelector.getDefault()));
    //private static final Downloader d = new ElibraryDownloader(httpClient);
    //private static final Document TEST_JOURNAL_DOC = ScrapeHelper.getDocument(d.download(TEST_JOURNAL));


    private static final String TEST_JOURNAL = "http://elibrary.ru/title_about.asp?id=9395";
    private static final Document TEST_JOURNAL_DOC = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(TEST_JOURNAL, new DefaultHttpClient()));


    private static final String TEST_JOURNAL2 = "http://elibrary.ru/title_about.asp?id=9891";
    private static final Document TEST_JOURNAL_DOC2 = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(TEST_JOURNAL2, new DefaultHttpClient()));
    private static final String TEST_EDITORIAL_BOARD_JOURNAL = "http://elibrary.ru/title_about.asp?id=9395";
    private static final Document TEST_EDITORIAL_BOARD_DOC = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(TEST_EDITORIAL_BOARD_JOURNAL, new DefaultHttpClient()));
    private static final String TEST_EDITORIAL_BOARD_JOURNAL2 = "http://elibrary.ru/title_about.asp?id=33402";
    private static final Document TEST_EDITORIAL_BOARD_DOC2 = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(TEST_EDITORIAL_BOARD_JOURNAL2, new DefaultHttpClient()));
    private static final String TEST_EMPTY_RUBRIC = "http://elibrary.ru/title_about.asp?id=11804";
    private static final Document TEST_EMPTY_RUBRIC_DOC = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(TEST_EMPTY_RUBRIC, new DefaultHttpClient()));

    /*@Test
    public void testLetters(){
        System.out.println(JournalHelper.getAllLetters());
    }

    @Test
    public void testPageNumbers(){
        ScrapeDownloader.initSession();
        System.out.println(JournalHelper.getAllPageLinksForLetter("1072"));
    }*/

    @Before
    public void initialize(){

    }

    @Test
    public void testTitle() {
        String expected = "ВЕСТНИК БАШКИРСКОГО УНИВЕРСИТЕТА";
        assertEquals(expected, JournalHelper.extractTitle(TEST_JOURNAL_DOC));
    }

    @Test
    public void testPublisher() {
        //String expected = "Федеральное государственное бюджетное образовательное учреждение высшего профессионального образования \"Башкирский государственный университет\"";
        String expected = "Федеральное государственное бюджетное образовательное учреждение высшего профессионального образования Башкирский государственный университет";

        assertEquals(expected, JournalHelper.extractPublisher(TEST_JOURNAL_DOC));
    }

    @Test
    public void testFoundationYear() {
        String expected = "1996";
        assertEquals(expected, JournalHelper.extractFoundationYear(TEST_JOURNAL_DOC));
    }

    @Test
    public void testPubsPerYear() {
        String expected = "4";
        assertEquals(expected, JournalHelper.extractPubsPerYear(TEST_JOURNAL_DOC));
    }

    @Test
    public void testArticlesPerPub() {
        String expected = "70";
        assertEquals(expected, JournalHelper.extractArticlesPerPub(TEST_JOURNAL_DOC));
    }

    @Test
    public void testIsReviewed() {
        String expected = "yes";
        assertEquals(expected, JournalHelper.extractIsReviewed(TEST_JOURNAL_DOC));
    }

    @Test
    public void testImpactJCR() {
        String expected = "no";
        assertEquals(expected, JournalHelper.extractImpactJCR(TEST_JOURNAL_DOC));
    }


    @Test
    public void testImpactRIOSQ_2011() {
        //String expected = "0,059";
        String expected = "0,099";
        assertEquals(expected, JournalHelper.extractImpactRIOSQ_2013(TEST_JOURNAL_DOC));
    }

    @Test
    public void testAbbreviation() {
        String expected = "Вестник Башкирск. ун-та";
        assertEquals(expected, JournalHelper.extractAbbreviation(TEST_JOURNAL_DOC));
    }

    @Test
    public void testCity() {
        String expected = "Уфа";
        assertEquals(expected, JournalHelper.extractCity(TEST_JOURNAL_DOC));
    }

    @Test
    public void testCountry() {
        String expected = "Russia";
        assertEquals(expected, JournalHelper.extractCountry(TEST_JOURNAL_DOC));
    }

    @Test
    public void testRegion() {
        String expected = "The Republic of Bashkortostan";
        assertEquals(expected, JournalHelper.extractRegion(TEST_JOURNAL_DOC));
    }

    @Test
    public void testPISSN() {
        String expected = "1998-4812";
        assertEquals(expected, JournalHelper.extractPISSN(TEST_JOURNAL_DOC));
    }

    @Test
    public void testSubscriberIndex() {
        String expected = "78387";
        assertEquals(expected, JournalHelper.extractSubscriberIndex(TEST_JOURNAL_DOC));
    }

    @Test
    public void testCirculation() {
        String expected = "1500";
        assertEquals(expected, JournalHelper.extractCirculation(TEST_JOURNAL_DOC));
    }

    @Test
    public void testEISSN() {
        String expected = "";
        assertEquals(expected, JournalHelper.extractEISSN(TEST_JOURNAL_DOC));
    }

    @Test
    public void testWebLink() {
        //String expected = "http://bulletin-bsu.ru";
        String expected = "http://bulletin-bsu.com";
        assertEquals(expected, JournalHelper.extractWebLink(TEST_JOURNAL_DOC));
    }

    @Test
    public void testRepresentation() {
        String expected = "the full text of articles";
        assertEquals(expected, JournalHelper.extractRepresentation(TEST_JOURNAL_DOC));
    }

    @Test
    public void testISI() {
        String expected = "нет";
        assertEquals(expected, JournalHelper.extractISI(TEST_JOURNAL_DOC));
    }

    @Test
    public void testScopus() {
        //String expected = "";
        String expected = "no";
        assertEquals(expected, JournalHelper.extractScopus(TEST_JOURNAL_DOC));
    }

    @Test
    public void testIsInRIOSQ() {
        //String expected = "included";
        String expected = "yes";
        assertEquals(expected, JournalHelper.extractIsInRIOSQ(TEST_JOURNAL_DOC));
    }

    @Test
    public void testIsInHAC() {
        String expected = "included";
        assertEquals(expected, JournalHelper.extractIsInHAC(TEST_JOURNAL_DOC));
    }

    @Test
    public void testTotalArticles() {
        //String expected = "2562";
        String expected = "3009";
        assertEquals(expected, JournalHelper.extractTotalArticles(TEST_JOURNAL_DOC));
    }

    @Test
    public void testTotalPublications() {
        //String expected = "47";
        String expected = "57";
        assertEquals(expected, JournalHelper.extractTotalPublications(TEST_JOURNAL_DOC));
    }

    @Test
    public void testTotalFullTexts() {
        //String expected = "2560";
        String expected = "3003";
        assertEquals(expected, JournalHelper.extractTotalFullTexts(TEST_JOURNAL_DOC));
    }

    @Test
    public void testTotalQuotations() {
        //String expected = "613";
        String expected = "1980";
        assertEquals(expected, JournalHelper.extractTotalQuotations(TEST_JOURNAL_DOC));
    }

    @Test
    public void testIsPublishedNow() {
        String expected = "being published";
        assertEquals(expected, JournalHelper.extractIsPublishedNow(TEST_JOURNAL_DOC));
    }

    @Test
    public void testArchiveTimeFrame() {
        //String expected = "01.2003";
        String expected = "01.2000";
        assertEquals(expected, JournalHelper.extractArchiveTimeFrameStart(TEST_JOURNAL_DOC));
    }

    @Test
    public void testArchiveTimeFrameEnd() {
        //String expected = "01.2013";
        String expected = "01.2014";
        assertEquals(expected, JournalHelper.extractArchiveTimeFrameEnd(TEST_JOURNAL_DOC));
    }

    @Test
    public void testIsAbstract() {
        String expected = "no";
        assertEquals(expected, JournalHelper.extractIsAbstract(TEST_JOURNAL_DOC));
    }

    @Test
    public void testIsMultidisciplinary() {
        String expected = "yes";
        assertEquals(expected, JournalHelper.extractIsMultidisciplinary(TEST_JOURNAL_DOC));
    }

    @Test
    public void testDescription() {
        String expected = "Научный журнал. Учрежден в 1996 году. Публикует результаты исследований по естественным и гуманитарным наукам на русском и английском языках.";
        assertEquals(expected, JournalHelper.extractDescription(TEST_JOURNAL_DOC));
    }

    @Test
    public void testRubrics() {
       /* Collection<Rubric> expected = new HashSet<Rubric>(){{
            add(new Rubric("00.00.00", "Общественные науки в целом", "370"));
            add(new Rubric("31.00.00", "Химия", "970"));
            add(new Rubric("43.00.00", "Общие и комплексные проблемы естественных и точных наук", "169"));
        }};*/
        Collection<Rubric> expected = new HashSet<Rubric>() {{
            add(new Rubric("31.00.00", "Химия", "1137"));
            add(new Rubric("43.00.00", "Общие и комплексные проблемы естественных и точных наук", "243"));
        }};
        assertEquals(expected, JournalHelper.extractRubrics(TEST_JOURNAL_DOC));
    }

    @Test
    public void testTitle2() {
        String expected = "ЭХОГРАФИЯ";
        assertEquals(expected, JournalHelper.extractTitle(TEST_JOURNAL_DOC2));
    }

    @Test
    public void testExtractEditorialBoard() {
        Map<String, Collection<String>> expected = new HashMap<String, Collection<String>>() {{
            put("Главный редактор", new HashSet<String>() {{
                add("Н.Д. Морозкин, д.ф.-м.н., профессор");
            }});
            put("Заместители главного редактора", new HashSet<String>() {{
                add("М.А.Ильгамов, д.ф.-м.н., член-корр. РАН");
                add("Р.Ф. Талипов, д.х.н., профессор");
            }});
            put("Ответственный секретарь", new HashSet<String>() {{
                add("И.С. Шепелевич, к.х.н., доцент");
            }});
            put("Члены редакционной коллегии", new HashSet<String>() {{
                add("А.А. Абдуллина, к.филол.н., доцент");
                add("А.И. Акманов, д.и.н., профессор");
                add("В.И. Баймурзина, д.пед.н., профессор");
                add("Ф.Б. Бурханова, д.социол.н., профессор");
                add("О.К. Валитов, д.филос.н., профессор");
                add("Б.С. Галимов, д.филос.н., профессор");
                add("С.И. Галяутдинова, к.психол.н., доцент");
                add("А.М. Гареев, д.г.н., профессор");
                add("Р.Н. Гимаев, д.т.н., профессор");
                add("З.М. Гусейнова, д.иск., профессор");
                add("У.Г. Гусманов, д.э.н., член-корр. РАСХН");
                add("У.М. Джемилев, д.х.н., член-корр. РАН");
                add("М.В. Зайнуллин, д.филол.н., профессор");
                add("Л.Б. Калимуллина, д.б.н., профессор");
                add("А.Ф. Кудряшев, д.филос.н., профессор");
                add("В.Н. Майстренко, д.х.н., профессор");
                add("Б.М. Миркин, д.б.н., профессор");
                add("Р.З. Мурясов, д.филол.н., профессор");
                add("В.В. Напалков, д.ф.-м.н., член-корр. РАН");
                add("Р.И. Нигматулин, д.ф.-м.н., академик РАН");
                add("Ю.А. Прочухан, д.х.н., профессор");
                add("В.Н. Пучков, д.г.-м.н., член-корр. РАН");
                add("З.Ф. Рахманкулова, д.б.н., профессор");
                add("Г.С. Розенберг, д.б.н., член-корр. РАН");
                add("Л.Г Саяхова, д.пед.н., профессор");
                add("Я.Т. Султанаев, д.ф.-м.н., профессор");
                add("В.М. Тимербулатов, д.м.н., профессор, член-корр. РАМН");
                add("М.М. Утяшев, д.полит.н., профессор");
                add("А.А.Федоров, д.филол.н., профессор");
                add("В.И. Хрулев, д.филол.н., профессор");
                add("Р.Б. Шайхисламов, д.и.н., профессор");
                add("М.С. Юнусов, д.х.н., академик РАН");
                add("Р.А. Якшибаев, д.ф.-м.н., профессор");
            }});
            put("Редакторы", new HashSet<String>() {{
                add("Денис Шамилевич Сабиров, к.х.н");
                add("Рената Разифовна Лукманова, к.филол.н");
                add("Юлия Анатольевна Белова, к.филол.н");
                add("Наталия Владимировна Милицкая");
                add("Римма Флюровна Морозова");
                add("Рудаков Дмитрий Олегович");
            }});
        }};
        assertEquals(expected, JournalHelper.extractEditorialBoard(TEST_EDITORIAL_BOARD_DOC));
        Journal j = new Journal();
    }

    @Test
    public void testExtractEditorialBoard2() {
        Map<String, Collection<String>> expected = new HashMap<String, Collection<String>>() {{
            put("Editors", new HashSet<String>() {{
                add("А. А. Черкасов - главный редактор д-р ист. наук, профессор, заведующий кафедрой истории и культурологии Сочинского государственного университета туризма и курортного дела (СГУТиКД)");
                add("А. М. Мамадалиев - зам. главного редактора канд. пед. наук, доцент кафедры истории и культурологии СГУТиКД");
                add("О. В. Натолочная - ответственный секретарь канд. ист. наук, доцент кафедры истории и культурологии СГУТиКД");
                add("В. Г. Иванцов - канд. ист. наук, доцент кафедры истории и культурологии СГУТиКД");
                add("Н. Ш. Сайфутдинова - канд. пед. наук, доцент кафедры русского языка СГУТиКД");
            }});
        }};
        assertEquals(expected, JournalHelper.extractEditorialBoard(TEST_EDITORIAL_BOARD_DOC2));
        Journal j = new Journal();
        j.setEditorialBoard(JournalHelper.extractEditorialBoard(TEST_EDITORIAL_BOARD_DOC2));
    }

    @Test
    public void testEmptyRubric() {
        Collection<Rubric> expected = new HashSet<Rubric>() {{
            add(new Rubric("", "", ""));
        }};
        assertEquals(expected, JournalHelper.extractRubrics(TEST_EMPTY_RUBRIC_DOC));
    }
}
