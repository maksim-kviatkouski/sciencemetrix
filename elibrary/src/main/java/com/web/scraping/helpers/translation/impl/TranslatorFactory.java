package com.web.scraping.helpers.translation.impl;

import com.web.scraping.helpers.translation.Translator;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/15/13
 * Time: 9:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class TranslatorFactory {
    private final static Logger LOG = Logger.getLogger(TranslatorFactory.class.getName());

    public static Translator getTranslator(String filename) {
        try {
            return new SimpleFileBasedTranslator(filename);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Could not initialize file based translator so no translation will be performed. Expected file: " + filename, e);
            return new VoidTranslator();
        }
    }
}
