package com.web.scraping.helpers.translation.impl;

import com.web.scraping.helpers.translation.Translator;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/13/13
 * Time: 8:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class SimpleFileBasedTranslator implements Translator {
    private Properties translationMap;
    private final Logger LOG = Logger.getLogger(this.getClass().getName());

    public SimpleFileBasedTranslator(String fileName) throws IOException {
        translationMap = new Properties();
        translationMap.load(new FileInputStream(fileName));
    }

    @Override
    public String translate(String s) {
        String result = s;
        try {
            //converting string we would like to translate from UTF-8 into ISO-8859-1 since
            //encoding for all files of this program is UTF-8 so it's best to be consistent.
            //at the same time java built-in class Properties that works really well for simple translation
            //treats input file as ISO-8859-1 and there is no way to tell it use different encoding unless
            //one wants to override ResourceBundle.Control class which will take ~2h of development and testing
            String _s = new String(s.getBytes("UTF-8"), "ISO-8859-1");
            result = translationMap.getProperty(_s, s);
        } catch (UnsupportedEncodingException e) {
            LOG.log(Level.SEVERE, "Either UTF-8 or ISO-8859-1 are not supported in this system", e);
        } finally {
            return result;
        }
    }
}
