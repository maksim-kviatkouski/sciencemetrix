package com.web.scraping.helpers.translation.impl;

import com.web.scraping.helpers.translation.Translator;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Fallback translator which is returned in case file-based translator was not initialized.
 */
public class VoidTranslator implements Translator {
    @Override
    public String translate(String s) {
        return s;
    }
}
