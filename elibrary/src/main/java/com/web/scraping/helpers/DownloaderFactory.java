package com.web.scraping.helpers;

import org.apache.http.client.HttpClient;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/19/13
 * Time: 9:41 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DownloaderFactory {
    public Downloader newInstance(HttpClient httpClient);
}
