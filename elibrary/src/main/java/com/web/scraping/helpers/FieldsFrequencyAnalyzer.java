package com.web.scraping.helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: Maksim_Kviatkouski
 * Date: 2/28/13 Time: 11:08 PM
 */

public class FieldsFrequencyAnalyzer {
    private static Map<String, Integer> frequencyMap = new HashMap<String, Integer>();

    public static void loadJournalFields(List<String> fields) {
        for (String f : fields) {
            if (!frequencyMap.containsKey(f)) {
                frequencyMap.put(f, 1);
            } else {
                frequencyMap.put(f, frequencyMap.get(f) + 1);
            }
        }
        System.out.println("[DEBUG 1]: " + fields);
        System.out.print("[DEBUG 2]: " + frequencyMap);
    }

    public static Map<String, Integer> getFrequencyMap() {
        return frequencyMap;
    }

}
