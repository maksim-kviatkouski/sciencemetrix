package com.web.scraping.helpers;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 7/29/13
 * Time: 10:04 PM
 * To change this template use File | Settings | File Templates.
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.http.HttpHost;
import org.apache.http.conn.params.ConnRoutePNames;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class that provides ability to download links listed in some file, be stopped and resume from where it stopped.
 * DONE:randomized suspends
 * DONE:rotating proxies?
 * DONE:multithreading?
 */
public class DownloadManager {
    private final static Logger LOG = Logger.getLogger(DownloadManager.class.getName());
    private String linkListingFileName;
    private DownloaderFactory dFactory;
    //TODO: replace with initialization with generic list of proxies
    private List<HttpHost> proxies = new LinkedList<HttpHost>() {{
//        add(new HttpHost("localhost", 9200));
        add(new HttpHost("localhost", 9201));
        add(new HttpHost("localhost", 9202));
        add(new HttpHost("localhost", 9203));
        add(new HttpHost("localhost", 9204));
        add(new HttpHost("localhost", 9205));
        add(new HttpHost("localhost", 9206));
        add(new HttpHost("localhost", 9207));
        add(new HttpHost("localhost", 9208));
        add(new HttpHost("localhost", 9209));
        add(new HttpHost("localhost", 9210));
//        add(new HttpHost("localhost", 9200));
    }};
    private ExecutorService executor = Executors.newFixedThreadPool(proxies.size());
    private GenericObjectPool<Downloader> httpClientsPool;

    private RandomAccessFile unprocessedLinks;
    private RandomAccessFile processedLinks;

    public DownloadManager(String linkListingFileName, DownloaderFactory dFactory) {
        this.linkListingFileName = linkListingFileName;
        this.dFactory = dFactory;
        init();
    }

    private void init() {

        httpClientsPool = new GenericObjectPool<Downloader>(
                new ProxiedHttpClientsPoolFactory(proxies, dFactory),
                proxies.size(),
                GenericObjectPool.WHEN_EXHAUSTED_BLOCK,
                -1,
                proxies.size()
        );
        httpClientsPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_BLOCK);
        httpClientsPool.setLifo(false); //all clients need to send more or less equal number of requests
        //so we should take one that was not used longest
        File _unprocessedFile = new File(getUnprocessedFileName());
        try {
            //TODO: do not copy file in case there is one already to implement 'resume' behavior
            File fileCheckIfExist = new File(getProcessedFileName());
            if (fileCheckIfExist.exists()) {
                unprocessedLinks = new RandomAccessFile(getUnprocessedFileName(), "rwd");
                processedLinks = new RandomAccessFile(getProcessedFileName(), "rwd");
                //processedLinks  new OutputStreamWriter(openFileOutput(getProcessedFileName(), Context.MOD_PRIVATE));
                //processedLinks = new OutputStreamWriter(new FileOutputStream(getProcessedFileName()));
                return;
            }
            FileUtils.copyFile(new File(linkListingFileName), _unprocessedFile);
            unprocessedLinks = new RandomAccessFile(getUnprocessedFileName(), "rwd");
            processedLinks = new RandomAccessFile(getProcessedFileName(), "rwd");
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "IO exception while initializing files with links to download", e);
        }
    }

    public void download() {
        Downloader downloader = null;
        try {
            while (true) {
                LOG.info("Borrowing next downloader. There are " + httpClientsPool.getNumActive() + " active and " + httpClientsPool.getNumIdle() + " idle now.");
                final Downloader _downloader = httpClientsPool.borrowObject();
                LOG.info("Borrowed " + _downloader.getHttpClient().getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY).toString());
                downloader = _downloader;
                final String link = getNextLink();
                if (link == null) break;
                executor.submit(new Runnable() {
                    @Override
                    public void run() {

                        String proxySettings = _downloader.getHttpClient().getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY).toString();
                        LOG.fine("Http client with proxy " + proxySettings + " started downloading");
                        try {
                            long _start = Calendar.getInstance().getTimeInMillis();
                            _downloader.download(link);
                            long _end = Calendar.getInstance().getTimeInMillis();
                            markLinkProcessed(link);
                            LOG.info("Http client with proxy " + proxySettings + " downloaded " + link + ". " + (_end - _start) + "ms.");
                            Thread.sleep(3000);

                        } catch (Exception e) {
                            LOG.log(Level.SEVERE, "Exception while downloading link " + link, e);
                        } finally {
                            if (_downloader != null) try {
                                httpClientsPool.returnObject(_downloader);
                            } catch (Exception e) {
                                LOG.log(Level.SEVERE, "Exception while returing http client into pool", e);
                            }
                        }
                    }
                });
            }
            LOG.fine("No more links to download. Waiting threads to complete last downloads.");
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.DAYS);
            LOG.info("All links downloaded.");
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Something went wrong while downloading links", e);
        } finally {
            if (downloader != null) {
                try {
                    //httpClientsPool.returnObject(downloader);
                } catch (Exception e) {
                    LOG.log(Level.SEVERE, "Something went wrong while trying to put http client back to pool", e);
                }
            }
        }
    }

    public synchronized void markLinkProcessed(String link) {
        try {
            //processedLinks.write(link + "\n");
            //processedLinks.flush();
            processedLinks.writeUTF(link + "\n");

        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Could not write into file " + getProcessedFileName(), e);
        }
    }

    public synchronized String getNextLink() {
        String result = null;
        try {
            long start = unprocessedLinks.getFilePointer();
            result = unprocessedLinks.readLine();
            if (result == null) {
                return result;
            }
            long end = unprocessedLinks.getFilePointer();
            byte[] buf = new byte[(int) (end - start)];
            unprocessedLinks.seek(start);
            unprocessedLinks.write(buf);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Exception while retrieving next unprocessed link");
        }
        return result;
    }

    public synchronized boolean haveMoreLinks() {
        long start = 0;
        boolean result = false;
        try {
            start = unprocessedLinks.getFilePointer();
            result = unprocessedLinks.readLine() != null;
            unprocessedLinks.seek(start);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Could not check file " + getUnprocessedFileName() + " for more links", e);
        }
        return result;
    }

    /**
     * Should be overriden in case custom naming convention should be implemented
     *
     * @return name of file containing processed links
     */
    protected String getProcessedFileName() {
        return linkListingFileName.replaceAll(".txt", "_processed.txt");
    }

    /**
     * Should be overriden in case custom naming convention should be implemented
     *
     * @return name of file containing unprocessed links
     */
    protected String getUnprocessedFileName() {
        return linkListingFileName.replaceAll(".txt", "_unprocessed.txt");
    }
}
