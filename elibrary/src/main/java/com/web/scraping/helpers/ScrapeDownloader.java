package com.web.scraping.helpers;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.net.ProxySelector;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * User: Maksim_Kviatkouski
 * Date: 2/25/13 Time: 10:53 PM
 */
public class ScrapeDownloader {

    private final static Logger log = Logger.getLogger(ScrapeDownloader.class.getName());

    private final static int MAX_DOWNLOAD_ATTEMPTS = 50;
    public static final String ENCODING = "UTF-8";

    private static DefaultHttpClient httpClient = new DefaultHttpClient();
    private static HttpContext httpContext = new BasicHttpContext();
    private static CookieStore cookieStore = new BasicCookieStore();
    static {
        httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BEST_MATCH);
        httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
        //two lines below are written to have Apache HTTP Client respecting system proxy settings
        ProxySelectorRoutePlanner routePlanner = new ProxySelectorRoutePlanner(httpClient.getConnectionManager().getSchemeRegistry(), ProxySelector.getDefault());
        httpClient.setRoutePlanner(routePlanner);
//        httpClient.getParams().setParameter("http.socket.timeout", new Integer("20000"));
    }

    @Deprecated
    /**
     * Should not be used as it contains inappropriate initSession method. Downloader interface and its implementations
     * should be used instead.
     */
    public static void initSession(){
        HttpPost httpPost = new HttpPost("http://elibrary.ru/");
        try {
            HttpResponse response = httpClient.execute(httpPost, httpContext);
            EntityUtils.consumeQuietly(response.getEntity());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String downloadLink(String url){
        String result = "";
        boolean done = false;
        int attempts = 0;

        HttpGet httpGet = new HttpGet(url);
        httpGet.getParams().setParameter("http.socket.timeout", new Integer(10000));
        long downloadTime = 0;

        while (!done && attempts <= MAX_DOWNLOAD_ATTEMPTS) {
            try {
                long start = Calendar.getInstance().getTimeInMillis();
                HttpResponse response = httpClient.execute(httpGet, httpContext);
                result = IOUtils.toString(response.getEntity().getContent());
                downloadTime = Calendar.getInstance().getTimeInMillis() - start;
//                httpClient.getConnectionManager().closeExpiredConnections();
                if (!result.contains("Access forbidden!")) { done = true;}
                else { log.info("Access forbidden has been returned for link " + url); Thread.sleep(2000);}
            } catch (Exception e) {
                log.log(Level.WARNING, "Exception while downloading " + url, e);
                attempts++;
            }
        }
        if (attempts > 0){
            log.info("Downloaded " + url + " successfully after " + attempts + " attempts.");
        } else if (attempts > MAX_DOWNLOAD_ATTEMPTS) {
            log.warning("Failed to download " + url);
        } else {
            log.info("Downloaded " + url + " in " + downloadTime + " ms.");
        }

        return result;
    }

    public static String downloadLink(String url, HttpClient client){
        String result = "";
        HttpGet httpGet = new HttpGet(url);
        boolean done = false;
        int attempts = 0;
        httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        httpGet.addHeader("Accept-Encoding", "gzip,deflate,sdch");
        httpGet.addHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
        httpGet.addHeader("Cache-Control", "max-age=0");
        httpGet.addHeader("Connection", "keep-alive");
        httpGet.addHeader("Host", "elibrary.ru");
        httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.76 Safari/537.36 u01-04");


        while (!done && attempts++ < 10){
            try {
                HttpResponse response = client.execute(httpGet);
                result = IOUtils.toString(response.getEntity().getContent());
                done = true;
            } catch (IOException e) {
                System.out.println("Opps, something is wrong with link " + url + ". Retrying. " + e.getMessage());
                if (attempts == 1) {
                    FileWriter sw = null;
                    try {
                        sw = new FileWriter("failedLinks.txt", true);
                        sw.write(url + "\n\r");
                        sw.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }


                }
            }
        }

        return result;
    }
}
