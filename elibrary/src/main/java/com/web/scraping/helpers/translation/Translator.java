package com.web.scraping.helpers.translation;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/13/13
 * Time: 8:34 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Translator {
    public String translate(String s);
}
