package com.web.scraping;

import com.web.scraping.elibrary.*;
import com.web.scraping.helpers.DownloadManager;
import com.web.scraping.helpers.Downloader;
import com.web.scraping.helpers.ScrapeHelper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;

import java.io.*;
import java.net.ProxySelector;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

//DONE: try to implement proxy chain support on a script level.
//it's implemented assuming following setup:
//script → apache → external proxy → target site
//this way script can loop through proxies set up on different ports of local/remote apache
//without any idea if there any other proxies behind apache

//http://httpcomponents.10934.n7.nabble.com/Implementing-Proxy-Chaining-in-Apache-HTTP-td11963.html
//http://www.apache.org/dist/httpcomponents/httpclient/RELEASE_NOTES.txt
//https://issues.apache.org/jira/browse/HTTPCLIENT-649

//TODO: move all script logic onto ElibraryDownloader
public class Main {
    private final static Logger LOG = Logger.getLogger(com.web.scraping.Main.class.getName());
    private final static DateFormat FILE_NAME_DF = new SimpleDateFormat("yyyy-MM-dd--hh-mm-ss");
    private final static String FILE_NAME = "elibrary_journal_%s.txt";
    private final static String RUBRICS_FILE_NAME = "elibrary_journal_rubrics_%s.txt";
    private final static String DUMP_FILENAME = "elibrary_journal_links_dump_%s.txt";
    public static final String NEW_LINE = "\r\n";
    public static final String ENCODING = "UTF-8";

    public static void main(String[] args) {

        if (args.length > 0 && "download".equals(args[0])) {
            LOG.info("Downloading journals based on a list of links");
            downloadAll();
        } else if (args.length > 0 && "makeFileWithLinks".equals(args[0])) {
            LOG.info("Make file with links");
            makeFileWithLinks();
        } else {
            LOG.info("Regular run");
            regularRun();
        }
//        for (String link : allLinks) {ScrapeDownloader.downloadLink(link); }
//        System.out.println(allLinks.size() + " links in total");
//        downloadAll();
    }

    private static void regularRun() {
      //  ScrapeDownloader.initSession();
        //Collection<String> allLinks = JournalHelper.getAllJournalLinks();
        Collection<String> allLinks = new HashSet<String>();
        try {
            FileReader fr = new FileReader("links.txt");
            BufferedReader br = new BufferedReader(fr);
            String temp = new String();
            while ((temp = br.readLine())!=null)
                allLinks.add(temp);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        DefaultHttpClient httpClient = new DefaultHttpClient();
        httpClient.setRoutePlanner(new ProxySelectorRoutePlanner(httpClient.getConnectionManager().getSchemeRegistry(), ProxySelector.getDefault()));
        Downloader d = new ElibraryDownloader(httpClient);
        d.doHandShaking();
        Collection<Journal> journals = new LinkedList<Journal>();
        try {
            Date now = Calendar.getInstance().getTime();
            OutputStreamWriter journalsFile = new OutputStreamWriter(
                    new FileOutputStream(
                            String.format(FILE_NAME, FILE_NAME_DF.format(now))
                    ),
                    ENCODING
            );
            OutputStreamWriter rubricsFile = new OutputStreamWriter(
                    new FileOutputStream(
                            String.format(RUBRICS_FILE_NAME, FILE_NAME_DF.format(now))
                    ),
                    ENCODING
            );
            journalsFile.write(Journal.tabDelimiterHeader() + NEW_LINE);
            rubricsFile.write(Rubric.tabDelimiterHeader() + NEW_LINE);
            int debug = 0;
            for (String link : allLinks) {
                Journal journal = JournalHelper.buildJournal(
                        ScrapeHelper.getDocument(
                                d.download(link)
                        )
                );
                journal.setUrl(link);
                journalsFile.write(journal.toTabDelimitedString() + NEW_LINE);
                journalsFile.flush();
                for (Rubric rubric : journal.getRubrics()) {
                    if (rubric.isEmpty()) continue;

                    rubric.setJournalId(link);
                    rubricsFile.write(rubric.toTabDelimitedString() + NEW_LINE);
                }
                //if (debug++ > 500) break;
            }
            journalsFile.close();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Could not initialize file for journals output", e);
        }
    }

    public static void dumpJournalLinks(Collection<String> links, String filename) {
        try {
            OutputStreamWriter dumpFileWriter = new OutputStreamWriter(new FileOutputStream(filename), ENCODING);
            for (String l : links) dumpFileWriter.write(l + NEW_LINE);
            dumpFileWriter.close();
        } catch (UnsupportedEncodingException e) {
            LOG.log(Level.WARNING, "Encoding is not supported", e);
        } catch (FileNotFoundException e) {
            LOG.log(Level.WARNING, "Could not fine/create file " + filename, e);
        } catch (IOException e) {
            LOG.log(Level.WARNING, "Could not close file " + filename, e);
        }
    }

    public static void makeFileWithLinks() {
        Collection<String> allLinks = JournalHelper.getAllJournalLinks();
        dumpJournalLinks(allLinks, "links.txt");

        //this code made file with links to failed links from file failedLinks.txt
       /* Collection<String> result = new HashSet<String>();
        result.addAll( JournalHelper.getAllJournalLinksFromPage("http://elibrary.ru/titles.asp?letter=1074&pagenum=14"));
        result.addAll( JournalHelper.getAllJournalLinksFromPage("http://elibrary.ru/titles.asp?letter=102&pagenum=1"));
        dumpJournalLinks(result,"linksToFailedLinks.txt");*/
    }

    public static void downloadAll() {
        DownloadManager dManager = new DownloadManager("links.txt", new ElibraryDownloaderFactory());
        dManager.download();
    }

}
