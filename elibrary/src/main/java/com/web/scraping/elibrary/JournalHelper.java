package com.web.scraping.elibrary;

import com.web.scraping.helpers.ScrapeDownloader;
import com.web.scraping.helpers.ScrapeHelper;
import com.web.scraping.helpers.translation.Translator;
import com.web.scraping.helpers.translation.impl.TranslatorFactory;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 7/21/13
 * Time: 6:17 PM
 * To change this template use File | Settings | File Templates.
 */
//TODO: do global field analysis once whole site is downloaded
//DONE: do we need http://screencast.com/t/mxe8TSwd76 ?
//we do
public class JournalHelper {
    public static final String DOMAIN = "http://elibrary.ru";
    public static final String LISTING_ENTRY_PAGE = "/titles.asp";
    public static final String START_SESSION_URL = "/start_session.asp";
    public static final String EMPTY_RUBRIC = "Не назначено рубрик";
    private static Translator TRANSLATOR = TranslatorFactory.getTranslator("translation.txt");
    private static final Logger LOG = Logger.getLogger(JournalHelper.class.getName());


    private static AtomicInteger linksCounter = new AtomicInteger(0);
    //this is for creating cash throught special httpClient
    private static String PROXY_HOST = "localhost";
    private static int PROXY_PORT = 9200;
    private static HttpHost proxy = new HttpHost(PROXY_HOST, PROXY_PORT);
    private static HttpRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy) {

        @Override
        public HttpRoute determineRoute(
                final HttpHost host,
                final HttpRequest request,
                final HttpContext context) throws HttpException {
            String hostname = host.getHostName();
            if (hostname.equals("127.0.0.1") || hostname.equalsIgnoreCase("localhost")) {
                // Return direct route
                return new HttpRoute(host);
            }
            return super.determineRoute(host, request, context);
        }
    };

    private static CloseableHttpClient closeableHttpClient = HttpClients.custom()
            .setRoutePlanner(routePlanner)
            .build();

    //TODO: try to make letters sorted same way each time without converting them into Integer
    public static Collection<String> getAllLetters() {
        Collection<String> result = new TreeSet<String>();
        Document doc = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(DOMAIN + LISTING_ENTRY_PAGE, closeableHttpClient));
        List<String> links = ScrapeHelper.queryTextValues("//MAP/AREA/@href", doc);
        Pattern imageMapLinkPattern = Pattern.compile("javascript\\:search_letter\\(([0-9]*)\\)");
        for (String l : links) {
            Matcher imageMapLinkMatcher = imageMapLinkPattern.matcher(l);
            if (imageMapLinkMatcher.find()) result.add(imageMapLinkMatcher.group(1));
        }
        LOG.info("ended getAllLetters method. letters count = " + result.size());
        return result;
    }

    public static Collection<String> getAllPageLinksForLetter(String letter) {
        Collection<String> result = new TreeSet<String>();
        String html = ScrapeDownloader.downloadLink(DOMAIN + LISTING_ENTRY_PAGE + "?letter=" + letter, closeableHttpClient);
        Document doc = ScrapeHelper.getDocument(html);
        String lastPageLink = ScrapeHelper.queryText("//A[contains(.,'В\u00a0конец')]/@href", doc);
        Pattern lastPageLinkPattern = Pattern.compile("javascript\\:goto_page\\(([0-9]*)\\)");
        Matcher lastPageLinkMatcher = lastPageLinkPattern.matcher(lastPageLink);
        LOG.info("get all page links for letter -- " + letter);
        if (lastPageLinkMatcher.find()) {
            try {
                int totalPages = Integer.valueOf(lastPageLinkMatcher.group(1)).intValue();
                for (int i = 1; i <= totalPages; i++) {
                    result.add(DOMAIN + LISTING_ENTRY_PAGE + "?letter=" + letter + "&pagenum=" + i);
                }

            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        } else {
            //if there is only one page for letter
            result.add(DOMAIN + LISTING_ENTRY_PAGE + "?letter=" + letter);
        }
        return result;
    }

    public static Collection<String> getAllPageLinks() {
        Collection<String> result = new HashSet<String>();
        for (String letter : getAllLetters()) {
            result.addAll(getAllPageLinksForLetter(letter));
        }
        return result;
    }

    public static Collection<String> getAllJournalLinksFromPage(String pageLink) {
        Collection<String> result = new HashSet<String>();
        Document doc = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(pageLink, closeableHttpClient));
        Collection<String> _result = ScrapeHelper.queryTextValues("//TABLE[@id='restab']//TR/TD[2]/A[contains(@href,'title_about.asp')]/@href", doc);
        for (String s : _result) result.add(DOMAIN + "/" + s);
        if ((linksCounter.addAndGet(1) % 50) == 0) {
            System.out.println(linksCounter.get() + " links founded");
        }
        return result;
    }

    public static Collection<String> getAllJournalLinks() {
        Collection<String> result = new HashSet<String>();
        for (String pageLink : getAllPageLinks()) result.addAll(getAllJournalLinksFromPage(pageLink));
        return result;
    }

    public static String clean(String s) {
        return s != null ? s.replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "").replaceAll("\u00a0", "")
                .replaceAll("\u008e", "\u017d").replaceAll(" +", " ").trim() : "";
    }

    public static String extractTitle(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(.,'Полное название')]/../../TD[2]/FONT/B", doc));
    }

    public static String extractPublisher(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Издательство')]/../../TD[2]/FONT/A", doc));
    }

    public static String extractFoundationYear(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Год основания')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractPubsPerYear(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Выпусков в год')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractAbbreviation(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Сокращение')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractArticlesPerPub(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Статей в выпуске')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractIsReviewed(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Рецензируемый')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractImpactJCR(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Импакт-фактор JCR')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractImpactRIOSQ_2013(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Импакт-фактор РИНЦ 2013')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractCity(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Город')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractCountry(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Страна')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractRegion(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Регион')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractPISSN(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'ISSN печатной версии')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractSubscriberIndex(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Подписной индекс')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractCirculation(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Тираж')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractEISSN(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'ISSN онлайновой версии')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractWebLink(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'WWW-адрес')]/../following-sibling::TD[1]/FONT", doc));
    }

    //DONE: ask if I need to translate enumeration values in Russian to English
    //we need. implemented.
    public static String extractRepresentation(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Вариант представления')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractISI(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'ISI')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractScopus(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'SCOPUS')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractIsInRIOSQ(Document doc) {
        //we need exact match here since there is another field above that contains word 'РИНЦ'
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[text()='РИНЦ']/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractIsInHAC(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(.,'Перечень ВАК')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractTotalArticles(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Всего статей')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractTotalPublications(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Всего выпусков')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractTotalFullTexts(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Полных текстов')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractTotalQuotations(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Цитирований')]/../following-sibling::TD[1]/FONT", doc));
    }

    public static String extractIsPublishedNow(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'В настоящее время')]/../following-sibling::TD[1]/FONT", doc)));
    }

    //DONE: ask if I need to parse this field into two fields? do I need to try to convert it to some other date format?
    //extracting both dates into separate fields
    public static String extractArchiveTimeFrameStart(Document doc) {
        String result = clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Доступный архив')]/../following-sibling::TD[1]/FONT", doc));
        String[] dates = result.split("-");
        if (dates.length > 0) result = clean(dates[0]);
        else result = "";
        return result;
    }

    public static String extractArchiveTimeFrameEnd(Document doc) {
        String result = clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Доступный архив')]/../following-sibling::TD[1]/FONT", doc));
        String[] dates = result.split("-");
        if (dates.length > 1) result = clean(dates[1]);
        else result = "";
        return result;
    }

    public static String extractIsAbstract(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Реферативный')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractIsMultidisciplinary(Document doc) {
        return TRANSLATOR.translate(clean(ScrapeHelper.queryText("//TR/TD/FONT[contains(., 'Мультидисциплинарный')]/../following-sibling::TD[1]/FONT", doc)));
    }

    public static String extractDescription(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD/TABLE/TBODY/TR/TD/FONT[contains(., 'Описаниежурнала')]/../../../../../following-sibling::TD[1]/TABLE/TBODY/TR/TD/FONT", doc));
    }

    public static Map<String, Collection<String>> extractEditorialBoard(Document doc) {
        Map<String, Collection<String>> result = new HashMap<String, Collection<String>>();
        NodeList nodeList = ScrapeHelper.queryNodes("//TR/TD/TABLE/TBODY/TR/TD/FONT[contains(., 'коллегия')]/../../../../../following-sibling::TD[1]/TABLE/TBODY/TR/TD/FONT//text()", doc);
        String _category = "Editors";
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node n = nodeList.item(i);
            if (n.getParentNode().getNodeName().equals("B")) {
                _category = cleanUpEditorial(clean(n.getTextContent()));
            } else {
                final String editor = cleanUpEditorial(clean(n.getNodeValue()));
                if (result.containsKey(_category)) result.get(_category).add(editor);
                else result.put(_category, new HashSet<String>() {{
                    add(editor);
                }});
            }
        }
        return result;
    }

    private static String cleanUpEditorial(String s) {
        String result = s;
        if (s.endsWith(":")) s = s.substring(0, s.length() - 1);
        if (s.endsWith(";")) s = s.substring(0, s.length() - 1);
        if (s.endsWith(".")) s = s.substring(0, s.length() - 1);
        if (s.endsWith(",")) s = s.substring(0, s.length() - 1);
        return clean(s);
    }

    public static Collection<Rubric> extractRubrics(Document doc) {
        Collection<Rubric> result = new HashSet<Rubric>();

        NodeList nodes = ScrapeHelper.queryNodes("//TR/TD/FONT[contains(.,'Тематические рубрики')]/../following-sibling::TD[1]//TR[position() > 1]", doc);
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            String code = clean(ScrapeHelper.queryText("./TD[1]", node));
            if (EMPTY_RUBRIC.equals(code)) code = "";
            String section = clean(ScrapeHelper.queryText("./TD[2]", node));
            String totalJournals = clean(ScrapeHelper.queryText("./TD[3]", node));
            result.add(new Rubric(code, section, totalJournals));
        }

        return result;
    }

    public static Journal buildJournal(Document doc) {
        Journal result = new Journal();

        result.setAbbreviation(extractAbbreviation(doc));
        result.setAbstract(extractIsAbstract(doc));
        result.setArchiveTimeframeStart(extractArchiveTimeFrameStart(doc));
        result.setArchiveTimeframeEnd(extractArchiveTimeFrameEnd(doc));
        result.setArticlesPerPub(extractArticlesPerPub(doc));
        result.setCirculation(extractCirculation(doc));
        result.setCity(extractCity(doc));
        result.setCountry(extractCountry(doc));
        result.setDescription(extractDescription(doc));
        result.seteISSN(extractEISSN(doc));
        result.setFoundationYear(extractFoundationYear(doc));
        result.setImpactJCR(extractImpactJCR(doc));
        result.setImpactRIOSQ_2011(extractImpactRIOSQ_2013(doc));
        result.setInHAC(extractIsInHAC(doc));
        result.setInRIOSQ(extractIsInRIOSQ(doc));
        result.setIsi(extractISI(doc));
        result.setMultidisciplinary(extractIsMultidisciplinary(doc));
        result.setpISSN(extractPISSN(doc));
        result.setPublishedNow(extractIsPublishedNow(doc));
        result.setPublisher(extractPublisher(doc));
        result.setPubsPerYear(extractPubsPerYear(doc));
        result.setRegion(extractRegion(doc));
        result.setRepresentation(extractRepresentation(doc));
        result.setReviewed(extractIsReviewed(doc));
        result.setRubrics(extractRubrics(doc));
        result.setScopus(extractScopus(doc));
        result.setSubscriberIndex(extractSubscriberIndex(doc));
        result.setTitle(extractTitle(doc));
        result.setTotalArticles(extractTotalArticles(doc));
        result.setTotalFullTexts(extractTotalFullTexts(doc));
        result.setTotalPublications(extractTotalPublications(doc));
        result.setTotalQuotations(extractTotalQuotations(doc));
        result.setWebLink(extractWebLink(doc));
        result.setEditorialBoard(extractEditorialBoard(doc));


        return result;
    }
}
