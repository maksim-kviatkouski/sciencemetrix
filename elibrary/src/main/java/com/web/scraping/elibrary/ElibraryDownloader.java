package com.web.scraping.elibrary;

import com.web.scraping.helpers.Downloader;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/2/13
 * Time: 8:51 PM
 * To change this template use File | Settings | File Templates.
 */
//DONE: something is wrong with encoding of ElibraryDownloader. try to compare result of its download and ScrapeDownloader
//for some reason I need to explicitly mention -Dfile.encoding=UTF-8 when launching jar file
//TODO: session is not initialized properly for some reason and first journal gets redirect. it should be fixed either by
//improving handshake method or by configuring Apache not to cache 302 redirects (preferrable)
public class ElibraryDownloader implements Downloader {
    private static final long CACHE_DOWNLOAD_TIME = 50;
    public static final int MINIMUM_SLEEP_TIME_SECONDS = 10;
    public static final int VARIABLE_SLEEP_TIME_SECONDS = 10;
    private static final int CLEAN_CACHE_THRESHOLD = 100;
    private Integer journalProcessedCounter = 0;
    private HttpContext httpContext = new BasicHttpContext();
    private CookieStore cookieStore = new BasicCookieStore();
    private HttpClient httpClient;
    private final static int MAX_DOWNLOAD_ATTEMPTS = 5;
    private final Logger LOG = Logger.getLogger(ElibraryDownloader.class.getName());

    public ElibraryDownloader(HttpClient httpClient) {
        this.httpClient = httpClient;
        httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BEST_MATCH);
        httpClient.getParams().setParameter("http.socket.timeout", new Integer(10000));
        httpClient.getParams().setParameter("http.protocol.warn-extra-input", true);
        HttpClientParams.setConnectionManagerTimeout(httpClient.getParams(), 10000);
        httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

    }

    @Override
    public void init() {
    }

    @Override
    public void doHandShaking() {
        LOG.fine("Doing handshaking");
        HttpPost httpPost = new HttpPost(JournalHelper.DOMAIN + "/start_session.asp?rpage=" + Calendar.getInstance().getTimeInMillis());
        mimicBrowserRequest(httpPost);
        try {
            HttpResponse response = httpClient.execute(httpPost, httpContext);
            EntityUtils.consumeQuietly(response.getEntity());
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Error while doing handshake with site");
        }
        LOG.fine("Handshaking done");
    }

    private void mimicBrowserRequest(HttpRequest httpRequest) {
        httpRequest.setHeader(new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36"));
        httpRequest.setHeader(new BasicHeader("Accept-Language", "en-US,en;q=0.8"));
        httpRequest.setHeader(new BasicHeader("Host", "elibrary.ru"));
        httpRequest.setHeader(new BasicHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));
        httpRequest.setHeader(new BasicHeader("Accept-Encoding", "gzip,deflate,sdch"));
    }

    @Override
    public String download(String url) {
        String result = "";
        boolean done = false;
        int attempts = 0;

        if ((journalProcessedCounter % CLEAN_CACHE_THRESHOLD) == 0){

            //LOG.info(String.format("HTTP client %s processed %s links", httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY).toString(), journalProcessedCounter));
            LOG.info(String.format("HTTP client  processed %s links", journalProcessedCounter));

            int len = 0;
            for (Cookie cookie : cookieStore.getCookies()) {
                len += cookie.getValue().getBytes().length;
            }
            LOG.info("size of cookies before delete :" + len);
            cookieStore.clear();
            httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
            doHandShaking();
            len = 0;
            for (Cookie cookie : cookieStore.getCookies()) {
                len += cookie.getValue().getBytes().length;
            }
            LOG.info("size of cookies after delete :" +  len);
           // LOG.info("size of cookies after delete:"+  (httpContext.getAttribute(ClientContext.COOKIE_STORE).toString().length()));
        }

        if(journalProcessedCounter %100 == 0){
            //LOG.info(String.format("HTTP client %s processed %s links", httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY).toString(), journalProcessedCounter));
            LOG.info(String.format("HTTP client  processed %s links", journalProcessedCounter));
        }


        HttpGet httpGet = new HttpGet(url);
        mimicBrowserRequest(httpGet);
        long downloadTime = 0;

        while (!done && attempts <= MAX_DOWNLOAD_ATTEMPTS) {
            try {
                long start = Calendar.getInstance().getTimeInMillis();
                HttpResponse response = httpClient.execute(httpGet, httpContext);
                InputStream content = response.getEntity().getContent();
                result = IOUtils.toString(content);
                content.close();
                downloadTime = Calendar.getInstance().getTimeInMillis() - start;
                LOG.info(url + " HTTP " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
                httpGet.releaseConnection();
                httpClient.getConnectionManager().closeExpiredConnections();
                httpClient.getConnectionManager().closeIdleConnections(10, TimeUnit.SECONDS);
                done = true;
            } catch (Exception e) {
                LOG.log(Level.WARNING, "Exception while downloading " + url, e);
                attempts++;
            }
        }
        if (attempts > 0) {
            LOG.info("Downloaded " + url + " successfully after " + attempts + " attempts.");

        } else if (attempts > MAX_DOWNLOAD_ATTEMPTS) {
            LOG.warning("Failed to download " + url);
        } else {
            LOG.info("Downloaded " + url + " in " + downloadTime + " ms.");
        }
        /*if (downloadTime > CACHE_DOWNLOAD_TIME) {
            try {
                //in case last request was not served from cache (determined by download time) we should wait a bit
                //not to abuse target server
                Thread.sleep((MINIMUM_SLEEP_TIME_SECONDS + (new Random().nextInt(VARIABLE_SLEEP_TIME_SECONDS))) * 1000);
            } catch (InterruptedException e) {
                LOG.log(Level.SEVERE, "Exception while giving server some rest");
            }
        }*/
        journalProcessedCounter++;
        return result;
    }

    public HttpClient getHttpClient() {
        return httpClient;
    }

    public void setHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }
}
