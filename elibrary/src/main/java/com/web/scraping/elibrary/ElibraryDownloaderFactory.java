package com.web.scraping.elibrary;

import com.web.scraping.helpers.Downloader;
import com.web.scraping.helpers.DownloaderFactory;
import org.apache.http.client.HttpClient;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/19/13
 * Time: 9:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class ElibraryDownloaderFactory implements DownloaderFactory {

    @Override
    public Downloader newInstance(HttpClient httpClient) {
        return new ElibraryDownloader(httpClient);
    }
}
