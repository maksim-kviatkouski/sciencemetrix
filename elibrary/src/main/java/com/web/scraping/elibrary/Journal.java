package com.web.scraping.elibrary;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 7/21/13
 * Time: 6:17 PM
 * To change this template use File | Settings | File Templates.
 */

//DONE: implement extraction of editorial board
//DONE: output editorial board
public class Journal {
    public static final String CATEGORY_MEMBERS_DELIMITER = ":";
    public static final String MEMBERS_DELIMITER = "|";
    public static final String CATEGORIES_DELIMITER = ">>";
    private String title;//done
    private String publisher;//done
    private String foundationYear;//done
    private String pubsPerYear;//done
    private String articlesPerPub;//done
    private String isReviewed;//done //translated
    private String impactJCR;//done //translated
    private String impactRIOSQ_2011;//done //translated
    private String abbreviation;//done
    private String city;//done //translated
    private String country;//done //translated
    private String region;//done //translated
    private String pISSN;//done
    private String subscriberIndex;//done
    private String circulation;//done
    private String eISSN;//done
    private String webLink;//done
    private String representation;//done
    private String isi;//done
    private String scopus;//done
    private String isInRIOSQ;//done //translated
    private String isInHAC;//done //translated
    private String totalArticles;//done
    private String totalPublications;//done
    private String totalFullTexts;//done
    private String totalQuotations;//done
    private String isPublishedNow;//done //translated
    private String archiveTimeframeStart;//done
    private String archiveTimeframeEnd;//
    private String isAbstract;//done
    private String isMultidisciplinary;//done
    private Collection<Rubric> rubrics;//done
    private String description;//done
    private String url;

    public Map<String, Collection<String>> getEditorialBoard() {
        return editorialBoard;
    }

    public void setEditorialBoard(Map<String, Collection<String>> editorialBoard) {
        this.editorialBoard = editorialBoard;
    }

    public String getArchiveTimeframeStart() {
        return archiveTimeframeStart;
    }

    private Map<String, Collection<String>> editorialBoard = new HashMap<String, Collection<String>>();

    public String toTabDelimitedString() {
        return url + '\t' +
                title + '\t' +
                publisher + '\t' +
                foundationYear + '\t' +
                pubsPerYear + '\t' +
                articlesPerPub + '\t' +
                isReviewed + '\t' +
                impactJCR + '\t' +
                impactRIOSQ_2011 + '\t' +
                abbreviation + '\t' +
                city + '\t' +
                country + '\t' +
                region + '\t' +
                pISSN + '\t' +
                subscriberIndex + '\t' +
                circulation + '\t' +
                eISSN + '\t' +
                webLink + '\t' +
                representation + '\t' +
                isi + '\t' +
                scopus + '\t' +
                isInRIOSQ + '\t' +
                isInHAC + '\t' +
                totalArticles + '\t' +
                totalPublications + '\t' +
                totalFullTexts + '\t' +
                totalQuotations + '\t' +
                isPublishedNow + '\t' +
                archiveTimeframeStart + '\t' +
                archiveTimeframeEnd + '\t' +
                isAbstract + '\t' +
                isMultidisciplinary + '\t' +
                description + '\t' +
                printEditorialBoard();
    }

    private String printEditorialBoard() {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<String, Collection<String>> e : editorialBoard.entrySet()) {
            result.append(e.getKey() + CATEGORY_MEMBERS_DELIMITER);
            for (String editor : e.getValue()) result.append(editor + MEMBERS_DELIMITER);
            if (result.length() > 0)
                result = result.replace(result.length() - MEMBERS_DELIMITER.length(), result.length(), "");
            result.append(CATEGORIES_DELIMITER);
        }
        if (result.length() > 0)
            result = result.replace(result.length() - CATEGORIES_DELIMITER.length(), result.length(), "");
        return result.toString();
    }

    public static String tabDelimiterHeader() {
        return "url\t" +
                "title\t" +
                "publisher\t" +
                "foundationYear\t" +
                "pubsPerYear\t" +
                "articlesPerPub\t" +
                "isReviewed\t" +
                "impactJCR\t" +
                "impactRIOSQ_2011\t" +
                "abbreviation\t" +
                "city\t" +
                "country\t" +
                "region\t" +
                "pISSN\t" +
                "subscriberIndex\t" +
                "circulation\t" +
                "eISSN\t" +
                "webLink\t" +
                "representation\t" +
                "isi\t" +
                "scopus\t" +
                "isInRIOSQ\t" +
                "isInHAC\t" +
                "totalArticles\t" +
                "totalPublications\t" +
                "totalFullTexts\t" +
                "totalQuotations\t" +
                "isPublishedNow\t" +
                "archiveTimeframeStart\t" +
                "archiveTimeframeEnd\t" +
                "isAbstract\t" +
                "isMultidisciplinary\t" +
                "description\t" +
                "editorial board";

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTotalPublications() {
        return totalPublications;
    }

    public void setTotalPublications(String totalPublications) {
        this.totalPublications = totalPublications;
    }

    public String getTotalFullTexts() {
        return totalFullTexts;
    }

    public void setTotalFullTexts(String totalFullTexts) {
        this.totalFullTexts = totalFullTexts;
    }

    public String getTotalQuotations() {
        return totalQuotations;
    }

    public void setTotalQuotations(String totalQuotations) {
        this.totalQuotations = totalQuotations;
    }

    public String getPublishedNow() {
        return isPublishedNow;
    }

    public void setPublishedNow(String publishedNow) {
        isPublishedNow = publishedNow;
    }

    public String getArchiveTimeframe() {
        return archiveTimeframeStart;
    }

    public void setArchiveTimeframeStart(String archiveTimeframe) {
        this.archiveTimeframeStart = archiveTimeframe;
    }

    public String getArchiveTimeframeEnd() {
        return archiveTimeframeEnd;
    }

    public void setArchiveTimeframeEnd(String archiveTimeframeEnd) {
        this.archiveTimeframeEnd = archiveTimeframeEnd;
    }

    public String getAbstract() {
        return isAbstract;
    }

    public void setAbstract(String anAbstract) {
        isAbstract = anAbstract;
    }

    public String getMultidisciplinary() {
        return isMultidisciplinary;
    }

    public void setMultidisciplinary(String multidisciplinary) {
        isMultidisciplinary = multidisciplinary;
    }

    public Collection<Rubric> getRubrics() {
        return rubrics;
    }

    public void setRubrics(Collection<Rubric> rubrics) {
        this.rubrics = rubrics;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getFoundationYear() {
        return foundationYear;
    }

    public void setFoundationYear(String foundationYear) {
        this.foundationYear = foundationYear;
    }

    public String getPubsPerYear() {
        return pubsPerYear;
    }

    public void setPubsPerYear(String pubsPerYear) {
        this.pubsPerYear = pubsPerYear;
    }

    public String getArticlesPerPub() {
        return articlesPerPub;
    }

    public void setArticlesPerPub(String articlesPerPub) {
        this.articlesPerPub = articlesPerPub;
    }

    public String getReviewed() {
        return isReviewed;
    }

    public void setReviewed(String reviewed) {
        isReviewed = reviewed;
    }

    public String getImpactJCR() {
        return impactJCR;
    }

    public void setImpactJCR(String impactJCR) {
        this.impactJCR = impactJCR;
    }

    public String getImpactRIOSQ_2011() {
        return impactRIOSQ_2011;
    }

    public void setImpactRIOSQ_2011(String impactRIOSQ_2011) {
        this.impactRIOSQ_2011 = impactRIOSQ_2011;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getpISSN() {
        return pISSN;
    }

    public void setpISSN(String pISSN) {
        this.pISSN = pISSN;
    }

    public String getSubscriberIndex() {
        return subscriberIndex;
    }

    public void setSubscriberIndex(String subscriberIndex) {
        this.subscriberIndex = subscriberIndex;
    }

    public String getCirculation() {
        return circulation;
    }

    public void setCirculation(String circulation) {
        this.circulation = circulation;
    }

    public String geteISSN() {
        return eISSN;
    }

    public void seteISSN(String eISSN) {
        this.eISSN = eISSN;
    }

    public String getWebLink() {
        return webLink;
    }

    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }

    public String getRepresentation() {
        return representation;
    }

    public void setRepresentation(String representation) {
        this.representation = representation;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getScopus() {
        return scopus;
    }

    public void setScopus(String scopus) {
        this.scopus = scopus;
    }

    public String getInRIOSQ() {
        return isInRIOSQ;
    }

    public void setInRIOSQ(String inRIOSQ) {
        isInRIOSQ = inRIOSQ;
    }

    public String getInHAC() {
        return isInHAC;
    }

    public void setInHAC(String inHAC) {
        isInHAC = inHAC;
    }

    public String getTotalArticles() {
        return totalArticles;
    }

    public void setTotalArticles(String totalArticles) {
        this.totalArticles = totalArticles;
    }
}
