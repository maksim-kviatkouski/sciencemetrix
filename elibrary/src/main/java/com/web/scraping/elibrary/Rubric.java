package com.web.scraping.elibrary;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/3/13
 * Time: 7:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class Rubric {
    private String code = "";
    private String section = "";
    private String totalJournals = "";
    private String journalId = "";

    public String getJournalId() {
        return journalId;
    }

    public void setJournalId(String journalId) {
        this.journalId = journalId;
    }

    public Rubric() {
    }

    public Rubric(String code, String section, String totalJournals) {
        this.code = code;
        this.section = section;
        this.totalJournals = totalJournals;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getTotalJournals() {
        return totalJournals;
    }

    public void setTotalJournals(String totalJournals) {
        this.totalJournals = totalJournals;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rubric rubric = (Rubric) o;

        if (!code.equals(rubric.code)) return false;
        if (!section.equals(rubric.section)) return false;
        if (!totalJournals.equals(rubric.totalJournals)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code.hashCode();
        result = 31 * result + section.hashCode();
        result = 31 * result + totalJournals.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Rubric{" +
                "code='" + code + '\'' +
                ", section='" + section + '\'' +
                ", totalJournals='" + totalJournals + '\'' +
                '}';
    }

    //journal URL at elibrary is used as an ID so mistmatch in toTabDelimitedString and tabDelimitedHeader is correct
    public String toTabDelimitedString() {
        return journalId + "\t" +
                code + "\t" +
                section + "\t" +
                totalJournals + "\t";
    }

    public static String tabDelimiterHeader() {
        return "Journal URL\t" +
                "Code\t" +
                "Section\t" +
                "Total journals in the rubric";
    }

    public boolean isEmpty() {
        return "".equals(code) && "".equals(totalJournals) && "".equals(section);
    }

}
