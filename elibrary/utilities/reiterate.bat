echo "Restarting apache"
"D:\Work\server\apache24\bin\httpd.exe" -k stop
del D:\Work\server\apache24\logs\error.log
del D:\Work\server\apache24\logs\access.log
"D:\Work\server\apache24\bin\httpd.exe" -k start
echo "Cleaning up cache"
call "D:\Work\server\apache24\bin\cleanElib.bat"
echo "Launching app"
java -Dhttp.proxyHost=localhost -Dhttp.proxyPort=80  -jar ../out/artifacts/elibrary_jar/elibrary.jar > out.txt 2>&1