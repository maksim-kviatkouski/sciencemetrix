import com.web.scraping.ScrapeHelper;
import com.web.scraping.model.FieldsAnalyzer;
import com.web.scraping.model.Journal;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        List<Journal> allJournals = Collections.synchronizedList(new LinkedList<Journal>());
        Set<Journal> dedupedJournals = Collections.synchronizedSet(new HashSet<Journal>());
        System.out.println("Getting a map of Categories to Subcategories");
        Map<String, List<String>> catsSubcats = ScrapeHelper.getCategoriesAndSubcategories();

        System.out.println("Starting to process categories and subcatogories");
        int journals_processed = 0;
        for (Map.Entry<String, List<String>> entry : catsSubcats.entrySet()){
            String cat = entry.getKey();
            int debug_count = 0;
            System.out.println("Starting to process category with number " + cat + " and its categories");
            for (String subcat : entry.getValue()){
                ScrapeHelper.addJournalsAsynchronously(cat, subcat,dedupedJournals);
            }
        }

        ScrapeHelper.waitForDownloads();

        System.out.println("Total journals found: " + dedupedJournals.size());
       // System.out.println("Total journals found: " + allJournals.size());
        long id = 1000000;
        OutputStreamWriter output = null;
        OutputStreamWriter avail = null;
        OutputStreamWriter abbrev = null;
        OutputStreamWriter alter = null;
        OutputStreamWriter trans = null;
        OutputStreamWriter history = null;
        try {
            output = new OutputStreamWriter(new FileOutputStream("journals.txt"));
            avail = new OutputStreamWriter(new FileOutputStream("availabilities.txt"));
            abbrev = new OutputStreamWriter(new FileOutputStream("abbreviations.txt"));
            alter = new OutputStreamWriter(new FileOutputStream("alternatives.txt"));
            trans = new OutputStreamWriter(new FileOutputStream("translations.txt"));
            history = new OutputStreamWriter(new FileOutputStream("history.txt"));
            output.write("ID\tTitle\tISSN\tPeer-reviewed\tLCCN\tPinyin\tCODEN\tUniform\r\n");
            avail.write("ID\tTitle\tISSN\tWhere\tWhen\r\n");
            abbrev.write("ID\tTitle\tISSN\tAbbreviation\r\n");
            alter.write("ID\tTitle\tISSN\tAlternative\r\n");
            trans.write("ID\tTitle\tISSN\tTranslation\r\n");
            history.write("ID\tTitle\tISSN\tRelation\tSubject\r\n");

            //dedupedJournals.addAll(allJournals);
            Iterator<Journal> jIterator = dedupedJournals.iterator();
            while (jIterator.hasNext()){
                Journal journal = jIterator.next();
                FieldsAnalyzer.loadJournalFields(journal.getAvailableFields());
                journal.setId(id++);
                output.write(journal.singleValuedToTabDelimited()+"\r\n");
                for (int j = 0; j < journal.getAvalabilities().size(); j++){
                    avail.write(journal.getAvailabilityItemTabDelimited(j)+"\r\n");
                }
                for (int j = 0; j < journal.getAbbrev().size(); j++){
                    abbrev.write(journal.getAbbrevsTabDelimited(j)+"\r\n");
                }
                for (int j = 0; j < journal.getAlternatives().size(); j++){
                    alter.write(journal.getAlternativesTabDelimited(j)+"\r\n");
                }
                for (int j = 0; j < journal.getTranslations().size(); j++){
                    trans.write(journal.getTranslationTabDelimited(j)+"\r\n");
                }
                for (int j = 0; j < journal.getHistory().size(); j++){
                    history.write(journal.getHistoryTabDelimited(j)+"\r\n");
                }
            }
            output.close();
            avail.close();
            abbrev.close();
            alter.close();
            trans.close();
            history.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println("Fields cardinality:");
        System.out.println(FieldsAnalyzer.getCardinalityMap());
        System.out.println("Done.");
    }
}
