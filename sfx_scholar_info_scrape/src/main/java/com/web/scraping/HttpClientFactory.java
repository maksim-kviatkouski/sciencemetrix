package com.web.scraping;

import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.protocol.HttpContext;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 2/28/13
 * Time: 11:31 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Helper class that provides internal logic of HttpClient objects pool
 */
public class HttpClientFactory implements PoolableObjectFactory<HttpClient> {
    private static String PROXY_HOST = "localhost";
    private static int PROXY_PORT = 9101;
    private static HttpHost proxy = new HttpHost(PROXY_HOST, PROXY_PORT);
    private static HttpRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy) {

        @Override
        public HttpRoute determineRoute(
                final HttpHost host,
                final HttpRequest request,
                final HttpContext context) throws HttpException {
            String hostname = host.getHostName();
            if (hostname.equals("127.0.0.1") || hostname.equalsIgnoreCase("localhost")) {
                // Return direct route
                return new HttpRoute(host);
            }
            return super.determineRoute(host, request, context);
        }
    };

    @Override
    public HttpClient makeObject() throws Exception {
        return HttpClients.custom()
                .setRoutePlanner(routePlanner)
                .build();
    }

    @Override
    public void destroyObject(HttpClient obj) throws Exception {
        return;
    }

    @Override
    public boolean validateObject(HttpClient obj) {
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void activateObject(HttpClient obj) throws Exception {
        return;
    }

    @Override
    public void passivateObject(HttpClient obj) throws Exception {
        return;
    }
}
