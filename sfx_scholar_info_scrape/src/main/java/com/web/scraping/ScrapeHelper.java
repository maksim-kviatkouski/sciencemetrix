package com.web.scraping;

import com.web.scraping.model.JounralExtractor;
import com.web.scraping.model.Journal;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.HttpRoutePlanner;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.protocol.HttpContext;
import org.cyberneko.html.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 2/25/13
 * Time: 10:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScrapeHelper {

    private static final XPathFactory xPathFactory = XPathFactory.newInstance();
    public static ObjectPool<HttpClient> httpClientsPool = new GenericObjectPool<HttpClient>(new HttpClientFactory(), 100, GenericObjectPool.WHEN_EXHAUSTED_BLOCK, -1);

    public static ExecutorService executorService = Executors.newFixedThreadPool(100);
    private static AtomicInteger journalCounter = new AtomicInteger(0);
    private static Set<String> processedLinks = Collections.synchronizedSet(new HashSet<String>());

    private static String PROXY_HOST = "localhost";
    private static int PROXY_PORT = 9101;
    private static HttpHost proxy = new HttpHost(PROXY_HOST, PROXY_PORT);
    private static HttpRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy) {

        @Override
        public HttpRoute determineRoute(
                final HttpHost host,
                final HttpRequest request,
                final HttpContext context) throws HttpException {
            String hostname = host.getHostName();
            if (hostname.equals("127.0.0.1") || hostname.equalsIgnoreCase("localhost")) {
                // Return direct route
                return new HttpRoute(host);
            }
            return super.determineRoute(host, request, context);
        }
    };

    private static CloseableHttpClient closeableHttpClient = HttpClients.custom()
            .setRoutePlanner(routePlanner)
            .build();



    private static String getCategotiesURL(String url)
    {
        String result = new String();
        String xPathQuery = "//FORM[@name='az_user_form']//TABLE/TBODY/TR/TD[@class='navigationTab'][2]//A/@href";

        Document doc;
        doc = getDocument(ScrapeDownloader.downloadLink(url, closeableHttpClient));
        result = queryValues(xPathQuery, doc).get(0);
        result = url + result;
        return result;

    }

    private static List<String> getCategories(String url){
        List<String> result = new LinkedList<String>();
        String xPathQuery = "//FORM[@name='az_user_form']//SELECT[@id='category_list']/OPTION/@value";
        Document doc;
        doc = getDocument(ScrapeDownloader.downloadLink(url, closeableHttpClient));
        result = queryValues(xPathQuery, doc);

        return result;
    }

    public static List<String> queryValues(String xPathQuery, Document doc) {
        List<String> result = new LinkedList<String>();
        XPath xPath = xPathFactory.newXPath();
        XPathExpression xPathExpression = null;
        NodeList nodeList = null;
        try {
            xPathExpression = xPath.compile(xPathQuery);
            nodeList = (NodeList) xPathExpression.evaluate(doc, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < nodeList.getLength(); i++){
            result.add(nodeList.item(i).getNodeValue());
        }
        return result;
    }

    public static String queryText(String xPathQuery, Node doc){
        String result = "";
        XPath xPath = xPathFactory.newXPath();
        XPathExpression xPathExpression = null;
        try {
            xPathExpression = xPath.compile(xPathQuery);
            result = (String) xPathExpression.evaluate(doc, XPathConstants.STRING);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Node queryNode(String xPathQuery, Node node){
        Node result = null;
        XPath xPath = xPathFactory.newXPath();
        XPathExpression xPathExpression = null;
        try {
            xPathExpression = xPath.compile(xPathQuery);
            result = (Node) xPathExpression.evaluate(node, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static NodeList queryNodes(String xPathQuery, Node node){
        NodeList result = null;
        XPath xPath = xPathFactory.newXPath();
        XPathExpression xPathExpression = null;
        try {
            xPathExpression = xPath.compile(xPathQuery);
            result = (NodeList) xPathExpression.evaluate(node, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Parses html page source into a DOM tree which can be queried then with XPath
     * @param html - html source of the page
     * @return DOM
     */
    public static Document getDocument(String html) {
        Document doc;DOMParser parser = new DOMParser();
        try {
            parser.setFeature("http://xml.org/sax/features/namespaces", false);
            parser.setFeature("http://cyberneko.org/html/features/scanner/ignore-specified-charset", true);
            parser.setProperty("http://cyberneko.org/html/properties/default-encoding", "UTF-8");
            parser.parse(new InputSource(new ByteArrayInputStream(html.getBytes())));
        } catch (SAXNotRecognizedException e) {
            e.printStackTrace();
        } catch (SAXNotSupportedException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        doc = parser.getDocument();
        return doc;
    }

    /**
     * Parses out subcategories from a category page (from right list of subcatgories)
     * @param html
     * @return
     */
    private static List<String> getSubCategories(String html){
        List<String> result = new LinkedList<String>();
        Document doc = getDocument(html);
        result = queryValues("//SELECT[@id='subcategory_list']/OPTION[position() > 1]/@value", doc);
        //subcategory --> param_vendor_value ?
     //   result = queryValues("//SELECT[@id='param_vendor_value']/OPTION/@value", doc);
        return result;
    }

    /**
     * Main method that builds map like CATEGORY NUMBER → [SUBCATEGORY1, SUBCATEGORY2, ....] parsing corresponding pages
     * @return
     */
    public static Map<String, List<String>> getCategoriesAndSubcategories(){
        Map<String, List<String>> result = new HashMap<String, List<String>>();
        String firstPage = "http://sfx.scholarsportal.info/guelph/az";
        String categoriesURL = getCategotiesURL(firstPage);
        List<String> categories = getCategories(categoriesURL);

        for (String cat : categories){
           if(!cat.equalsIgnoreCase("")) {
                String subCatURL = "http://sfx.scholarsportal.info/guelph/az?param_sid_save=cc9ed7707332a2c22bdb3e438db02fe3&param_chinese_checkbox_type_save=Pinyin&param_lang_save=eng&param_letter_group_save=&param_perform_save=searchCategories&param_letter_group_script_save=&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_current_view_save=table&param_jumpToPage_save=1&param_type_save=textSearch&param_textSearchType_save=contains&param_langcode_save=en&param_jumpToPage_value=&param_category_search_type_value=browseCategory&display_single_object=&param_category_value=" + cat + "&param_subcategory_active=1&param_perform_value=searchCategories";
                List<String> subCats = getSubCategories(ScrapeDownloader.downloadLink(subCatURL, closeableHttpClient));
                result.put(cat, subCats);
                System.out.println(cat + " : " + subCats.toString());
            }
        }
        return result;
    }

    public static String buildLinkToSubcategoryPage(String cat, String subcat){
        return "http://sfx.scholarsportal.info/guelph/az/default?&param_sid_save=1436c01b815739fe0abfe62594097a51&param_category_search_type_save=browseSubCategory&param_chinese_checkbox_type_save=Pinyin&param_lang_save=eng&param_letter_group_save=&param_perform_save=searchCategories&param_letter_group_script_save=&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_current_view_save=table&param_subcategory_save=" + subcat + "&param_jumpToPage_save=&param_type_save=textSearch&param_category_save=" + cat + "&param_textSearchType_save=contains";
    }

    /**
     * Parsing out number of pages in journals list from a piece lie "Showing page 3 of 27"
     * 27 is what is obtained from there
     * @param subCatHtml
     * @return
     */
    public static int getNumberOfSubPages(String subCatHtml){
        int result = 1;

        Document doc = getDocument(subCatHtml);
        String pages = queryText("//TABLE//TD[contains(text(),'Showing page')][1]/text()", doc);
        pages = pages.replaceAll("Showing page [0-9]* of ","").replaceAll(" pages", "").trim();
        try {
            result = Integer.valueOf(pages);
        } catch (NumberFormatException e){}
        System.out.println(result + " pages found in another subcategory of current category.");
        return result;
    }

    /**
     * Parsing out links that are downloaded when (i) button is pressed
     * @param cat category number
     * @param subcat subcategory number
     * @param i page number
     * @return
     */
    public static List<String> getLinksToJournalStickers(String cat, String subcat, int i) {
        List<String> result = new LinkedList<String>();

        String linkToPage = buildLinkToSubcategoryPage(cat, subcat) + "&param_jumpToPage_value=" + i;
        String html = ScrapeDownloader.downloadLink(linkToPage, closeableHttpClient);
        Document doc = getDocument(html);
        result = queryValues("//TABLE[@id='tableView']//TR/TD[3]/A[1]/@href", doc);

        return result;
    }

    /**
     * Same method as above but with ability to pass an HttpClient which can be used for multithreaded downloading using
     * a pool of HttpClient objects
     * @param cat
     * @param subcat
     * @param i
     * @param client
     * @return
     */
    public static List<String> getLinksToJournalStickers(String cat, String subcat, int i, HttpClient client) {
        List<String> result = new LinkedList<String>();

        String linkToPage = buildLinkToSubcategoryPage(cat, subcat) + "&param_jumpToPage_value=" + i;
        String html = ScrapeDownloader.downloadLink(linkToPage, client);
        Document doc = getDocument(html);
        result = queryValues("//TABLE[@id='tableView']//TR/TD[3]/A[1]/@href", doc);

        return result;
    }


    /**
     * This method is synchronous - it completes only when all intended journals are added
     * @param cat
     * @param subcat
     * @param allJournals
     */
    public static  void  addJournalsAsynchronously(String cat, String subcat, Collection<Journal> allJournals) {
        String linkToSubCatPage = ScrapeHelper.buildLinkToSubcategoryPage(cat, subcat);

        int num_of_pages = ScrapeHelper.getNumberOfSubPages(ScrapeDownloader.downloadLink(linkToSubCatPage, closeableHttpClient));
        for (int i = 0; i < num_of_pages; i++){
            processPage(cat, subcat, allJournals, i);
        }
    }

    /**
     * This metho is synchronous
     * @param cat
     * @param subcat
     * @param allJournals list that accumulates journals
     * @param i Page number
     */
    private static void processPage(final String cat, final String subcat, final Collection<Journal> allJournals, final int i) {
        try {
            final HttpClient client = httpClientsPool.borrowObject();


            final ExecutorService localExecutor = Executors.newFixedThreadPool(25);
                    try {
                        List<String> stickers = ScrapeHelper.getLinksToJournalStickers(cat, subcat, i, closeableHttpClient);

                        for (String stickerLink : stickers){
                            String link = "http://sfx.scholarsportal.info" + stickerLink;
                            if (!processedLinks.contains(link)){
                                //this method below is asynchronous. it uses HttpClient objects pool and ExecutorService
                                //to download many info stickers simultaneously and speed up scraping
                                processJournalSticker(link, allJournals, localExecutor);
                                processedLinks.add(link);
                            }
                        }
                        localExecutor.shutdown();
                        localExecutor.awaitTermination(10, TimeUnit.SECONDS);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            httpClientsPool.returnObject(client);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
            //this may be redundant since it was used when I tried to implement hyper multithreading (pages are processed
            //asynchronously as well as info stickers on those pages but it turned out to be quite difficult to synchronize
            //threads properly
            //However in case this call is redundant it makes no harm since it should return immediately
            localExecutor.awaitTermination(10, TimeUnit.SECONDS);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Downloads info sticker and parses out all required info into a Journal entity and adds it into resulting set of Journals
     * @param link
     * @param allJournals
     * @param executor
     */
    public static void processJournalSticker(final String link, final Collection<Journal> allJournals, final ExecutorService executor) {
        try {
            final HttpClient client = httpClientsPool.borrowObject();
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    Journal journal = JounralExtractor.extractJournal(ScrapeDownloader.downloadLink(link, client));
                    allJournals.add(journal);
                    if ((journalCounter.addAndGet(1) % 50) == 0){
                        System.out.println(journalCounter.get() + " journals processed");
                    }
                    try {
                        httpClientsPool.returnObject(client);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void waitForDownloads() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            executorService = Executors.newFixedThreadPool(200);
        }

    }
}
