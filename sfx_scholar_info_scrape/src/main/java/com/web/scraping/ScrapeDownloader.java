package com.web.scraping;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import java.io.IOException;
import java.net.URI;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 2/25/13
 * Time: 10:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class ScrapeDownloader {


    public static String downloadLink(String url){
        String result = "";
        try {
            result = IOUtils.toString(URI.create(url));
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return result;
    }

    public static String downloadLink(String url, HttpClient client){

        String result = "";
        HttpGet httpGet = new HttpGet(url);
        httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        httpGet.addHeader("Accept-Encoding", "gzip,deflate,sdch");
        httpGet.addHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
        httpGet.addHeader("Cache-Control", "max-age=0");
        httpGet.addHeader("Connection", "keep-alive");
        httpGet.addHeader("Host", "sfx.scholarsportal.info");
        httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36 u01-04");
        boolean done = false;
        int attempts = 0;


        while (!done && attempts++ < 10){
            try {
                HttpResponse response = client.execute(httpGet);
                result = IOUtils.toString(response.getEntity().getContent());
                httpGet.releaseConnection();
                done = true;
            } catch (IOException e) {
                System.out.println("Opps, something is wrong with link " + url + ". Retrying. " + e.getMessage());
            }
        }

        return result;
    }
}
