package com.web.scraping.model;

import com.web.scraping.ScrapeHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 2/26/13
 * Time: 7:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class JounralExtractor {

    public static Journal extractJournal(String html) {
        Journal result = new Journal();
        Document doc = ScrapeHelper.getDocument(html);

        extractTitle(result, doc);
        extractAbbrevs(result, doc);
        extractISSN(result, doc);
        extractLCCN(result, doc);
        extractPeerReviewed(result, doc);
        extractCoden(result, doc);
        extractAvailability(result, doc);
        extractAlternatives(result, doc);
        extractFieldTitles(result, doc);
        extractUniform(result, doc);
        extractTranslation(result, doc);
        extractPinyin(result, doc);
        extractHistory(result, doc);

        return result;
    }

    private static void extractPinyin(Journal result, Document doc) {
        String pinyin = ScrapeHelper.queryText("//TABLE//TABLE//TR/TD[contains(.,'Pinyin:')]/../TD[2]/text()", doc);
        if (pinyin!=null){
            result.setPinyin(pinyin.trim());
        }
    }

    private static void extractTranslation(Journal result, Document doc) {
        List<String> translations = ScrapeHelper.queryValues("//TABLE//TABLE//TR/TD[contains(.,'Translation:')]/../TD[2]/text()", doc);
        result.setTranslations(translations);
    }

    private static void extractAlternatives(Journal result, Document doc) {
        List<String> alternatives = ScrapeHelper.queryValues("//TABLE//TABLE//TR/TD[text()='Alternative:']/../TD[2]/text()", doc);
        result.setAlternatives(alternatives);
    }

    private static void extractHistory(Journal result, Document doc) {
        List<Journal.History> history = new LinkedList<Journal.History>();
        NodeList trsHistory = ScrapeHelper.queryNodes("//TABLE//TR[contains(./TD/B, 'Journal History:')]/following-sibling::TR/TD", doc);
        for (int i = 0; i < trsHistory.getLength(); i++){
            Node relationLink = ScrapeHelper.queryNode("./A", trsHistory.item(i));
            String relation = relationLink!=null ? relationLink.getTextContent().trim() : "";
            String subject = trsHistory.item(i).getTextContent();
            if (subject!=null) subject = subject.replaceAll("\\r\\n|\\r|\\n|\\t", "").trim().substring(relation.length()+1).trim().replaceAll("\\r\\n|\\r|\\n|\\t", "").replaceAll(" {2,}", " ").replaceAll("\\[", " [");
            history.add(new Journal.History(relation, subject));
        }

        result.setHistory(history);
    }

    private static void extractAvailability(Journal result, Document doc) {
        List<Journal.Avalability> avalabilities = new LinkedList<Journal.Avalability>();
        NodeList trsAvailability = ScrapeHelper.queryNodes("//TABLE//TABLE//TR/TD[contains(.,'Availability:')]/..", doc);
        for (int i = 0; i < trsAvailability.getLength(); i++){
            Node tdWhere = ScrapeHelper.queryNode("./TD[2]", trsAvailability.item(i));
            Node tdWhen = ScrapeHelper.queryNode("./following-sibling::TR[1]/TD[2]//TR[2]/TD", trsAvailability.item(i));
            String where = "";
            if (tdWhere!=null) where=tdWhere.getTextContent();
            String when = "";
            if (tdWhen!=null) when=tdWhen.getTextContent();
            if (where!=null) where=where.trim().replaceAll("\\r\\n|\\r|\\n", "").replaceAll(" {2,}", " ");
            if (when!=null) when=when.trim().replaceAll("\\r\\n|\\r|\\n", "").replaceAll(" {2,}", " ");
            avalabilities.add(new Journal.Avalability(where, when));
        }
        result.setAvalabilities(avalabilities);
    }

    private static void extractUniform(Journal result, Document doc) {
        String uniform = ScrapeHelper.queryText("//TABLE//TABLE//TR/TD[contains(.,'Uniform:')]/../TD[2]/text()", doc);
        if (uniform!=null){
            result.setUniform(uniform.trim());
        }
    }

    private static void extractCoden(Journal result, Document doc) {
        String coden = ScrapeHelper.queryText("//TABLE//TABLE//TR/TD[contains(.,'CODEN:')]/../TD[2]/text()", doc);
        if (coden != null){
            result.setCoden(coden.trim());
        }
    }

    private static void extractPeerReviewed(Journal result, Document doc) {
        String peerReviewed = ScrapeHelper.queryText("//TABLE//TABLE//TR/TD[contains(.,'Peer-Reviewed:')]/../TD[2]/text()", doc);
        if (peerReviewed != null){
            result.setPeerReviewed(peerReviewed.trim());
        }
    }

    private static void extractLCCN(Journal result, Document doc) {
        String lccn = ScrapeHelper.queryText("//TABLE//TABLE//TR/TD[text()='LCCN:']/../TD[2]/text()", doc);
        if (lccn != null){
            result.setLCCN(lccn.trim());
        }
    }

    private static void extractISSN(Journal result, Document doc) {
        String issn = ScrapeHelper.queryText("//TABLE//TABLE//TR/TD[text()='ISSN:']/../TD[2]/text()", doc);
        //   //TABLE//TBODY//TR/TD/TABLE/TBODY/TR/TD[text()='ISSN:']/../TD[2]/text()
        // //TABLE/../TD[text()='ISSN:']/../TD[2]/text()
        if (issn != null){
            result.setIssn(issn.trim());
        }
    }

    private static void extractTitle(Journal result, Document doc) {
        String title = ScrapeHelper.queryText("//TABLE//TABLE//TR/TD[text()='Title:']/../TD[2]/A", doc);
        if (title==null || "".equals(title)){
            title = ScrapeHelper.queryText("//TABLE//TABLE//TR/TD[text()='Title:']/../TD[2]/SPAN", doc);
        }
        if (title != null){
            result.setTitle(title.trim());
        }
    }

    private static void extractFieldTitles(Journal result, Document doc) {
        List<String> availableFields = ScrapeHelper.queryValues("//BODY/DIV/TABLE/descendant::TABLE[1]/TBODY/TR/TD[1]/text()", doc);
        List<String> filteredFields = new LinkedList<String>();
        for (String field : availableFields){
            if (field!=null && !"".equals(field.trim())){
                filteredFields.add(field.trim());
            }
        }
        result.setAvailableFields(filteredFields);
    }

    private static void extractAbbrevs(Journal result, Document doc) {
        List<String> abbrevs = ScrapeHelper.queryValues("//TABLE//TABLE//TR/TD[text()='Abbrev:']/../TD[2]/text()", doc);
        result.setAbbrev(abbrevs);
    }

}
