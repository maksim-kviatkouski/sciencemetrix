package com.web.scraping.model;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 2/26/13
 * Time: 12:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class Journal {
    private String title;
    private List<String> abbrev;
    private String issn;
    private String peerReviewed;
    private String LCCN;
    private String pinyin;
    private List<String> availableFields = new LinkedList<String>();
    private String coden;
    private List<String> alternatives;
    private String uniform;
    private List<String> translations;
    private List<History> history;
    private long id;
    private List<Avalability> avalabilities = new LinkedList<Avalability>();
    public List<History> getHistory() {
        return history;
    }
    private String translation;
    private String linkToSticker;


    public void setHistory(List<History> history) {
        this.history = history;
    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



    public List<Avalability> getAvalabilities() {
        return avalabilities;
    }

    public void setAvalabilities(List<Avalability> avalabilities) {
        this.avalabilities = avalabilities;
    }



    public List<String> getAvailableFields() {
        return availableFields;
    }

    public void setAvailableFields(List<String> availableFields) {
        this.availableFields = availableFields;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(List<String> abbrev) {
        this.abbrev = abbrev;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public String getPeerReviewed() {
        return peerReviewed;
    }

    public void setPeerReviewed(String peerReviewed) {
        this.peerReviewed = peerReviewed;
    }

    public String getLCCN() {
        return LCCN;
    }

    public void setLCCN(String LCCN) {
        this.LCCN = LCCN;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public String getLinkToSticker() {
        return linkToSticker;
    }

    public void setLinkToSticker(String linkToSticker) {
        this.linkToSticker = linkToSticker;
    }



    @Override
    public String toString() {
        return "Journal{" +
                "title='" + title + '\'' +
                ", abbrev=" + abbrev +
                ", issn='" + issn + '\'' +
                ", peerReviewed='" + peerReviewed + '\'' +
                ", LCCN='" + LCCN + '\'' +
                ", pinyin='" + pinyin + '\'' +
                ", coden='" + coden + '\'' +
                ", alternatives=" + alternatives +
                ", uniform='" + uniform + '\'' +
                ", translations=" + translations +
                ", history=" + history +
                ", id=" + id +
                ", avalabilities=" + avalabilities +
                ", translation='" + translation + '\'' +
                '}';
    }

    /**
     * Prints out tab-delimited fields which cardinality is 1 (not multivalued)
     * @return
     */
    public String singleValuedToTabDelimited() {
        return  Long.toString(id) + "\t" +
                title + "\t" +
                issn + "\t" +
                peerReviewed + "\t" +
                LCCN + "\t" +
                pinyin + "\t" +
                coden + "\t" +
                uniform;
    }

    public String getAvailabilityItemTabDelimited(int i){
        return Long.toString(id) + "\t" +
                title + "\t" +
                issn + "\t" + avalabilities.get(i).toTabDelimited();
    }

    public String getAbbrevsTabDelimited(int i){
        return Long.toString(id) + "\t" + title + "\t" + issn + "\t" + abbrev.get(i);
    }

    public String getAlternativesTabDelimited(int i){
        return Long.toString(id) + "\t" + title + "\t" + issn + "\t" + alternatives.get(i);
    }

    public String getTranslationTabDelimited(int i){
        return Long.toString(id) + "\t" + title + "\t" + issn + "\t" + translations.get(i);
    }

    public String getHistoryTabDelimited(int i){
        return Long.toString(id) + "\t" + title + "\t" + issn + "\t" + history.get(i).getRelation() + "\t" + history.get(i).getSubject();
    }

    public void setCoden(String coden) {
        this.coden = coden;
    }

    public String getCoden() {
        return coden;
    }

    public void setAlternatives(List<String> alternatives) {
        this.alternatives = alternatives;
    }

    public List<String> getAlternatives() {
        return alternatives;
    }

    public void setUniform(String uniform) {
        this.uniform = uniform;
    }

    public String getUniform() {
        return uniform;
    }

    public void setTranslations(List<String> translations) {
        this.translations = translations;
    }

    public List<String> getTranslations() {
        return translations;
    }

    /**
     * Considering two journals equal as soon as they have their titles and ISSNs equal
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Journal journal = (Journal) o;

        if (issn != null ? !issn.equals(journal.issn) : journal.issn != null) return false;
        if (title != null ? !title.equals(journal.title) : journal.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (issn != null ? issn.hashCode() : 0);
        return result;
    }

    public static class Avalability {
        private String where;
        private String when;

        public Avalability(String where, String when) {
            this.where = where;
            this.when = when;
        }

        @Override
        public String toString() {
            return "Avalability{" +
                    "where='" + where + '\'' +
                    ", when='" + when + '\'' +
                    '}';
        }

        public String toTabDelimited() {
            return where + "\t" + when;
        }

        public String getWhere() {
            return where;
        }

        public void setWhere(String where) {
            this.where = where;
        }

        public String getWhen() {
            return when;
        }

        public void setWhen(String when) {
            this.when = when;
        }
    }

    public static class History {
        private String relation;
        private String subject;

        public History(String relation, String subject) {
            this.relation = relation;
            this.subject = subject;
        }

        public String getRelation() {
            return relation;
        }

        public void setRelation(String relation) {
            this.relation = relation;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        @Override
        public String toString() {
            return "History{" +
                    "relation='" + relation + '\'' +
                    ", subject='" + subject + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            History history = (History) o;

            if (relation != null ? !relation.equals(history.relation) : history.relation != null) return false;
            if (subject != null ? !subject.equals(history.subject) : history.subject != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = relation != null ? relation.hashCode() : 0;
            result = 31 * result + (subject != null ? subject.hashCode() : 0);
            return result;
        }
    }
}
