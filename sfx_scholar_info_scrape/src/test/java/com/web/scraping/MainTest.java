package com.web.scraping;

import com.web.scraping.model.JounralExtractor;
import com.web.scraping.model.Journal;
import org.junit.Test;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/4/13
 * Time: 11:33 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Test class I used to check following:
 * 1. Encoding of output classes
 * 2. Deduplication
 * 3. Handling of advanced paging
 */
public class MainTest {

    private static final String TEST_JOURNAL_URL = "http://sfx.scholarsportal.info/guelph/cgi/core/azlist_ver3/a-z_retrieve.cgi/default?&param_sid_save=45315b0c8317df9091b4a84ee9591c61&param_letter_group_script_save=&param_issn_save=&param_current_view_save=table&param_textSearchType_save=contains&param_locate_category_save=14&param_lang_save=eng&param_chinese_checkbox_type_save=Pinyin&param_perform_save=locate&param_letter_group_save=&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_pattern_save=&param_jumpToPage_save=&param_type_save=textSearch&param_langcode_save=en&param_vendor_save=110976704639514&param_ui_control_scripts_save=&&lang=eng&function=get_info&obj_id=958480199244&issn=0163-5727&profile=default&exclude_issn=&exclude_auth=&exclude_note=&exclude_lccn=&exclude_coden=&exclude_availability=&exclude_categories=1";
    private static final Journal TEST_JOURNAl = JounralExtractor.extractJournal(ScrapeDownloader.downloadLink(TEST_JOURNAL_URL));

    private static final String INCORRECT_ISSN_TEST_LINK = "http://sfx.scholarsportal.info/guelph/cgi/core/azlist_ver3/a-z_retrieve.cgi/default?&param_sid_save=d49a38a3d8204b45d0577466c2a08c05&param_letter_group_script_save=&param_current_view_save=table&param_textSearchType_save=contains&param_lang_save=eng&param_chinese_checkbox_type_save=Pinyin&param_perform_save=searchTitle&param_letter_group_save=&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_pattern_save=The%20Discount%20merchandiser&param_starts_with_browse_save=0&param_jumpToPage_save=&param_type_save=textSearch&param_langcode_save=en&param_ui_control_scripts_save=&&lang=eng&function=get_info&obj_id=954921341137&issn=0012-3579&profile=default&exclude_issn=&exclude_auth=&exclude_note=&exclude_lccn=&exclude_coden=&exclude_availability=&exclude_categories=1";
    private static final Journal INCORRECT_ISSN_TEST_JOURNAL  = JounralExtractor.extractJournal(ScrapeDownloader.downloadLink(INCORRECT_ISSN_TEST_LINK));

    @Test
    public void testUnicode() {
        List<Journal> journals = new LinkedList<Journal>();
        Set<Journal> jSet = new HashSet<Journal>();
        ExecutorService executor = Executors.newSingleThreadExecutor();
        ScrapeHelper.processJournalSticker("http://sfx.scholarsportal.info/guelph/cgi/core/azlist_ver3/a-z_retrieve.cgi/default?&param_sid_save=1436c01b815739fe0abfe62594097a51&param_category_search_type_save=browseSubCategory&param_letter_group_script_save=&param_issn_save=0021-4876&param_current_view_save=table&param_category_save=6&param_textSearchType_save=contains&param_locate_category_save=&param_lang_save=eng&param_chinese_checkbox_type_save=Pinyin&param_perform_save=locate&param_letter_group_save=&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_subcategory_save=309&param_pattern_save=&param_jumpToPage_save=&param_type_save=textSearch&param_vendor_save=&param_ui_control_scripts_save=&&lang=eng&function=get_info&obj_id=954925409780&issn=0021-4876&profile=default&exclude_issn=&exclude_auth=&exclude_note=&exclude_lccn=&exclude_coden=&exclude_availability=&exclude_categories=1",
                journals, executor);
        ScrapeHelper.processJournalSticker("http://sfx.scholarsportal.info/guelph/cgi/core/azlist_ver3/a-z_retrieve.cgi/default?&param_sid_save=1436c01b815739fe0abfe62594097a51&param_category_search_type_save=browseSubCategory&param_letter_group_script_save=&param_issn_save=1392-8619&param_current_view_save=table&param_category_save=6&param_textSearchType_save=contains&param_locate_category_save=&param_lang_save=eng&param_chinese_checkbox_type_save=Pinyin&param_perform_save=locate&param_letter_group_save=&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_subcategory_save=309&param_pattern_save=&param_jumpToPage_save=&param_type_save=textSearch&param_vendor_save=&param_ui_control_scripts_save=&&lang=eng&function=get_info&obj_id=991042728071898&issn=1392-8619&profile=default&exclude_issn=&exclude_auth=&exclude_note=&exclude_lccn=&exclude_coden=&exclude_availability=&exclude_categories=1",
                journals, executor);
        ScrapeHelper.processJournalSticker("http://sfx.scholarsportal.info/guelph/cgi/core/azlist_ver3/a-z_retrieve.cgi/default?&param_sid_save=1436c01b815739fe0abfe62594097a51&param_category_search_type_save=browseSubCategory&param_letter_group_script_save=&param_issn_save=1710-209X&param_current_view_save=table&param_category_save=6&param_textSearchType_save=contains&param_locate_category_save=&param_lang_save=eng&param_chinese_checkbox_type_save=Pinyin&param_perform_save=locate&param_letter_group_save=&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_subcategory_save=309&param_pattern_save=&param_jumpToPage_save=&param_type_save=textSearch&param_vendor_save=&param_ui_control_scripts_save=&&lang=eng&function=get_info&obj_id=1000000000319433&issn=1710-209X&profile=default&exclude_issn=&exclude_auth=&exclude_note=&exclude_lccn=&exclude_coden=&exclude_availability=&exclude_categories=1",
                journals, executor);
        ScrapeHelper.processJournalSticker("http://sfx.scholarsportal.info/guelph/cgi/core/azlist_ver3/a-z_retrieve.cgi/default?&param_sid_save=1436c01b815739fe0abfe62594097a51&param_category_search_type_save=browseSubCategory&param_letter_group_script_save=&param_issn_save=0901-9936&param_current_view_save=table&param_category_save=6&param_textSearchType_save=contains&param_locate_category_save=&param_lang_save=eng&param_chinese_checkbox_type_save=Pinyin&param_perform_save=locate&param_letter_group_save=&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_subcategory_save=309&param_pattern_save=&param_jumpToPage_save=&param_type_save=textSearch&param_vendor_save=&param_ui_control_scripts_save=&&lang=eng&function=get_info&obj_id=110978977743957&issn=0901-9936&profile=default&exclude_issn=&exclude_auth=&exclude_note=&exclude_lccn=&exclude_coden=&exclude_availability=&exclude_categories=1",
                journals, executor);
        ScrapeHelper.processJournalSticker("http://sfx.scholarsportal.info/guelph/cgi/core/azlist_ver3/a-z_retrieve.cgi/default?&param_sid_save=1436c01b815739fe0abfe62594097a51&param_category_search_type_save=browseSubCategory&param_letter_group_script_save=&param_issn_save=0901-9936&param_current_view_save=table&param_category_save=6&param_textSearchType_save=contains&param_locate_category_save=&param_lang_save=eng&param_chinese_checkbox_type_save=Pinyin&param_perform_save=locate&param_letter_group_save=&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_subcategory_save=309&param_pattern_save=&param_jumpToPage_save=&param_type_save=textSearch&param_vendor_save=&param_ui_control_scripts_save=&&lang=eng&function=get_info&obj_id=110978977743957&issn=0901-9936&profile=default&exclude_issn=&exclude_auth=&exclude_note=&exclude_lccn=&exclude_coden=&exclude_availability=&exclude_categories=1",
                journals, executor);
        executor.shutdown();
        try {
            executor.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        PrintStream out = null;
        try {
            out = new PrintStream("test.out.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        jSet.addAll(journals);


        try {
            OutputStreamWriter oWriter = new OutputStreamWriter(new FileOutputStream("ostream.txt"), "UTF-8");
            Iterator<Journal> jIterator = jSet.iterator();
            while (jIterator.hasNext()){
                Journal j = jIterator.next();
                String s = j.singleValuedToTabDelimited();
                oWriter.write(s + "\r\n");
            }
            oWriter.flush();
            oWriter.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        out.close();
    }


    @Test
    public void testTitle(){
        String expected = "SIGCAPH newsletter";
        assertEquals(expected,TEST_JOURNAl.getTitle());
    }

    @Test
    public void testAbbrev(){
        List<String> expected = new ArrayList<String>();
        expected.add("ACM SIGCAPH COMPUTERS AND THE PHYSICALLY HANDICAPPED ");
        expected.add("SIGCAPH COMPUT. PHYS. HANDICAP");
        assertEquals(expected, TEST_JOURNAl.getAbbrev());
    }

    @Test
    public void testIssn(){
        String expected = "0163-5727";
        assertEquals(expected,TEST_JOURNAl.getIssn());

        expected = "0012-3579";
        assertEquals(expected,INCORRECT_ISSN_TEST_JOURNAL.getIssn());
    }

    @Test
    public void testPeerReviewed(){
        String expected = "No";
        assertEquals(expected,TEST_JOURNAl.getPeerReviewed());
    }

   /* @Test
    public void testAvailavleFields(){
        List<String> expected = new LinkedList<String>();


        assertEquals(expected,TEST_JOURNAl.getAvailableFields());
    }*/

    @Test
    public void testHistory(){
        List<Journal.History> expected = new LinkedList<Journal.History>();
        expected.add(new Journal.History("Continued by","Accessibility and computing [1558-2337]"));
        assertEquals(expected, TEST_JOURNAl.getHistory());
    }

    @Test
    public void testCoden(){
        String expected = "SGNWD2";
        assertEquals(expected,TEST_JOURNAl.getCoden());
    }

    @Test
    public void equalsTest(){
        Journal j1 = JounralExtractor.extractJournal(ScrapeDownloader.downloadLink("http://sfx.scholarsportal.info/guelph/cgi/core/azlist_ver3/a-z_retrieve.cgi/default?&param_sid_save=ec8a165bcf9468cc74a8303ae5402ff4&param_letter_group_script_save=Latin&param_current_view_save=table&param_textSearchType_save=contains&param_lang_save=eng&param_chinese_checkbox_type_save=Pinyin&param_perform_save=searchTitle&param_letter_group_save=0%2C1%2C2%2C3%2C4%2C5%2C6%2C7%2C8%2C9&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_pattern_save=Virchows%20Archiv.%20B%2C%20Cell%20pathology&param_starts_with_browse_save=0&param_jumpToPage_save=&param_type_save=textSearch&param_langcode_save=en&param_ui_control_scripts_save=&&lang=eng&function=get_info&obj_id=954927655913&issn=0340-6075&profile=default&exclude_issn=&exclude_auth=&exclude_note=&exclude_lccn=&exclude_coden=&exclude_availability=&exclude_categories=1"));
        Journal j2 = JounralExtractor.extractJournal(ScrapeDownloader.downloadLink("http://sfx.scholarsportal.info/guelph/cgi/core/azlist_ver3/a-z_retrieve.cgi/default?&param_sid_save=ec8a165bcf9468cc74a8303ae5402ff4&param_letter_group_script_save=Latin&param_current_view_save=table&param_textSearchType_save=contains&param_lang_save=eng&param_chinese_checkbox_type_save=Pinyin&param_perform_save=searchTitle&param_letter_group_save=0%2C1%2C2%2C3%2C4%2C5%2C6%2C7%2C8%2C9&param_chinese_checkbox_save=0&param_services2filter_save=getFullTxt&param_pattern_save=Virchows%20Archiv.%20B%2C%20Cell%20pathology&param_starts_with_browse_save=0&param_jumpToPage_save=&param_type_save=textSearch&param_langcode_save=en&param_ui_control_scripts_save=&&lang=eng&function=get_info&obj_id=991042742026168&issn=&profile=default&exclude_issn=&exclude_auth=&exclude_note=&exclude_lccn=&exclude_coden=&exclude_availability=&exclude_categories=1"));
        assertNotEquals(j1,j2);

    }




}
