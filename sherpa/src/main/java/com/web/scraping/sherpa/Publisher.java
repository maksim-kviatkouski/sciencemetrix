package com.web.scraping.sherpa;

import java.util.Collection;
import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/29/13
 * Time: 7:45 PM
 * To change this template use File | Settings | File Templates.
 */
public class Publisher {
    private String url = "";
    private String name = "";
    private String country = "";
    private String numberOfJournals = "";
    private String romeoStatus = "";
    private Collection<String> copyright = Collections.EMPTY_LIST;
    private String updated = "";
    private String authorsPrePrint = "";
    private String authorsPostPrint = "";
    private String publisherVersion = "";
    private Collection<String> conditions = Collections.EMPTY_SET;
    private String mandatedOA = "";
    private String paidOpenAccess = "";
    //TODO: figure out what should be extracted - link, text, object 'policy' that will go into another table?
    private Collection<String> exceptionsToPolicy = Collections.EMPTY_LIST;
    private String romeoPublisherID = "";
    private String linkToThisPage = "";

    public static String getTabDelimitedHeader(){
        return "URL\t" +
                "Name\t" +
                "Country\t" +
                "Number of journals\t" +
                "Romeo status\t" +
                "Copyright\t" +
                "Updated\t" +
                "Authors pre-print\t" +
                "Authors post-print\t" +
                "Publishers version\t" +
                "Conditions\t" +
                "Mandated OA\t" +
                "Paid open access\t" +
                "Exceptions to policy\t" +
                "Romeo publisher ID\t" +
                "Link to this page";
    }

    public String toTabDelimitedString(){
        return url + '\t' +
                name + '\t' +
                country + '\t' +
                numberOfJournals + '\t' +
                romeoStatus + '\t' +
                printCollection(getCopyright()) + '\t' +
                updated + '\t' +
                authorsPrePrint + '\t' +
                authorsPostPrint + '\t' +
                publisherVersion + '\t' +
                printCollection(getConditions()) + '\t' +
                mandatedOA + '\t' +
                paidOpenAccess + '\t' +
                printCollection(getExceptionsToPolicy()) + '\t' +
                romeoPublisherID + '\t' +
                linkToThisPage;
    }

    @Override
    public String toString() {
        return "Publisher{" +
                "url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", numberOfJournals='" + numberOfJournals + '\'' +
                ", romeoStatus='" + romeoStatus + '\'' +
                ", copyright='" + copyright + '\'' +
                ", updated='" + updated + '\'' +
                ", authorsPrePrint='" + authorsPrePrint + '\'' +
                ", authorsPostPrint='" + authorsPostPrint + '\'' +
                ", publisherVersion='" + publisherVersion + '\'' +
                ", conditions=" + conditions +
                ", mandatedOA='" + mandatedOA + '\'' +
                ", paidOpenAccess='" + paidOpenAccess + '\'' +
                ", exceptionsToPolicy='" + exceptionsToPolicy + '\'' +
                ", romeoPublisherID='" + romeoPublisherID + '\'' +
                ", linkToThisPage='" + linkToThisPage + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Publisher)) return false;

        Publisher publisher = (Publisher) o;

        if (!authorsPostPrint.equals(publisher.authorsPostPrint)) return false;
        if (!authorsPrePrint.equals(publisher.authorsPrePrint)) return false;
        if (!conditions.equals(publisher.conditions)) return false;
        if (!copyright.equals(publisher.copyright)) return false;
        if (!country.equals(publisher.country)) return false;
        if (!exceptionsToPolicy.equals(publisher.exceptionsToPolicy)) return false;
        if (!linkToThisPage.equals(publisher.linkToThisPage)) return false;
        if (!mandatedOA.equals(publisher.mandatedOA)) return false;
        if (!name.equals(publisher.name)) return false;
        if (!numberOfJournals.equals(publisher.numberOfJournals)) return false;
        if (!paidOpenAccess.equals(publisher.paidOpenAccess)) return false;
        if (!publisherVersion.equals(publisher.publisherVersion)) return false;
        if (!romeoPublisherID.equals(publisher.romeoPublisherID)) return false;
        if (!romeoStatus.equals(publisher.romeoStatus)) return false;
        if (!updated.equals(publisher.updated)) return false;
        if (!url.equals(publisher.url)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = url.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + country.hashCode();
        result = 31 * result + numberOfJournals.hashCode();
        result = 31 * result + romeoStatus.hashCode();
        result = 31 * result + copyright.hashCode();
        result = 31 * result + updated.hashCode();
        result = 31 * result + authorsPrePrint.hashCode();
        result = 31 * result + authorsPostPrint.hashCode();
        result = 31 * result + publisherVersion.hashCode();
        result = 31 * result + conditions.hashCode();
        result = 31 * result + mandatedOA.hashCode();
        result = 31 * result + paidOpenAccess.hashCode();
        result = 31 * result + exceptionsToPolicy.hashCode();
        result = 31 * result + romeoPublisherID.hashCode();
        result = 31 * result + linkToThisPage.hashCode();
        return result;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getNumberOfJournals() {
        return numberOfJournals;
    }

    public void setNumberOfJournals(String numberOfJournals) {
        this.numberOfJournals = numberOfJournals;
    }

    public String getRomeoStatus() {
        return romeoStatus;
    }

    public void setRomeoStatus(String romeoStatus) {
        this.romeoStatus = romeoStatus;
    }

    public Collection<String> getCopyright() {
        return copyright;
    }

    public void setCopyright(Collection<String> copyright) {
        this.copyright = copyright;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getAuthorsPrePrint() {
        return authorsPrePrint;
    }

    public void setAuthorsPrePrint(String authorsPrePrint) {
        this.authorsPrePrint = authorsPrePrint;
    }

    public String getAuthorsPostPrint() {
        return authorsPostPrint;
    }

    public void setAuthorsPostPrint(String authorsPostPrint) {
        this.authorsPostPrint = authorsPostPrint;
    }

    public String getPublisherVersion() {
        return publisherVersion;
    }

    public void setPublisherVersion(String publisherVersion) {
        this.publisherVersion = publisherVersion;
    }

    public Collection<String> getConditions() {
        return conditions;
    }

    public void setConditions(Collection<String> conditions) {
        this.conditions = conditions;
    }

    public String getMandatedOA() {
        return mandatedOA;
    }

    public void setMandatedOA(String mandatedOA) {
        this.mandatedOA = mandatedOA;
    }

    public String getPaidOpenAccess() {
        return paidOpenAccess;
    }

    public void setPaidOpenAccess(String paidOpenAccess) {
        this.paidOpenAccess = paidOpenAccess;
    }

    public Collection<String> getExceptionsToPolicy() {
        return exceptionsToPolicy;
    }

    public void setExceptionsToPolicy(Collection<String> exceptionsToPolicy) {
        this.exceptionsToPolicy = exceptionsToPolicy;
    }

    public String getRomeoPublisherID() {
        return romeoPublisherID;
    }

    public void setRomeoPublisherID(String romeoPublisherID) {
        this.romeoPublisherID = romeoPublisherID;
    }

    public String getLinkToThisPage() {
        return linkToThisPage;
    }

    public void setLinkToThisPage(String linkToThisPage) {
        this.linkToThisPage = linkToThisPage;
    }

    private String printCollection(Collection<String> c) {
        StringBuilder result = new StringBuilder();

        for (String s : c) result.append(s + "|");
        if (!"".equals(result.toString())) result = result.replace(result.length() - 1, result.length(), "");

        return result.toString();
    }
}
