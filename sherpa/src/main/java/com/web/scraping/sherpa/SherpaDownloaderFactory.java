package com.web.scraping.sherpa;

import com.web.scraping.helpers.Downloader;
import com.web.scraping.helpers.DownloaderFactory;
import org.apache.http.client.HttpClient;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/20/13
 * Time: 9:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class SherpaDownloaderFactory implements DownloaderFactory {
    @Override
    public Downloader newInstance(HttpClient httpClient) {
        return new SherpaDownloader(httpClient);
    }
}
