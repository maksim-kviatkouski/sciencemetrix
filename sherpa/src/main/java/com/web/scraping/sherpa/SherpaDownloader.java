package com.web.scraping.sherpa;

import com.web.scraping.helpers.Downloader;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicHeader;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/20/13
 * Time: 8:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class SherpaDownloader implements Downloader{
    private HttpClient httpClient;
    private final static Logger LOG = Logger.getLogger(SherpaDownloader.class.getName());

    public SherpaDownloader(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public void init() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void doHandShaking() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private void mimicBrowserRequest(HttpRequest httpRequest) {
        httpRequest.setHeader(new BasicHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36"));
        httpRequest.setHeader(new BasicHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3"));
        httpRequest.setHeader(new BasicHeader("Host", "www.sherpa.ac.uk"));
        httpRequest.setHeader(new BasicHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"));
        httpRequest.setHeader(new BasicHeader("Accept-Encoding", "gzip, deflate"));
    }

    @Override
    public String download(String s) {
        String result = "";
        s = s.replaceAll("fIDnum=\\|", ""); //pipe seems to break RFC-2369. it's easier to stip it rather than to fight it
        try {
            HttpGet httpGet = new HttpGet(s);
            mimicBrowserRequest(httpGet);
            HttpResponse response = httpClient.execute(httpGet);
            result = IOUtils.toString(response.getEntity().getContent());
            LOG.info("Succesfully downloaded " + s);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Failed to download link " + s, e);
        } catch (IllegalArgumentException e) {
            LOG.log(Level.WARNING, "Something is wrong with link " + s, e);
        }
        return result;
    }

    @Override
    public HttpClient getHttpClient() {
        return httpClient;
    }
}
