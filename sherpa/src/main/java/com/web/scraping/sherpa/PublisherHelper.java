package com.web.scraping.sherpa;

import com.web.scraping.helpers.Downloader;
import com.web.scraping.helpers.ScrapeHelper;
import org.w3c.dom.Document;

import java.util.Collection;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.web.scraping.helpers.ScrapeHelper.queryTextValues;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/29/13
 * Time: 7:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class PublisherHelper extends JournalHelper{

    public PublisherHelper(Downloader d) {
        //super(d);
        this.d = d;
    }

    public Publisher getPublisher(String link) {
        Publisher result = new Publisher();
        Document doc = ScrapeHelper.getDocument(d.download(link));

        result.setUrl(link);
        result.setName(extractName(doc));
        result.setCountry(extractCountry(doc));
        result.setNumberOfJournals(extractNumberOfJournals(doc));
        result.setRomeoStatus(extractRomeoStatus(doc));
        result.setCopyright(extractCopyright(doc));
        result.setPaidOpenAccess(extractPaidOpenAccess(doc));
        result.setExceptionsToPolicy(extractExceptionsToPolicy(doc));
        result.setRomeoPublisherID(extractRomeoPublisherId(doc));
        result.setLinkToThisPage(extractLinkToThisPage(doc));
        result.setUpdated(extractUpdated(doc));
        result.setAuthorsPrePrint(extractAuthorsPrePrint(doc));
        result.setAuthorsPostPrint(extractAuthorsPostPrint(doc));
        result.setPublisherVersion(extractPublishersVersion(doc));
        result.setConditions(extractConditions(doc));
        result.setMandatedOA(extractMandatedOA(doc));

        return result;
    }

    private String extractName(Document doc) {
        String result = "";
        String nameLine = clean(ScrapeHelper.queryText("//TR/TD[contains(.,\"Publisher:\")]/following-sibling::TD[1]/A", doc));
        if ("".equals(nameLine)) {
            nameLine = clean(ScrapeHelper.queryText("//TR/TD[contains(.,\"Special Policy:\")]/following-sibling::TD[1]", doc));
            if (nameLine.split(":").length > 1) result = clean(nameLine.split(":")[0]);
        } else {
            result = clean(nameLine);
        }
        return result;
    }

    private String extractCountry(Document doc){
        String result = "";
        String countryLine = clean(ScrapeHelper.queryText("//TR/TD[contains(.,\"Publisher:\")]/following-sibling::TD[1]/SMALL", doc));
        if ("".equals(countryLine) || countryLine.split(",").length < 2) {
            countryLine = clean(ScrapeHelper.queryText("//TR/TD[contains(.,\"Special Policy:\")]/following-sibling::TD[1]/SMALL", doc));
        }
        if (!"".equals(countryLine) || countryLine.split(",").length >=2) {
            result = countryLine.split(",")[1];
        }
        return clean(result);
    }

    //TODO: figure out how to parse out this field properly since on the site it looks like "105 journals (including journals with special policies) - involving 8 other organisations"
    private String extractNumberOfJournals(Document doc) {
        String result = clean(ScrapeHelper.queryText("//TR/TD[contains(.,\"Journals:\")]/following-sibling::TD[1]/A", doc));
        Pattern numberOfJournalsPattern = Pattern.compile("^~?([0-9]+)");
        Matcher numberOfJournalsMatcher = numberOfJournalsPattern.matcher(result);
        if (numberOfJournalsMatcher.find()){
            result = numberOfJournalsMatcher.group(1);
        }
        return result;
    }

    private String extractRomeoStatus(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[contains(.,\"RoMEO:\")]/following-sibling::TD[1]", doc));
    }

    private Collection<String> extractCopyright(Document doc){
        Collection<String> result = new LinkedList<String>();
        for (String s : queryTextValues("//TR/TD[contains(.,\"Copyright:\")]/following-sibling::TD[1]//A/@href", doc))
            result.add(clean(s));
        return result;
    }

    private String extractPaidOpenAccess(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[contains(.,\"Paid Open Access:\")]/following-sibling::TD[1]//A/@href", doc));
    }

    private Collection<String> extractExceptionsToPolicy(Document doc){
        Collection<String> result = new TreeSet<String>();
        for (String s : queryTextValues("//TR/TD[contains(.,\"Exceptions to this policy:\")]/following-sibling::TD[1]//LI", doc))
            result.add(clean(s));
        return result;
    }

    private String extractRomeoPublisherId(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[contains(.,\"RoMEO Publisher ID:\")]/following-sibling::TD[1]", doc).split(" - ")[0]);
    }

    private String extractLinkToThisPage(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[contains(.,\"Link to this page:\")]/following-sibling::TD[1]//A/@href", doc));
    }

}
