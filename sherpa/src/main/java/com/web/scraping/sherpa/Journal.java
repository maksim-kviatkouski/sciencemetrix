package com.web.scraping.sherpa;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/21/13
 * Time: 8:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class Journal {
    private String title = "";
    private String issn = "";
    private String essn = "";
    private String romeoColour = "";
    private String notes = "";

    private String authorsPrePrint = "";
    private String authorsPostPrint = "";
    private String publishersVersion = "";
    private Collection<String> conditions = new HashSet<String>();
    private String mandatedOA = "";
    private Collection<String> extendedNotes = new HashSet<String>();
    private String copyright = "";
    private String updated = "";
    private String journalPageLink = "";
    private String publishedBy = "";
    private String publisherID = "";

    private String url = "";

    private Publisher publisher;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public static String tabDelimitedHeader() {
        return "Title\t" +
                "ISSN\t" +
                "ESSN\t" +
                "Romeo Colour\t" +
                "Notes\t" +
                "Authors pre-print\t" +
                "Author post-print\t" +
                "Publishers version\t" +
                "Conditions\t" +
                "Mandated OA\t" +
                "Extended notes\t" +
                "Copyright\t" +
                "Updated\t" +
                "Journal page link\t" +
                "Published by\t" +
                "Publisher ID\t" +
                "URL";
    }

    public String toTabDelimitedString() {
        return title + '\t' +
                issn + '\t' +
                essn + '\t' +
                romeoColour + '\t' +
                notes + '\t' +
                authorsPrePrint + '\t' +
                authorsPostPrint + '\t' +
                publishersVersion + '\t' +
                printCollection(getConditions()) + '\t' +
                mandatedOA + '\t' +
                printCollection(getExtendedNotes()) + '\t' +
                copyright + '\t' +
                updated + '\t' +
                journalPageLink + '\t' +
                publishedBy + '\t' +
                publisherID + '\t' +
                url;
    }

    private String printCollection(Collection<String> c) {
        StringBuilder result = new StringBuilder();

        for (String s : c) result.append(s + "|");
        if (!"".equals(result.toString())) result = result.replace(result.length() - 1, result.length(), "");

        return result.toString();
    }

    public String getPublisherID() {
        return publisherID;
    }

    public void setPublisherID(String publisherID) {
        this.publisherID = publisherID;
    }

    public String getPublisherLink() {
        return publisherLink;
    }

    public void setPublisherLink(String publisherLink) {
        this.publisherLink = publisherLink;
    }

    public String getPublishedBy() {
        return publishedBy;
    }

    public void setPublishedBy(String publishedBy) {
        this.publishedBy = publishedBy;
    }

    public String getJournalPageLink() {
        return journalPageLink;
    }

    public void setJournalPageLink(String journalPageLink) {
        this.journalPageLink = journalPageLink;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public Collection<String> getExtendedNotes() {
        return extendedNotes;
    }

    public void setExtendedNotes(Collection<String> extendedNotes) {
        this.extendedNotes = extendedNotes;
    }

    public String getMandatedOA() {
        return mandatedOA;
    }

    public void setMandatedOA(String mandatedOA) {
        this.mandatedOA = mandatedOA;
    }

    public Collection<String> getConditions() {
        return conditions;
    }

    public void setConditions(Collection<String> conditions) {
        this.conditions = conditions;
    }

    public String getPublishersVersion() {
        return publishersVersion;
    }

    public void setPublishersVersion(String publishersVersion) {
        this.publishersVersion = publishersVersion;
    }

    public String getAuthorsPostPrint() {
        return authorsPostPrint;
    }

    public void setAuthorsPostPrint(String authorsPostPrint) {
        this.authorsPostPrint = authorsPostPrint;
    }

    public String getAuthorsPrePrint() {
        return authorsPrePrint;
    }

    public void setAuthorsPrePrint(String authorsPrePrint) {
        this.authorsPrePrint = authorsPrePrint;
    }

    private String publisherLink = "";

    public Journal() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public String getEssn() {
        return essn;
    }

    public void setEssn(String essn) {
        this.essn = essn;
    }

    public String getRomeoColour() {
        return romeoColour;
    }

    public void setRomeoColour(String romeoColour) {
        this.romeoColour = romeoColour;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Journal)) return false;

        Journal journal = (Journal) o;

        if (authorsPostPrint != null ? !authorsPostPrint.equals(journal.authorsPostPrint) : journal.authorsPostPrint != null)
            return false;
        if (authorsPrePrint != null ? !authorsPrePrint.equals(journal.authorsPrePrint) : journal.authorsPrePrint != null)
            return false;
        if (conditions != null ? !conditions.equals(journal.conditions) : journal.conditions != null) return false;
        if (copyright != null ? !copyright.equals(journal.copyright) : journal.copyright != null) return false;
        if (essn != null ? !essn.equals(journal.essn) : journal.essn != null) return false;
        if (extendedNotes != null ? !extendedNotes.equals(journal.extendedNotes) : journal.extendedNotes != null)
            return false;
        if (issn != null ? !issn.equals(journal.issn) : journal.issn != null) return false;
        if (journalPageLink != null ? !journalPageLink.equals(journal.journalPageLink) : journal.journalPageLink != null)
            return false;
        if (mandatedOA != null ? !mandatedOA.equals(journal.mandatedOA) : journal.mandatedOA != null) return false;
        if (notes != null ? !notes.equals(journal.notes) : journal.notes != null) return false;
        if (publishedBy != null ? !publishedBy.equals(journal.publishedBy) : journal.publishedBy != null) return false;
        if (publisherLink != null ? !publisherLink.equals(journal.publisherLink) : journal.publisherLink != null)
            return false;
        if (publishersVersion != null ? !publishersVersion.equals(journal.publishersVersion) : journal.publishersVersion != null)
            return false;
        if (romeoColour != null ? !romeoColour.equals(journal.romeoColour) : journal.romeoColour != null) return false;
        if (title != null ? !title.equals(journal.title) : journal.title != null) return false;
        if (updated != null ? !updated.equals(journal.updated) : journal.updated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + (issn != null ? issn.hashCode() : 0);
        result = 31 * result + (essn != null ? essn.hashCode() : 0);
        result = 31 * result + (romeoColour != null ? romeoColour.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + (authorsPrePrint != null ? authorsPrePrint.hashCode() : 0);
        result = 31 * result + (authorsPostPrint != null ? authorsPostPrint.hashCode() : 0);
        result = 31 * result + (publishersVersion != null ? publishersVersion.hashCode() : 0);
        result = 31 * result + (conditions != null ? conditions.hashCode() : 0);
        result = 31 * result + (mandatedOA != null ? mandatedOA.hashCode() : 0);
        result = 31 * result + (extendedNotes != null ? extendedNotes.hashCode() : 0);
        result = 31 * result + (copyright != null ? copyright.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (journalPageLink != null ? journalPageLink.hashCode() : 0);
        result = 31 * result + (publishedBy != null ? publishedBy.hashCode() : 0);
        result = 31 * result + (publisherLink != null ? publisherLink.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Journal{" +
                "title='" + title + '\'' +
                ", issn='" + issn + '\'' +
                ", essn='" + essn + '\'' +
                ", romeoColour='" + romeoColour + '\'' +
                ", notes='" + notes + '\'' +
                ", authorsPrePrint='" + authorsPrePrint + '\'' +
                ", authorsPostPrint='" + authorsPostPrint + '\'' +
                ", publishersVersion='" + publishersVersion + '\'' +
                ", conditions=" + conditions +
                ", mandatedOA='" + mandatedOA + '\'' +
                ", extendedNotes=" + extendedNotes +
                ", copyright='" + copyright + '\'' +
                ", updated='" + updated + '\'' +
                ", journalPageLink='" + journalPageLink + '\'' +
                ", publishedBy='" + publishedBy + '\'' +
                ", publisherLink='" + publisherLink + '\'' +
                '}';
    }

}
