package com.web.scraping.sherpa;

import com.web.scraping.helpers.Downloader;
import com.web.scraping.helpers.DownloaderFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;

import java.io.*;
import java.net.ProxySelector;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

//TODO: implement global analysis of available fields
public class Main {
    private final static Logger LOG = Logger.getLogger(Main.class.getName());
    private static final String JOURNALS_FILE_NAME = "sherpa_journals_%s.txt";
    private static final String PUBLISHERS_FILE_NAME = "sherpa_publishers_%s.txt";
    private static final DateFormat FILE_NAME_DF = new SimpleDateFormat("yyyy-MM-dd--hh-mm-ss");
    public static final String ENCODING = "UTF-8";
    public static final String NEW_LINE = "\r\n";

    public static void main(String[] args) {

        if (args.length > 0 && "download".equals(args[0])) {
            LOG.info("Downloading journals based on a list of links");
            downloadAll();
        } else if (args.length > 0 && "makeFileWithLinks".equals(args[0])) {
            LOG.info("Make file with links");
            makeFileWithLinks();
        } else {
            LOG.info("Regular run");
            regularRun();
        }
    }


    private static void regularRun() {
        DownloaderFactory downloaderFactory = new SherpaDownloaderFactory();
        DefaultHttpClient httpClient = new DefaultHttpClient();
        ProxySelectorRoutePlanner routePlanner = new ProxySelectorRoutePlanner(httpClient.getConnectionManager().getSchemeRegistry(), ProxySelector.getDefault());
        httpClient.setRoutePlanner(routePlanner);
        Downloader d = downloaderFactory.newInstance(httpClient);
        JournalHelper jHelper = new JournalHelper(d);

        Collection<Journal> allJournals = jHelper.getAllJournals();
        Collection<Publisher> allPublishers = new HashSet<Publisher>();
        LOG.info("Extracted " + allJournals.size() + " journals");

        Date now = Calendar.getInstance().getTime();
        String journalsFileName = String.format(JOURNALS_FILE_NAME, FILE_NAME_DF.format(now));
        String publishersFileName = String.format(PUBLISHERS_FILE_NAME, FILE_NAME_DF.format(now));
        try {
            OutputStreamWriter journalsFile = new OutputStreamWriter(
                    new FileOutputStream(journalsFileName),
                    ENCODING
            );
            OutputStreamWriter publishersFile = new OutputStreamWriter(
                    new FileOutputStream(publishersFileName),
                    ENCODING
            );
            journalsFile.write(Journal.tabDelimitedHeader()+ NEW_LINE);
            for (Journal j : allJournals) {
                journalsFile.write(j.toTabDelimitedString() + NEW_LINE);
                allPublishers.add(j.getPublisher());
            }
            journalsFile.close();

            publishersFile.write(Publisher.getTabDelimitedHeader() + NEW_LINE);
            for (Publisher p : allPublishers) {
                publishersFile.write(p.toTabDelimitedString() + NEW_LINE);
            }
            publishersFile.close();
        } catch (UnsupportedEncodingException e) {
            LOG.log(Level.SEVERE, "It seems that encoding " + ENCODING + " is not supported in this system", e);
        } catch (FileNotFoundException e) {
            LOG.log(Level.SEVERE, "Could not find/open file " + journalsFileName + " or " + publishersFileName, e);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Error writing into file " + journalsFileName + " or " + publishersFileName, e);
        }

        LOG.info("Done.");
    }



    public static void dumpJournalLinks(Collection<String> links, String filename) {
        try {
            OutputStreamWriter dumpFileWriter = new OutputStreamWriter(new FileOutputStream(filename), ENCODING);
            for (String l : links) dumpFileWriter.write(l + NEW_LINE);
            dumpFileWriter.close();
        } catch (UnsupportedEncodingException e) {
            LOG.log(Level.WARNING, "Encoding is not supported", e);
        } catch (FileNotFoundException e) {
            LOG.log(Level.WARNING, "Could not fine/create file " + filename, e);
        } catch (IOException e) {
            LOG.log(Level.WARNING, "Could not close file " + filename, e);
        }
    }

    public static void makeFileWithLinks() {

        DownloaderFactory downloaderFactory = new SherpaDownloaderFactory();
        DefaultHttpClient httpClient = new DefaultHttpClient();
        ProxySelectorRoutePlanner routePlanner = new ProxySelectorRoutePlanner(httpClient.getConnectionManager().getSchemeRegistry(), ProxySelector.getDefault());
        httpClient.setRoutePlanner(routePlanner);
        Downloader d = downloaderFactory.newInstance(httpClient);
        JournalHelper jHelper = new JournalHelper(d);

        Collection<String> allLinks = jHelper.getAllJournalsLinks();
        dumpJournalLinks(allLinks, "links.txt");
    }


    public static void downloadAll() {
        DownloadManager dManager = new DownloadManager("links.txt", new SherpaDownloaderFactory());
        dManager.download();

    }
}
