package com.web.scraping.sherpa;

import com.web.scraping.helpers.Downloader;
import com.web.scraping.helpers.DownloaderFactory;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.impl.conn.SchemeRegistryFactory;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/2/13
 * Time: 8:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProxiedHttpClientsPoolFactory extends BasePoolableObjectFactory {
    private final Logger LOG = Logger.getLogger(ProxiedHttpClientsPoolFactory.class.getName());
    private List<HttpHost> proxies;
    private DownloaderFactory dFactory;
    private int index = 0;

    public ProxiedHttpClientsPoolFactory(List<HttpHost> proxies, DownloaderFactory dFactory) {
        this.proxies = proxies;
        this.dFactory = dFactory;
    }

    @Override
    public Object makeObject() throws InstantiationException {
        PoolingClientConnectionManager conman = new PoolingClientConnectionManager(
                SchemeRegistryFactory.createDefault(),
                30,
                TimeUnit.SECONDS
        );
        conman.setDefaultMaxPerRoute(100);
        conman.setMaxTotal(500);
        HttpClient httpClient = new DefaultHttpClient(conman);
        HttpHost proxy = proxies.get(index++ % proxies.size());
        //HttpContext
        httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
        httpClient.getParams().setParameter("http.socket.timeout", new Integer(30000));
        HttpClientParams.setConnectionManagerTimeout(httpClient.getParams(), 30000);
        Downloader result = dFactory.newInstance(httpClient);
        result.doHandShaking();
        LOG.info("Created http client with proxy " + proxy + ". " + index + " clients in total now");
        return result;
    }
}
