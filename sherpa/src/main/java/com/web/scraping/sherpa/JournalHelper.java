package com.web.scraping.sherpa;

import com.web.scraping.helpers.Downloader;
import com.web.scraping.helpers.ScrapeHelper;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.web.scraping.helpers.ScrapeHelper.*;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/20/13
 * Time: 9:06 PM
 * To change this template use File | Settings | File Templates.
 */
//TODO: http://www.sherpa.ac.uk/romeo/search.php?issn=1687-7632&type=issn&la=en&fIDnum=|&mode=simple do we need "Listed In" field?
//TODO: since there are much more pages to scrape do we need multithreading?
//TODO: think of cleaning up cache for pages that have "awaiting information" in their fields
public class JournalHelper {
    public static final String DOMAIN = "http://www.sherpa.ac.uk";
    public static final String ENTRY_PAGE = "/romeo/journalbrowse.php";
    public static final String ROMEO = "/romeo/";
    public static final String ENCODING = "UTF-8";
    private final Collection<String> allPublishers = new TreeSet<String>();
    private static int countJournalsProcessed = 0;
    //using only one downloader here instead of DownloadManager since at a stage of obtaining links to all journals
    //multithreaded downloading is a bit of overkill which would make implementation overcomplicated
    protected Downloader d;
    private final static Logger LOG = Logger.getLogger(JournalHelper.class.getName());
    private PublisherHelper pHelper;

    public JournalHelper(Downloader d) {
        this.d = d;
        pHelper = new PublisherHelper(d);
    }

    public JournalHelper() {
    }

    public Collection<String> getAllJournalsLinks(){
        Collection<String> result = new HashSet<String>();
        for (String letterPage : getAllLetters()){
            for (String subPage : getAllSubPages(letterPage)){
                result.addAll(getJournalLinks(subPage));
            }
        }
        return result;
    }


    public Collection<Journal> getAllJournals(){
        Collection<Journal> result = new HashSet<Journal>();
        for (String letterPage : getAllLetters()){
            for (String subPage : getAllSubPages(letterPage)){
                result.addAll(getJournals(subPage));
            }
        }
        return result;
    }

    private Collection<String> getJournalLinks(String subPage){
        Collection<String> result = new HashSet<String>();
        Document doc = getDocument(d.download(subPage));
        Collection<String> _result = ScrapeHelper.queryTextValues("//TABLE[@class='journaltable']/TBODY/TR[position() > 1]/TD/A/@href", doc);
        for (String s : _result) {
            result.add(DOMAIN + ROMEO + s);
            countJournalsProcessed++;
            if (countJournalsProcessed % 300 == 0) {
                LOG.info("Processed " + countJournalsProcessed + "journals");
                LOG.info(DOMAIN + ROMEO + s);
            }
        }
        return result;
    }

    private Collection<? extends Journal> getJournals(String subPage) {
        Collection<Journal> result = new HashSet<Journal>();
        Document doc = getDocument(d.download(subPage));
        NodeList journalRaws = queryNodes("//TABLE[@class='journaltable']/TBODY/TR[position() > 1]", doc);
        for (int i = 0; i < journalRaws.getLength(); i++){
            countJournalsProcessed++;
            result.add(getJournal(journalRaws.item(i)));
            if (countJournalsProcessed % 300 == 0 )
                LOG.info("Processed " + countJournalsProcessed + "journals" );
        }
        return result;
    }

    private Journal getJournal(Node journalRaw) {
        Journal result = new Journal();

        result.setTitle(extractTitle(journalRaw));
        result.setIssn(extractISSN(journalRaw));
        result.setEssn(extractESSN(journalRaw));
        result.setRomeoColour(extractRomeoColor(journalRaw));
        result.setNotes(extractNotes(journalRaw));

        String journalPageLink = extractJournalPageLink(journalRaw);
        result.setJournalPageLink(journalPageLink);
        if (!"".equals(journalPageLink)){
            result.setUrl(journalPageLink);
            Document doc = ScrapeHelper.getDocument(d.download(result.getJournalPageLink()));
            result.setAuthorsPrePrint(extractAuthorsPrePrint(doc));
            result.setAuthorsPostPrint(extractAuthorsPostPrint(doc));
            result.setPublishersVersion(extractPublishersVersion(doc));
            result.setConditions(extractConditions(doc));
            result.setMandatedOA(extractMandatedOA(doc));
            result.setExtendedNotes(extractExtendedNotes(doc));
            result.setCopyright(extractCopyright(doc));
            result.setUpdated(extractUpdated(doc));
            result.setJournalPageLink(extractJournalPageLink(doc));
            result.setPublishedBy(extractPublishedBy(doc));
            final String publisherID = extractPublisherID(doc);
            result.setPublisherID(publisherID);
            if (!"".equals(publisherID)) {
                if (!allPublishers.contains(publisherID)){
                    allPublishers.add(publisherID);
                }
                result.setPublisher(pHelper.getPublisher(publisherID));
            } else {
                result.setPublisher(new Publisher());
                LOG.warning("Unable to identify publisher ID for journal " + journalPageLink);
            }
        }

        return result;
    }

    //TODO: some journals have 'society' publisher like http://www.sherpa.ac.uk/romeo/search.php?issn=1017-0405.
        //what should we do about them?
    private String extractPublisherID(Document doc) {
        String result = "";
        String _link = clean(queryText("//TR/TD[contains(.,\"Published\u00a0by:\")]/following-sibling::TD[1]/A[contains(@href,'id=')]/@href", doc));
        List<NameValuePair> params = null;
        try {
            //pipe is illeagal character in according to RFC. browsers and web servers seem to be tolarate it but apache
            //http utils do not so I have to strip it off here
            params = URLEncodedUtils.parse(new URI(DOMAIN + _link.replaceAll("\\|", "")), "UTF-8");
            String id = "";
            for (NameValuePair param : params) if ("id".equals(param.getName())) id = param.getValue();
            if (!"".equals(id)) result = DOMAIN + "/romeo/search.php?id=" + id + "&format=full";
        } catch (URISyntaxException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return result;
    }

    protected String extractAuthorsPrePrint(Document doc) {
        return clean(queryText("//TR/TD[contains(.,\"Author's Pre-print:\")]/following-sibling::TD[1]", doc));
    }

    protected String extractAuthorsPostPrint(Document doc) {
        return clean(queryText("//TR/TD[contains(.,\"Author's Post-print:\")]/following-sibling::TD[1]", doc));
    }

    protected String extractPublishersVersion(Document doc) {
        return clean(queryText("//TR/TD[contains(.,\"Publisher's Version/PDF:\")]/following-sibling::TD[1]", doc));
    }

    protected Collection<String> extractConditions(Document doc) {
        Collection<String> result = new TreeSet<String>();
        for (String s : queryTextValues("//TR/TD[contains(.,\"General Conditions:\")]/following-sibling::TD[1]//LI", doc))
            result.add(clean(s));
        return result;
    }

    protected String extractMandatedOA(Document doc) {
        return clean(queryText("//TR/TD[contains(.,\"Mandated OA:\")]/following-sibling::TD[1]/text()", doc));
    }

    private Collection<String> extractExtendedNotes(Document doc) {
        Collection<String> result = new TreeSet<String>();
        for (String s : queryTextValues("//TR/TD[contains(.,\"Notes:\")]/following-sibling::TD[1]//LI", doc))
            result.add(clean(s));
        return result;
    }

    private String extractCopyright(Document doc) {
        return clean(queryText("//TR/TD[contains(.,\"Copyright:\")]/following-sibling::TD[1]", doc));
    }

    protected String extractUpdated(Document doc) {
        return clean(queryText("//TR/TD[contains(.,\"Updated:\")]/following-sibling::TD[1]", doc).split(" - ")[0]);
    }

    private String extractJournalPageLink(Document doc){
        return clean(queryText("//TR/TD[contains(.,\"Link to this page:\")]/following-sibling::TD[1]//A/@href", doc));
    }

    private String extractPublishedBy(Document doc){
        return clean(queryText("//TR/TD[contains(.,\"Published\u00a0by:\")]/following-sibling::TD[1]//B/A", doc));
    }

    private String extractNotes(Node journalRaw) {
        return clean(queryText("./TD[6]", journalRaw));
    }

    private String extractRomeoColor(Node journalRaw) {
        return clean(queryText("./TD[4]", journalRaw));
    }

    private String extractESSN(Node journalRaw) {
        return clean(queryText("./TD[3]", journalRaw));
    }

    private String extractISSN(Node journalRaw) {
        return clean(queryText("./TD[2]", journalRaw));
    }

    private String extractTitle(Node journalRaw) {
        return clean(queryText("./TD[1]", journalRaw));
    }

    private String extractJournalPageLink(Node journalRaw) {
        //TODO: ROMEO should be replaced with appropriate calculation of context we're in at the moment
        return DOMAIN + ROMEO + clean(queryText("./TD[1]//A/@href", journalRaw)).replaceAll(" ", "%20");
    }

    private Collection<String> getAllSubPages(String letterPage) {
        Collection<String> result = new TreeSet<String>();
        String html = d.download(letterPage);
        Document doc = getDocument(html);
        String lastLink = queryText("//A[contains(.,'Last\u00a0>>')]/@href", doc);
        Pattern pageNoPattern = Pattern.compile("pageno=([0-9]+)");
        Matcher pageNoPatternMatcher = pageNoPattern.matcher(lastLink);

        if (pageNoPatternMatcher.find()){
            String number = pageNoPatternMatcher.group(1);
            try {
                Integer pageNum = Integer.valueOf(number);
                // there is some error with link to letterpage Q on the website , it fix it.  number of journals to letter q = 100 ,if it changed this "if" maybe need to remove
                if (letterPage.equals("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=Q")){
                    pageNum--;
                }
                for (int i = 1; i <= pageNum; i++) {
                    result.add(DOMAIN + pageNoPatternMatcher.replaceAll("pageno=" + Integer.toString(i)));
                }
            } catch (NumberFormatException e) {
                LOG.warning("Unable to identify number of sub-pages at page " + letterPage + " so they are skipped.");
            }
        } else {
            result.add(letterPage);
            LOG.info("It seems there is no 'Last' link on " + letterPage + " so assuming this is the only subpage.");
        }
        LOG.info("download all subpages to letterpage : " + letterPage);
        return result;
    }

    private Collection<String> getAllLetters() {
        Collection<String> result = new TreeSet<String>();
        String html = d.download(DOMAIN + ENTRY_PAGE);
        Document doc = getDocument(html);
        List<String> links = queryValues("//DIV[@class='main']/TABLE[1]//TD[@align='center']/A/@href", doc);
        for (String l : links){
            result.add(DOMAIN + l);
        }
        LOG.info("Succesfully get all letters , letters count =  " + result.size());
        return result;
    }

    public static String clean(String s){
        String result = s != null ? s.replaceAll("\r","").replaceAll("\n","").replaceAll("\t","").replaceAll("\u00a0","")
                .replaceAll(" +"," ").trim() : "";
        if ("-".equals(result)) result = "";//replacing dash with empty string since dash is meaningless
        return result;
    }
}
