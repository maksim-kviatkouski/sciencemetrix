package com.web.scraping.helpers;

import org.apache.http.client.HttpClient;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/2/13
 * Time: 8:49 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Downloader {
    public void init();

    public void doHandShaking();

    public String download(String url);

    public HttpClient getHttpClient();
}
