package com.web.scraping.sherpa;

import com.web.scraping.helpers.Downloader;
import com.web.scraping.helpers.DownloaderFactory;
import com.web.scraping.helpers.ScrapeHelper;
import org.apache.commons.io.IOUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.net.ProxySelector;
import java.util.Collection;
import java.util.HashSet;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/20/13
 * Time: 9:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class JournalHelperTest {
    private Downloader d;
    private JournalHelper jHelper;

    //those guys are used to return some predefined html source making it absolutely transparent
    //for tests
    private Downloader mockedDownloader;
    private JournalHelper mockedJournalHelper;

    private final String TEST_JOURNAL_LINK = "http://www.sherpa.ac.uk/romeo/search.php?issn=1687-7632&type=issn&la=en&fIDnum=|&mode=simple";
    private Document TEST_JOURNAL_DOC;

    private final String TEST_JOURNAL_LINK2 = "http://www.sherpa.ac.uk/romeo/search.php?issn=1017-0405";
    private Document TEST_JOURNAL_DOC2;

    private final String TEST_JOURNAL_LINK3 = "http://www.sherpa.ac.uk/romeo/search.php?issn=0105-1873&type=issn&la=en&fIDnum=|&mode=simple";
    private Document TEST_JOURNAL_DOC3;

    @Before
    public void setUp() throws Exception {
        DownloaderFactory downloaderFactory = new SherpaDownloaderFactory();
        DefaultHttpClient httpClient = new DefaultHttpClient();
        ProxySelectorRoutePlanner routePlanner = new ProxySelectorRoutePlanner(httpClient.getConnectionManager().getSchemeRegistry(), ProxySelector.getDefault());
        httpClient.setRoutePlanner(routePlanner);
        d = downloaderFactory.newInstance(httpClient);
        jHelper = new JournalHelper(d);

        String testHTML = IOUtils.toString(new FileInputStream("src\\test\\java\\com\\web\\scraping\\sherpa\\test.html"));
        mockedDownloader = mock(Downloader.class);
        when(mockedDownloader.download(anyString())).thenReturn(testHTML);
        mockedJournalHelper = new JournalHelper(mockedDownloader);

        TEST_JOURNAL_DOC = ScrapeHelper.getDocument(d.download(TEST_JOURNAL_LINK));
        TEST_JOURNAL_DOC2 = ScrapeHelper.getDocument(d.download(TEST_JOURNAL_LINK2));
        TEST_JOURNAL_DOC3 = ScrapeHelper.getDocument(d.download(TEST_JOURNAL_LINK3));
    }

    /**
     * This tests are written about differently than tests for previous scripts. Previously I had to open some methods
     * as 'public' in order to test them. That's not right thing to do. Possible solutions: test through reflection
     * (done here) or extract methods into a separate class and make them public if they are complicated enough (takes
     * more time so I'm not doing it here).
     * @throws Exception
     */
    @Test public void testGetLetters() throws Exception {
        Collection<String> expected = new HashSet<String>(){{
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=A");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=C");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=D");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=E");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=F");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=G");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=H");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=I");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=J");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=K");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=L");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=M");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=N");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=O");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=P");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=Q");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=R");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=S");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=T");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=U");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=V");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=W");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=X");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=Y");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=Z");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?letter=OTHER&la=en&fIDnum=|&mode=simple");
        }};
        Method method = JournalHelper.class.getDeclaredMethod("getAllLetters");
        method.setAccessible(true);
        Collection<String> produced = (Collection<String>) method.invoke(jHelper);
        assertEquals(expected, produced);
    }

    @Test public void testGetAllSubPages() throws Exception {
        Collection<String> expected = new HashSet<String>(){{
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=1&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=2&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=3&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=4&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=5&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=6&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=7&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=8&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=9&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=10&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=11&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=12&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=13&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=14&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=15&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=16&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=17&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=18&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=19&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=20&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=21&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=22&la=en&fIDnum=|&mode=simple&letter=B");
            add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=23&la=en&fIDnum=|&mode=simple&letter=B");
        }};
        Method method = JournalHelper.class.getDeclaredMethod("getAllSubPages", String.class);
        method.setAccessible(true);
        Collection<String> produced = (Collection<String>) method.invoke(jHelper, "http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=B");
        assertEquals(expected, produced);
    }

    @Test
    public void testGetAllSubpages2() throws Exception{
        Collection<String> expected = new HashSet<String>(){{
                add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=1&la=en&fIDnum=|&mode=simple&letter=Q");
                add("http://www.sherpa.ac.uk/romeo/journalbrowse.php?pageno=2&la=en&fIDnum=|&mode=simple&letter=Q");
            }};

            Method method = JournalHelper.class.getDeclaredMethod("getAllSubPages", String.class);
            method.setAccessible(true);
            Collection<String> produced = (Collection<String>) method.invoke(jHelper, "http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=Q");
            assertEquals(expected, produced);
    }

    @Test public void testGetJournals() throws Exception {
        Collection<Journal> expected = new HashSet<Journal>(){{
            add(new Journal(){{
                setTitle("X-Acoustics: Imaging and Sensing");
                setIssn("");
                setEssn("2353-0634");
                setRomeoColour("Green");
                setNotes("");
            }});
            add(new Journal(){{
                setTitle("X-Ray Optics and Instrumentation");
                setIssn("1687-7632");
                setEssn("1687-7640");
                setRomeoColour("Green");
                setNotes("");
            }});
            add(new Journal(){{
                setTitle("X-Ray Spectrometry");
                setIssn("0049-8246");
                setEssn("1097-4539");
                setRomeoColour("Green");
                setNotes("Other parties");
            }});
            add(new Journal(){{
                setTitle("X-ray Structure Analysis Online");
                setIssn("");
                setEssn("1883-3578");
                setRomeoColour("Blue");
                setNotes("");
            }});
            add(new Journal(){{
                setTitle("Xenobiotica");
                setIssn("0049-8254");
                setEssn("1366-5928");
                setRomeoColour("Yellow");
                setNotes("");
            }});
            add(new Journal(){{
                setTitle("Xenotransplantation");
                setIssn("0908-665X");
                setEssn("1399-3089");
                setRomeoColour("Green");
                setNotes("Other parties");
            }});
            add(new Journal(){{
                setTitle("XVIIe siècle");
                setIssn("0012-4273");
                setEssn("");
                setRomeoColour("White");
                setNotes("Other parties");
            }});

        }};
        Method method = JournalHelper.class.getDeclaredMethod("getJournals", String.class);
        method.setAccessible(true);
        Collection<Journal> produced = (Collection<Journal>) method.invoke(jHelper, "http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=X");
        assertEquals(expected, produced);
    }

    @Test public void testExtractLinkToMoreInfo() throws Exception {
        String expected = "http://www.sherpa.ac.uk/romeo/search.php?issn=1687-7632&type=issn&la=en&fIDnum=|&mode=simple";
        Method method = JournalHelper.class.getDeclaredMethod("getJournals", String.class);
        method.setAccessible(true);
        Collection<Journal> produced = (Collection<Journal>) method.invoke(mockedJournalHelper, "http://www.sherpa.ac.uk/romeo/journalbrowse.php?la=en&fIDnum=|&mode=simple&letter=X");
        assertEquals(expected, produced.iterator().next().getJournalPageLink());
    }

    @Test public void testExtractAuthorsPrePrint() throws Exception {
        String expected = "author can archive pre-print (ie pre-refereeing)";
        Method method = JournalHelper.class.getDeclaredMethod("extractAuthorsPrePrint", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractAuthorsPostPrint() throws Exception {
        String expected = "author can archive post-print (ie final draft post-refereeing)";
        Method method = JournalHelper.class.getDeclaredMethod("extractAuthorsPostPrint", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractPublisherVersion() throws Exception {
        String expected = "author can archive publisher's version/PDF";
        Method method = JournalHelper.class.getDeclaredMethod("extractPublishersVersion", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractConditions() throws Exception {
        Collection<String> expected = new TreeSet<String>(){{
            add("Publisher's version/PDF may be used");
            add("Creative Commons Attribution License");
            add("Eligible UK authors may deposit in OpenDepot");
        }};
        Method method = JournalHelper.class.getDeclaredMethod("extractConditions", Document.class);
        method.setAccessible(true);
        Collection<String> produced = (Collection<String>) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testMandatedOA() throws Exception {
        String expected = "(Awaiting information)";
        Method method = JournalHelper.class.getDeclaredMethod("extractMandatedOA", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractExtendedNotes() throws Exception {
        Collection<String> expected = new TreeSet<String>(){{
            add("All titles are open access journals");
        }};
        Method method = JournalHelper.class.getDeclaredMethod("extractExtendedNotes", Document.class);
        method.setAccessible(true);
        Collection<String> produced = (Collection<String>) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testCopyright() throws Exception {
        String expected = "Policy";
        Method method = JournalHelper.class.getDeclaredMethod("extractCopyright", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testUpdated() throws Exception {
        String expected = "20-Mar-2013";
        Method method = JournalHelper.class.getDeclaredMethod("extractUpdated", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test
    public void testUpdated2() throws Exception{
        String expected = "11-Aug-2014";
        Method method = JournalHelper.class.getDeclaredMethod("extractUpdated", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC3);
        assertEquals(expected, produced);
    }

    @Test
    public void testColor() throws Exception{
        String expected = "yellow";
        Method method = JournalHelper.class.getDeclaredMethod("extractRomeoColor", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC3);
        assertEquals(expected, produced);
    }

    @Test public void testJournalLink() throws Exception {
        String expected = "http://www.sherpa.ac.uk/romeo/issn/1687-7632/";
        Method method = JournalHelper.class.getDeclaredMethod("extractJournalPageLink", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testPublishedBy() throws Exception {
        String expected = "Hindawi Publishing Corporation";
        Method method = JournalHelper.class.getDeclaredMethod("extractPublishedBy", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testPublisherID() throws Exception {
        String expected = "http://www.sherpa.ac.uk/romeo/search.php?id=100&format=full";
        Method method = JournalHelper.class.getDeclaredMethod("extractPublisherID", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testPublisherID2() throws Exception {
        String expected = "http://www.sherpa.ac.uk/romeo/search.php?id=232&format=full";
        Method method = JournalHelper.class.getDeclaredMethod("extractPublisherID", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(jHelper, TEST_JOURNAL_DOC2);
        assertEquals(expected, produced);
    }
}
