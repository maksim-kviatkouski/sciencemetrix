package com.web.scraping.sherpa;

import com.web.scraping.helpers.Downloader;
import com.web.scraping.helpers.DownloaderFactory;
import com.web.scraping.helpers.ScrapeHelper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.lang.reflect.Method;
import java.net.ProxySelector;
import java.util.Collection;
import java.util.LinkedList;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 8/29/13
 * Time: 7:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class PublisherHelperTest {
    private Downloader d;
    private PublisherHelper pHelper;

    private final static String PUBLISHER_LINK = "http://www.sherpa.ac.uk/romeo/search.php?id=242&format=full";
    private static Document PUBLISHER_DOC;

    private final static String PUBLISHER_LINK2 = "http://www.sherpa.ac.uk/romeo/search.php?id=1499&format=full";
    private static Document PUBLISHER_DOC2;

    private final static String PUBLISHER_LINK3 = "http://www.sherpa.ac.uk/romeo/search.php?id=736&format=full";
    private static Document PUBLISHER_DOC3;

    public static final String PUBLISHER_LINK4 = "http://www.sherpa.ac.uk/romeo/search.php?id=74&format=full";
    private static Document PUBLISHER_DOC4;

    @Before
    public void setUp() throws Exception {
        DownloaderFactory downloaderFactory = new SherpaDownloaderFactory();
        DefaultHttpClient httpClient = new DefaultHttpClient();
        ProxySelectorRoutePlanner routePlanner = new ProxySelectorRoutePlanner(httpClient.getConnectionManager().getSchemeRegistry(), ProxySelector.getDefault());
        httpClient.setRoutePlanner(routePlanner);
        d = downloaderFactory.newInstance(httpClient);
        pHelper = new PublisherHelper(d);

        PUBLISHER_DOC = ScrapeHelper.getDocument(d.download(PUBLISHER_LINK));
        PUBLISHER_DOC2 = ScrapeHelper.getDocument(d.download(PUBLISHER_LINK2));
        PUBLISHER_DOC3 = ScrapeHelper.getDocument(d.download(PUBLISHER_LINK3));
        PUBLISHER_DOC4 = ScrapeHelper.getDocument(d.download(PUBLISHER_LINK4));
    }

    @Test public void testExtractName() throws Exception {
        String expected = "Karger";
        Method method = PublisherHelper.class.getDeclaredMethod("extractName", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractCountry() throws Exception {
        String expected = "Switzerland";
        Method method = PublisherHelper.class.getDeclaredMethod("extractCountry", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractNumberOfJournals() throws Exception {
        String expected = "105";
        Method method = PublisherHelper.class.getDeclaredMethod("extractNumberOfJournals", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractRomeoStatus() throws Exception {
        String expected = "This is a RoMEO green publisher";
        Method method = PublisherHelper.class.getDeclaredMethod("extractRomeoStatus", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractCopyright() throws Exception {
        Collection<String> expected = new LinkedList<String>(){{
            add("http://www.karger.com/Journal/Guidelines/238704#15");
            add("http://www.karger.com/Journal/Guidelines/224177#10");
        }};
        Method method = PublisherHelper.class.getDeclaredMethod("extractCopyright", Document.class);
        method.setAccessible(true);
        Collection<String> produced = (Collection<String>) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractUpdated() throws Exception {
        String expected = "08-Aug-2013";
        Method method = JournalHelper.class.getDeclaredMethod("extractUpdated", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractAuthorsPrePrint() throws Exception {
        String expected = "author can archive pre-print (ie pre-refereeing)";
        //not perfect. but java inheritance doesn't work when you deal with reflection so this test case has to know
        //that extractAtuhorsPrePrint is inherited by PublisherHelper from JournalHelper and do reflection on JournalHelper
        Method method = JournalHelper.class.getDeclaredMethod("extractAuthorsPrePrint", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractAuthorsPostPrint() throws Exception {
        String expected = "author can archive post-print (ie final draft post-refereeing)";
        Method method = JournalHelper.class.getDeclaredMethod("extractAuthorsPostPrint", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractPublisherVersion() throws Exception {
        String expected = "author cannot archive publisher's version/PDF";
        Method method = JournalHelper.class.getDeclaredMethod("extractPublishersVersion", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractConditions() throws Exception {
        Collection<String> expected = new TreeSet<String>(){{
            add("On author's server or institutional server");
            add("Server must be non-commercial");
            add("Publisher's version/PDF cannot be used");
            add("Publisher copyright and source must be acknowledged");
            add("Must link to publisher version");
        }};
        Method method = JournalHelper.class.getDeclaredMethod("extractConditions", Document.class);
        method.setAccessible(true);
        Collection<String> produced = (Collection<String>) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractMandatedOA() throws Exception {
        String expected = "(Awaiting information)";
        Method method = JournalHelper.class.getDeclaredMethod("extractMandatedOA", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractPaidOpenAccess() throws Exception {
        String expected = "http://karger.com/Services/AuthorsChoice";
        Method method = PublisherHelper.class.getDeclaredMethod("extractPaidOpenAccess", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractExceptionsToPolicy() throws Exception {
        Collection<String> expected = new TreeSet<String>(){{
            add("Open Access Journals - RoMEO Green");
        }};
        Method method = PublisherHelper.class.getDeclaredMethod("extractExceptionsToPolicy", Document.class);
        method.setAccessible(true);
        Collection<String> produced = (Collection<String>) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractRomeoPublisherID() throws Exception {
        String expected = "242";
        Method method = PublisherHelper.class.getDeclaredMethod("extractRomeoPublisherId", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractLinkToThisPage() throws Exception {
        String expected = "http://www.sherpa.ac.uk/romeo/pub/242/";
        Method method = PublisherHelper.class.getDeclaredMethod("extractLinkToThisPage", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC);
        assertEquals(expected, produced);
    }

    @Test public void testExtractCountry2() throws Exception {
        String expected = "United States";
        Method method = PublisherHelper.class.getDeclaredMethod("extractCountry", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC2);
        assertEquals(expected, produced);
    }

    @Test public void testExtractName2() throws Exception {
        String expected = "Wiley";
        Method method = PublisherHelper.class.getDeclaredMethod("extractName", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC2);
            assertEquals(expected, produced);
    }

    @Test public void testExtractName3() throws Exception {
        String expected = "Association of Occupational Science";
        Method method = PublisherHelper.class.getDeclaredMethod("extractName", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC3);
        assertEquals(expected, produced);
    }

    @Test public void testExtractNumberOfJournals2() throws Exception {
        String expected = "2392";
        Method method = PublisherHelper.class.getDeclaredMethod("extractNumberOfJournals", Document.class);
        method.setAccessible(true);
        String produced = (String) method.invoke(pHelper, PUBLISHER_DOC4);
        assertEquals(expected, produced);
    }
}
