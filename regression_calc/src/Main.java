import com.sciencemetrix.helpers.*;
import com.sciencemetrix.regression.LinearRegression;
import com.sciencemetrix.regression.OLSRegressor;
import com.sciencemetrix.regression.RMARegressor;
import com.sciencemetrix.regression.confidence.ConfidenceBandBuilder;
import com.sciencemetrix.regression.jackknife.IterablePowerSetGenerator;
import com.sciencemetrix.regression.runner.JackknifedRunner;
import org.apache.commons.math3.stat.descriptive.moment.Mean;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Logger.infoLog("Reading properties");
        PropertiesReader propertiesReader = new PropertiesReader("regression.properties");
        final int[] RANKS = propertiesReader.getJackknifeRank();
        final boolean APPLY_LOG = propertiesReader.applyLog();
        final double [] CONFIDENCE_SIZES = propertiesReader.getConfidenceBandSizes();

        Logger.infoLog("Reading input data");
        DataReader dataReader = new DataReader();
        dataReader.readFile("input.txt");
        double [] x = dataReader.getX();
        double [] y = dataReader.getY();

        if (APPLY_LOG){
            Logger.infoLog("Applying log() to input data");
            x = LogApplyer.applyLog(x);
            y = LogApplyer.applyLog(y);
        }

        PrintWriter pw = null;
        try {
            pw = new PrintWriter("output.txt");
        } catch (FileNotFoundException e) {
            System.err.println("Error writing into file 'output.txt'");
            System.exit(1);
        }

        pw.write("Simulation for "+dataReader.getxHeader()+" as independent variable and "+dataReader.getyHeader()+" as dependent variable\n");
        pw.write(" \tOLS Diff. Avg.\tOLS Deviation\tRMA Diff. Avg.\tRMA Diff. Deviation.\r\n");
        for (int i = 0; i < RANKS.length; i++){
            Logger.infoLog("Running tests with " + RANKS[i] + " missing data points");
            JackknifedRunner runner = new JackknifedRunner(x, y, RANKS[i]);
            runner.setNumberOfThreads(propertiesReader.getNumberOfThreads());
            runner.runModels();
            pw.write(RANKS[i] + " missing values: \t" + runner.getOls_y_diff_average_percents() + "%\t" + runner.getOls_y_deviation() + "\t" + runner.getRma_y_diff_average_percents() + "%\t" + runner.getRma_y_deviation() + "\r\n");
        }

        pw.write("Building confidence bands");
        for (int i = 0; i < CONFIDENCE_SIZES.length; i++) {
            double p = CONFIDENCE_SIZES[i];
            ConfidenceBandBuilder olsCBB = new ConfidenceBandBuilder(new OLSRegressor().getRegression(x,y), x, y);
            ConfidenceBandBuilder rmaCBB = new ConfidenceBandBuilder(new RMARegressor().getRegression(x,y), x, y);

            try {
                PrintWriter olsPw = new PrintWriter("ols_confidence_" + p + ".txt");
                PrintWriter rmaPw = new PrintWriter("rma_confidence_" + p + ".txt");
                olsCBB.build(p);
                rmaCBB.build(p);
                ConfidencePrinter.printConfidence(olsCBB,olsPw);
                ConfidencePrinter.printConfidence(rmaCBB,rmaPw);
                olsPw.close();
                rmaPw.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }

        pw.close();
        Logger.infoLog("Done");
    }

}
