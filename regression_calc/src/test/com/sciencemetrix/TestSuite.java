package test.com.sciencemetrix;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.com.sciencemetrix.helpers.DataReaderTest;
import test.com.sciencemetrix.helpers.PropertiesReaderTest;
import test.com.sciencemetrix.regression.RMARegressorTest;
import test.com.sciencemetrix.regression.jackknife.JacknifeArraySlicerTest;
import test.com.sciencemetrix.regression.jackknife.PowerSetsGeneratorTest;
import test.com.sciencemetrix.regression.jackknife.SpeedyPowerSetGeneratorTest;
import test.com.sciencemetrix.regression.runner.JackknifedRunnerTest;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/19/13 Time: 7:55 PM
 */
@RunWith(value = Suite.class)
@Suite.SuiteClasses({DataReaderTest.class, PropertiesReaderTest.class, RMARegressorTest.class,
        JackknifedRunnerTest.class, JacknifeArraySlicerTest.class, PowerSetsGeneratorTest.class,
        SpeedyPowerSetGeneratorTest.class})

public class TestSuite {}
