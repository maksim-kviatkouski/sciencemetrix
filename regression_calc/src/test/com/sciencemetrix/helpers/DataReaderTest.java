package test.com.sciencemetrix.helpers;

import com.sciencemetrix.helpers.DataReader;
import com.sciencemetrix.regression.LinearRegression;
import org.junit.Assert;
import org.junit.Before;

import java.io.InputStream;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 6:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class DataReaderTest {

    private static final double DELTA = LinearRegression.DELTA;
    private DataReader dReader = new DataReader();

    @Before
    public void setUp() throws Exception {
        dReader.readFile("src\\test\\com\\sciencemetrix\\helpers\\testInput.txt");
    }

    @org.junit.Test
    public void testGetX() throws Exception {
        double [] testX = new double[]{100d, 110d, 125d, 130d, 135d, 140d, 160d, 170d, 200d};
        double [] x = dReader.getX();
        Assert.assertArrayEquals(testX, x, DELTA);
    }

    @org.junit.Test
    public void testGetY() throws Exception {
        double [] testY = new double[]{15000d, 25000d, 40000d, 45000d, 40000d, 65000d, 80000d, 120000d, 90000d};
        double [] y = dReader.getY();
        Assert.assertArrayEquals(testY, y, DELTA);
    }
}
