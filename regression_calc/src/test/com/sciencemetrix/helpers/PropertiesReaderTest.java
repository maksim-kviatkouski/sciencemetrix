package test.com.sciencemetrix.helpers;

import com.sciencemetrix.helpers.PropertiesReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/17/13 Time: 7:24 PM
 */
public class PropertiesReaderTest {
    private PropertiesReader propertiesReader;
    @Before
    public void setUp() throws Exception {
        propertiesReader = new PropertiesReader("src\\test\\com\\sciencemetrix\\helpers\\test.properties");
    }

    @Test
    public void testGetJackknifeRank() throws Exception {
        Assert.assertArrayEquals(new int[] {3,4,9}, propertiesReader.getJackknifeRank());
    }

    @Test
    public void testApplyLog() throws Exception {
        Assert.assertEquals(true, propertiesReader.applyLog());
    }

    @Test
    public void testThreadsNumber() throws Exception {
        Assert.assertEquals(8, propertiesReader.getNumberOfThreads());
    }
}
