package test.com.sciencemetrix.regression.jackknife;

import com.sciencemetrix.regression.jackknife.PowerSetsGenerator;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 9:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class PowerSetsGeneratorTest {
    @Test
    public void testGenerateSubsets() throws Exception {
        Set<Set<Integer>> set_5_2 = PowerSetsGenerator.generateSubsets(5,2);
        Set<Set<Integer>> test_5_2_set = new HashSet<Set<Integer>>();
        test_5_2_set.add(new HashSet<Integer>(){{add(0);add(1);}});
        test_5_2_set.add(new HashSet<Integer>(){{add(0);add(2);}});
        test_5_2_set.add(new HashSet<Integer>(){{add(0);add(3);}});
        test_5_2_set.add(new HashSet<Integer>(){{add(0);add(4);}});
        test_5_2_set.add(new HashSet<Integer>(){{add(1);add(2);}});
        test_5_2_set.add(new HashSet<Integer>(){{add(1);add(3);}});
        test_5_2_set.add(new HashSet<Integer>(){{add(1);add(4);}});
        test_5_2_set.add(new HashSet<Integer>(){{add(2);add(3);}});
        test_5_2_set.add(new HashSet<Integer>(){{add(2);add(4);}});
        test_5_2_set.add(new HashSet<Integer>(){{add(3);add(4);}});

        Assert.assertTrue(set_5_2.equals(test_5_2_set));
    }
}
