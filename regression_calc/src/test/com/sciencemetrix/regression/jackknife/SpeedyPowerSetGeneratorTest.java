package test.com.sciencemetrix.regression.jackknife;

import com.sciencemetrix.regression.jackknife.SpeedyPowerSetGenerator;
import org.junit.Test;

import java.util.Arrays;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/18/13 Time: 11:24 PM
 */
public class SpeedyPowerSetGeneratorTest {
    @Test
    public void testGeneratePowerSet() throws Exception {
        int[][] missingIndexes = SpeedyPowerSetGenerator.generatePowerSet(10, 3);
        for (int i = 0; i < missingIndexes.length; i++){
            System.out.println(Arrays.toString(missingIndexes[i]));
        }
    }
}
