package test.com.sciencemetrix.regression.jackknife;

import com.sciencemetrix.regression.LinearRegression;
import com.sciencemetrix.regression.jackknife.JackknifeArraySlicer;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 10:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class JacknifeArraySlicerTest {
    @Test
    public void testSliceArray() throws Exception {
        double [] source_x = new double[]{0.0d, 0.1d, 0.2d, 0.3d, 0.4d, 0.5d, 0.6d, 0.7d, 0.8d, 0.9d};
        Set<Integer> missingIndexes = new HashSet<Integer>();
        missingIndexes.add(0);
        missingIndexes.add(2);
        missingIndexes.add(9);
        double [] sliced_x = JackknifeArraySlicer.sliceArray(source_x, missingIndexes);

        double [] test_x = new double[]{0.1d, 0.3d, 0.4d, 0.5d, 0.6d, 0.7d, 0.8d};

        Assert.assertArrayEquals(test_x, sliced_x, LinearRegression.DELTA);
    }
}
