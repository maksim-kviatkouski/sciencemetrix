package test.com.sciencemetrix.regression.confidence;

import com.sciencemetrix.regression.confidence.ConfidenceBandBuilder;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/27/13 Time: 10:43 PM
 */
public class ConfidenceBandBuilderTest {
    @Test
    public void testGetT() throws Exception {
        double test_t = 2.042;
        double t = ConfidenceBandBuilder.getT(0.95, 30);
        assertEquals(test_t, t, 0.001);
    }

    @Test
    public void testGetT2() throws Exception {
        double test_t = 3.416;
        double t = ConfidenceBandBuilder.getT(0.999, 80);
        assertEquals(test_t, t, 0.001);
    }

    @Test
    public void testGetT3() throws Exception {
        double test_t = 1.943;
        double t = ConfidenceBandBuilder.getT(0.9, 6);
        assertEquals(test_t, t, 0.001);
    }
}
