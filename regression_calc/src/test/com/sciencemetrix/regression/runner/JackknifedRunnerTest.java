package test.com.sciencemetrix.regression.runner;

import com.sciencemetrix.regression.runner.JackknifedRunner;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/17/13
 * Time: 7:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class JackknifedRunnerTest {

    private JackknifedRunner runner;
    double [] x;
    double [] y;

    @Before
    public void setUp(){
        double [] x = new double[]{6.671344027d, 6.41424245d, 6.852632946d, 6.711694551d, 7.911007493d, 7.531869466d};
        double [] y = new double[]{11.42242568d, 11.17551181d, 11.54863506d, 11.41630759d, 12.48925517d, 12.14270225d};
        runner = new JackknifedRunner(x, y, 1);
        runner.runModels();
    }

    @Test
    public void testOlsAverage() throws Exception {
        Assert.assertEquals(0.000159d, runner.getOls_y_diff_average(), 0.00005d);
    }

    @Test
    public void testOlsDeviation() throws Exception {
        Assert.assertEquals(0.00178305d, runner.getOls_y_deviation(), 0.00001d);
    }

    @Test
    public void testRmaAverage() throws Exception {
        Assert.assertEquals(0.000153d, runner.getRma_y_diff_average(), 0.00005d);
    }

    @Test
    public void testRmaDeviation() throws Exception {
        Assert.assertEquals(0.001773844d, runner.getRma_y_deviation(), 0.00001d);
    }
}
