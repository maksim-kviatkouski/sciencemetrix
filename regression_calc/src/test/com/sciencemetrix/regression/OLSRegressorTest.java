package test.com.sciencemetrix.regression;

import com.sciencemetrix.helpers.DataReader;
import com.sciencemetrix.regression.LinearRegression;
import com.sciencemetrix.regression.OLSRegressor;
import com.sciencemetrix.regression.Regressor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 7:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class OLSRegressorTest {
    private static final double DELTA = LinearRegression.DELTA;
    private static DataReader dReader = new DataReader();

    @Before
    public void setUp() throws Exception {
        dReader.readFile("src\\test\\com\\sciencemetrix\\regression\\testInput.txt");
    }

    @Test
    public void testX() throws Exception {
        double [] test_x = new double[] {6.41424245d, 6.852632946d, 6.711694551d, 7.911007493d, 7.531869466d};
        double [] x = dReader.getX();
        Assert.assertArrayEquals(test_x, x, DELTA);
    }

    @Test
    public void testY() throws Exception {
        double [] test_y = new double[] {11.17551181d, 11.54863506d, 11.41630759d, 12.48925517d, 12.14270225};
        double [] y = dReader.getY();
        Assert.assertArrayEquals(test_y, y, DELTA);
    }

    @Test
    public void testGetRegression() throws Exception {
        LinearRegression test_regression = new LinearRegression(0.880405904d, 5.517432181d);

        Regressor regressor = new OLSRegressor();
        LinearRegression regression = regressor.getRegression(dReader.getX(), dReader.getY());

        Assert.assertTrue(test_regression.equals(regression));
    }
}
