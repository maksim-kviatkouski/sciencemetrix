package com.sciencemetrix.regression;

import org.apache.commons.math3.stat.regression.SimpleRegression;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 6:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class OLSRegressor implements Regressor {
    @Override
    public LinearRegression getRegression(double[] x, double[] y) {
        SimpleRegression regression = new SimpleRegression();
        for (int i = 0; i < x.length; i++){
            regression.addData(x[i], y[i]);
        }
        double m = regression.getSlope();
        double b = regression.getIntercept();

        return new LinearRegression(m,b);
    }
}
