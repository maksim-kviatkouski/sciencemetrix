package com.sciencemetrix.regression.jackknife;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 7:44 PM
 * To change this template use File | Settings | File Templates.
 */

import com.sciencemetrix.helpers.Logger;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * This class generates all possible subsets of rank r of a set {0, 1, 2, ... , n}
 * For example result of generateSubsets(5,2) should look like:
 * {1,2} {1,3} {1,4} {1,5} {2,3} {2,4} {2,5} {3,4} {3,5} {4,5}
 * WARNING: Be careful with params! In case you call generateSubsets(20,10) there is going to be ~185k of possible
 * permutations which can cause performance degradation.
 */
public class PowerSetsGenerator {
    public static Set<Set<Integer>> generateSubsets(int n, int r){
        Set<Set<Integer>> result = null;
        long start = Calendar.getInstance().getTimeInMillis();
        result = generateSubsets(n, r, 0);
        Logger.infoLog("Powerset of rank " + r + " for set of " + n + " numbers was generated in " + (Calendar.getInstance().getTimeInMillis() - start) + "ms.");
        return result;
    }

    private static Set<Set<Integer>> generateSubsets(int n, int r, int start) {
        Set<Set<Integer>> result = new HashSet<Set<Integer>>();
        if (r == 0){
            Set<Set<Integer>> rankZeroSet = new HashSet<Set<Integer>>();
            rankZeroSet.add(new HashSet<Integer>());
            return  rankZeroSet;
        }
        for (int i = start; i < n - r + 1; i++){
            Set<Set<Integer>> tempSet = generateSubsets(n, r - 1, i + 1);
            for (Set<Integer> s : tempSet){s.add(i);}
            result.addAll(tempSet);
        }
        return result;
    }
}
