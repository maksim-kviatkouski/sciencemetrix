package com.sciencemetrix.regression.jackknife;

import java.util.Arrays;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 10:01 PM
 * To change this template use File | Settings | File Templates.
 */

public class JackknifeArraySlicer {
    public static double [] sliceArray(double [] sourceArray, Set<Integer> missingIndexes){
        double [] result = new double[sourceArray.length - missingIndexes.size()];
        int j = 0;
        for (int i = 0; i < sourceArray.length; i++){
            if (!missingIndexes.contains(Integer.valueOf(i))){
                result[j++] = sourceArray[i];
            }
        }
        return result;
    }

    public static double [] sliceArray(double [] sourceArray, int [] missingIndexes){
        double [] result = new double[sourceArray.length - missingIndexes.length];
        int j = 0;
        for (int i = 0; i < sourceArray.length; i++){
            if (Arrays.binarySearch(missingIndexes,i) < 0) {
                result[j++] = sourceArray[i];
            }
        }
        return result;
    }
}
