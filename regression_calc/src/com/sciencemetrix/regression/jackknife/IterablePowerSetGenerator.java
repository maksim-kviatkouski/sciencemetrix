package com.sciencemetrix.regression.jackknife;

import com.sciencemetrix.helpers.Logger;

import java.util.Arrays;
import java.util.Calendar;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/18/13 Time: 11:20 PM
 */
public class IterablePowerSetGenerator {
    private int[] currentSubSet;
    private int counter = 0;
    private int n;
    private int k;
    private int p;
    private int[] temp_result;

    public void init(int n, int k){
        this.n = n;
        this.k = k;
        currentSubSet = new int[k];
        for (int i = 0; i < k; i++){
            currentSubSet[i] = i;
        }
        p = k - 1;
    }

    public int[] getNextPermutation(){
        temp_result = Arrays.copyOf(currentSubSet,k);

        if (currentSubSet[k-1] == n-1) {
            p--;
        } else {
            p = k-1;
        }
        if (p >= 0){
            for (int j = k-1; j >= p; j--){
                currentSubSet[j] = currentSubSet[p] + j - p + 1;
            }
        }

        return temp_result;
    }

    public boolean hasNext(){
        return p>=0;
    }

    public static int getPowerSetSize(int n, int k) {
        long temp_result = 1;

        for (int i = 0; i < k; i++){
            temp_result *= (n-i);
        }
        for (int i = 1; i <= k; i++){
            temp_result /= i;
        }

        return (int) temp_result;
    }
}
