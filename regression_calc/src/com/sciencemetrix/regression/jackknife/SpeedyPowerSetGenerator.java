package com.sciencemetrix.regression.jackknife;

import com.sciencemetrix.helpers.Logger;

import java.util.Arrays;
import java.util.Calendar;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/18/13 Time: 11:20 PM
 */
public class SpeedyPowerSetGenerator {
    public static int[][] generatePowerSet(int n, int k){
        long start = Calendar.getInstance().getTimeInMillis();

        int[][] result = new int[getPowerSetSize(n,k)][];
        int counter = 0;

        int[] subset = new int[k];
        for (int i = 0; i < k; i++){
            subset[i] = i;
        }
        int p = k - 1;
        while (p >= 0) {
            result[counter++] = Arrays.copyOf(subset,k);

            if (subset[k-1] == n-1) {
                p--;
            } else {
                p = k-1;
            }

            if (p >= 0){
                for (int j = k-1; j >= p; j--){
                    subset[j] = subset[p] + j - p + 1;
                }
            }
        }

        Logger.infoLog("Powerset of rank " + k + " for set of " + n + " numbers was generated in " + (Calendar.getInstance().getTimeInMillis() - start) + "ms.");
        return result;
    }

    private static int getPowerSetSize(int n, int k) {
        long temp_result = 1;

        for (int i = 0; i < k; i++){
            temp_result *= (n-i);
        }
        for (int i = 1; i <= k; i++){
            temp_result /= i;
        }

        return (int) temp_result;
    }
}
