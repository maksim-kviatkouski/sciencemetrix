package com.sciencemetrix.regression.confidence;

import com.sciencemetrix.helpers.DataReader;
import com.sciencemetrix.helpers.PropertiesReader;
import com.sciencemetrix.regression.LinearRegression;
import com.sciencemetrix.regression.OLSRegressor;
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.stat.descriptive.moment.Mean;

import java.util.Arrays;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/27/13 Time: 10:18 PM
 */
public class ConfidenceBandBuilder {
    private final static int NUMBER_OF_SAMPLES = new PropertiesReader("regression.properties").getNumberOfSamples();

    private LinearRegression lr;
    double [] x;
    double [] y;

    double [] lowerEdge;
    double [] upperEdge;

    double [] regressionLine;

    double [] samplingPoints;

    double [] residuals;

    public ConfidenceBandBuilder(LinearRegression lr, double[] x, double[] y) {
        this.lr = lr;
        this.x = x;
        this.y = y;
    }

    /**
     * @param p is confidence param assuming both-sided confidence band so if one wants to build both-sided 95%
     *          confidence band one should pass 0.95 here
     */
    public void build(double p){
        final double t = ConfidenceBandBuilder.getT(p, x.length-2); //-2 is not a magic here
        // please check http://en.wikipedia.org/wiki/Simple_linear_regression#Normality_assumption

        Mean mean = new Mean();
        double xMean = mean.evaluate(x);
        double sqDev = 0;
        double sqEps = 0;
        for (int i = 0; i < x.length; i++){
            sqDev += (x[i]-xMean)*(x[i]-xMean);
            sqEps += (y[i]-lr.predict(x[i]))*(y[i]-lr.predict(x[i]));
        }
        lowerEdge = new double[NUMBER_OF_SAMPLES];
        upperEdge = new double[NUMBER_OF_SAMPLES];
        regressionLine = new double[NUMBER_OF_SAMPLES];
        samplingPoints = new double[NUMBER_OF_SAMPLES];
        residuals = new double[x.length];
        //we should not affect initial array since it may be used elsewhere
        double [] temp_x = Arrays.copyOf(x, x.length);
        Arrays.sort(temp_x);
        double dx = (temp_x[temp_x.length-1] - temp_x[0])/NUMBER_OF_SAMPLES;
        for (int i = 0; i < NUMBER_OF_SAMPLES; i++) {
            double xNow = temp_x[0] + dx * i;
            double temp_y = lr.predict(xNow);
            int n = x.length;
            double stErr = Math.sqrt(sqEps/(n-2));
            double err = t * stErr * Math.sqrt(1.0d/n + (xNow-xMean)*(xNow-xMean)/sqDev);
            lowerEdge[i] = temp_y - err;
            upperEdge[i] = temp_y + err;
            regressionLine[i] = temp_y;
            samplingPoints[i] = xNow;
        }

        for (int i = 0; i < x.length; i++) {
            residuals[i] = y[i] - lr.predict(x[i]);
        }
    }


    /**
     * Return's corresponding quantile of Student's distribution with @param df degrees of freedom
     * @param p both=sided confidence interval
     * @param df
     * @return
     */
    public static double getT(double p, int df) {
        final double _p = (1-p)/2;
        TDistribution tdist = new TDistribution(df);
        return -tdist.inverseCumulativeProbability(_p);
    }

    public double[] getLowerEdge() {
        return lowerEdge;
    }

    public double[] getUpperEdge() {
        return upperEdge;
    }

    public double[] getRegressionLine() {
        return regressionLine;
    }

    public double[] getX() {
        return x;
    }

    public double[] getY() {
        return y;
    }

    public double[] getSamplingPoints() {
        return samplingPoints;
    }

    public double[] getResiduals() {
        return residuals;
    }
}
