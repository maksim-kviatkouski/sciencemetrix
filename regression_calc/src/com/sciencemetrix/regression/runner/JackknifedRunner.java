package com.sciencemetrix.regression.runner;

import com.sciencemetrix.regression.LinearRegression;
import com.sciencemetrix.regression.OLSRegressor;
import com.sciencemetrix.regression.RMARegressor;
import com.sciencemetrix.regression.Regressor;
import com.sciencemetrix.regression.jackknife.IterablePowerSetGenerator;
import com.sciencemetrix.regression.jackknife.JackknifeArraySlicer;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.commons.math3.stat.descriptive.SynchronizedSummaryStatistics;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/16/13 Time: 10:23 PM
 */

public class JackknifedRunner {
    private static final int N_THREADS = 4;
    private double [] x;
    private double [] y;

    private int jackknifeRank;

    private int numberOfDifferences;

    private SummaryStatistics ols_stats = new SynchronizedSummaryStatistics();
    private SummaryStatistics rma_stats = new SynchronizedSummaryStatistics();

    private List<LinearRegression> ols_reggr_list = Collections.synchronizedList(new LinkedList<LinearRegression>());
    private List<LinearRegression> rma_reggr_list = Collections.synchronizedList(new LinkedList<LinearRegression>());

    private ExecutorService executorService = new ThreadPoolExecutor(8, 16, 30, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(8), new ThreadPoolExecutor.CallerRunsPolicy());

    public JackknifedRunner(double[] x, double[] y, int jackknifeRank) {
        this.x = x;
        this.y = y;
        this.jackknifeRank = jackknifeRank;
    }

    public void runModels(){
        final int missingPointsTotal = IterablePowerSetGenerator.getPowerSetSize(x.length, jackknifeRank);
        final AtomicInteger counter = new AtomicInteger(0);

        IterablePowerSetGenerator ipsg = new IterablePowerSetGenerator();
        ipsg.init(x.length, jackknifeRank);

        while (ipsg.hasNext()){
            final int[] missingIndexes = ipsg.getNextPermutation();
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    double[] sliced_x = JackknifeArraySlicer.sliceArray(x, missingIndexes);
                    double[] sliced_y = JackknifeArraySlicer.sliceArray(y, missingIndexes);
                    Regressor olsRegressor = new OLSRegressor();
                    LinearRegression olsRegression = olsRegressor.getRegression(sliced_x, sliced_y);
                    Regressor rmaRegressor = new RMARegressor();
                    LinearRegression rmaRegression = rmaRegressor.getRegression(sliced_x, sliced_y);

                    double ols_y_diff;
                    double rma_y_diff;
                    for (int j = 0; j < missingIndexes.length; j++){
                        int ix = missingIndexes[j];
                        ols_y_diff = ((y[ix] - olsRegression.predict(x[ix]))/y[ix]);
                        rma_y_diff = ((y[ix] - rmaRegression.predict(x[ix]))/y[ix]);
                        ols_stats.addValue(ols_y_diff);
                        rma_stats.addValue(rma_y_diff);
                    }

                    ols_reggr_list.add(olsRegression);
                    rma_reggr_list.add(rmaRegression);

                    int c = counter.incrementAndGet();
                    prettyProgressPrint(c,missingPointsTotal,jackknifeRank);
                }
            });
        }
        executorService.shutdown();
        try {
            if (executorService.awaitTermination(1, TimeUnit.DAYS)) {
                System.out.println();
                System.out.println("All tasks finished their execution.");
            } else {
                System.out.println();
                System.out.println("Some tasks were interrupted");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println();
    }

    public void setNumberOfThreads(int n){
        executorService = new ThreadPoolExecutor(n, n, 30, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(n), new ThreadPoolExecutor.CallerRunsPolicy());
    }

    private void prettyProgressPrint(int counter, int missingPointsTotal, int rank) {
        if (counter%10000==0 || counter==missingPointsTotal-1){
            int percents = 100*counter/missingPointsTotal;
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < percents-1; i++) sb.append("=");
            if (percents<100) sb.append(">");
            for (int i = percents; i < 100; i++) sb.append(" ");
            sb.append(" " + counter + " of " + missingPointsTotal + " missing points calculated. Rank: " + rank + "   ");
            System.out.print("\r" + sb.toString());
        }
    }

    public double getOls_y_diff_average() {
        return ols_stats.getMean();
    }

    public double getRma_y_diff_average() {
        return rma_stats.getMean();
    }

    public double getOls_y_deviation() {
        return ols_stats.getStandardDeviation();
    }

    public double getRma_y_deviation(){
        return rma_stats.getStandardDeviation();
    }

    public double getOls_y_diff_average_percents(){
        return getOls_y_diff_average()*100;
    }

    public double getRma_y_diff_average_percents(){
        return getRma_y_diff_average()*100;
    }

    public List<LinearRegression> getOls_reggr_list() {
        return ols_reggr_list;
    }

    public List<LinearRegression> getRma_reggr_list() {
        return rma_reggr_list;
    }
}
