package com.sciencemetrix.regression;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 6:56 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Regressor {
    public abstract LinearRegression getRegression(double[] x, double[] y);
}
