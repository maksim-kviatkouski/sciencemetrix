package com.sciencemetrix.regression;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 6:52 PM
 * Class represents linear one-dimension regression as a m*x + b
 */
public class LinearRegression {
    //sets precision for double values comparison
    public static final double DELTA = 0.00000001d;

    private double m;
    private double b;

    public LinearRegression(double m, double b) {
        this.m = m;
        this.b = b;
    }

    public double getM() {
        return m;
    }

    public double getB() {
        return b;
    }

    public double predict(double x){
        return m*x + b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LinearRegression that = (LinearRegression) o;

        if (that.b - b >= DELTA) return false;
        if (that.m - m >= DELTA) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = m != +0.0d ? Double.doubleToLongBits(m) : 0L;
        result = (int) (temp ^ (temp >>> 32));
        temp = b != +0.0d ? Double.doubleToLongBits(b) : 0L;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "LinearRegression{" +
                "m=" + m +
                ", b=" + b +
                '}';
    }
}
