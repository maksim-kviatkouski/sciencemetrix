package com.sciencemetrix.regression;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 6:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class RMARegressor implements Regressor {
    @Override
    public LinearRegression getRegression(double[] x, double[] y) {
        DescriptiveStatistics xStats = new DescriptiveStatistics();
        DescriptiveStatistics yStats = new DescriptiveStatistics();
        for (int i = 0; i < x.length; i++){
            xStats.addValue(x[i]);
        }
        for (int i = 0; i < y.length; i++){
            yStats.addValue(y[i]);
        }
        double xStDev = xStats.getStandardDeviation();
        double yStDev = yStats.getStandardDeviation();
        double xAverage = xStats.getMean();
        double yAverage = yStats.getMean();

        double m = yStDev/xStDev;
        double b = yAverage - m * xAverage;

        return new LinearRegression(m,b);
    }
}
