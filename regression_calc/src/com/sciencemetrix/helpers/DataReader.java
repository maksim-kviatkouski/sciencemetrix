package com.sciencemetrix.helpers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/16/13
 * Time: 6:00 PM
 * To change this template use File | Settings | File Templates.
 */

public class DataReader {
    private static final int MAX_ELEMENTS = 10000;

    private double[] x;
    private double[] y;

    private String xHeader;
    private String yHeader;

    public void readFile(String filename){
        double[] x_buff = new double[MAX_ELEMENTS];
        double[] y_buff = new double[MAX_ELEMENTS];
        try {
            BufferedReader bReader = new BufferedReader(new FileReader(filename));
            //first line carries headers per spec
            String str = bReader.readLine();
            if (str!=null){
                String[] headers = str.split("\t");
                xHeader = headers[0];
                yHeader = headers[1];
            }
            str = bReader.readLine();
            int i = 0;
            while (str != null){
                Scanner scanner = new Scanner(str);
                x_buff[i] = scanner.nextDouble();
                y_buff[i] = scanner.nextDouble();
                i++;

                str = bReader.readLine();
            }
            x = Arrays.copyOf(x_buff, i);
            y = Arrays.copyOf(y_buff, i);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double[] getX(){
        return this.x;
    }

    public double[] getY(){
        return this.y;
    }

    public String getxHeader() {
        return xHeader;
    }

    public String getyHeader() {
        return yHeader;
    }
}
