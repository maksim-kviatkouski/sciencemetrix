package com.sciencemetrix.helpers;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/17/13 Time: 7:18 PM
 */
public class PropertiesReader {
    private Properties props = new Properties();

    public PropertiesReader(String filename) {
        try {
            props.load(new FileReader(filename));
        } catch (IOException e) {
            System.err.println("File '"+filename+"' cannot be found.");
        }
    }

    public int[] getJackknifeRank(){
        String str_prop = props.getProperty("jackknife.rank");
        String[] str_values = str_prop.split(" ");
        int[] result = new int[str_values.length];
        for (int i = 0; i < result.length; i++){
            result[i] = Integer.valueOf(str_values[i]);
        }
        return result;
    }

    public boolean applyLog(){
        return Boolean.valueOf(props.getProperty("apply.log"));
    }

    public int getNumberOfThreads(){
        return Integer.valueOf(props.getProperty("threads.number"));
    }

    public int getNumberOfSamples(){
        return Integer.valueOf(props.getProperty("confidence.samples.number", "200"));
    }

    public double[] getConfidenceBandSizes(){
        String str_prop = props.getProperty("confidence.band.sizes");
        String[] str_values = str_prop.split(" ");
        double [] result = new double[str_values.length];
        for (int i = 0; i < result.length; i++){
            result[i] = Double.valueOf(str_values[i]);
        }
        return result;
    }
}
