package com.sciencemetrix.helpers;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/17/13
 * Time: 7:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class LogApplyer {
    public static double[] applyLog(double [] source){
        double [] result = new double[source.length];

        for (int i = 0; i < source.length; i++){
            result[i] = Math.log10(source[i]);
        }

        return result;
    }
}
