package com.sciencemetrix.helpers;

import com.sciencemetrix.regression.confidence.ConfidenceBandBuilder;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/24/13 Time: 6:03 PM
 */
public class ConfidencePrinter {
    public static void printConfidence(ConfidenceBandBuilder cbb, PrintWriter pw) {
        double [] x = cbb.getX();
        double [] y = cbb.getY();
        double [] regressionLine = cbb.getRegressionLine();
        double [] upperEdge = cbb.getUpperEdge();
        double [] lowerEdge = cbb.getLowerEdge();
        double [] samplingPoints = cbb.getSamplingPoints();

        pw.write("X\tY\tUpper\tLower\tRegression\r\n");

        for (int i = 0; i < x.length; i++){
            pw.write(x[i]+"\t"+y[i]+"\t"+"\t"+"\t"+"\r\n");
        }

        for (int i = 0; i < samplingPoints.length; i++){
            pw.write(samplingPoints[i]+"\t"+"\t"+upperEdge[i]+"\t"+lowerEdge[i]+"\t"+regressionLine[i]+"\r\n");
        }
    }
}
