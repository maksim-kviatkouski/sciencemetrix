package com.sciencemetrix.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 3/18/13
 * Time: 8:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class Logger {

    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void infoLog(String message){
        System.out.println(df.format(Calendar.getInstance().getTime()) + " [INFO] " + message);
    }
}
