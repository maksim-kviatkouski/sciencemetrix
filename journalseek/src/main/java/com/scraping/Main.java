package com.scraping;


import com.scraping.journalseek.Journal;
import com.scraping.journalseek.JournalHelper;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.LinkedList;

//DONE: figure out where empty lines come from - from cached redirects
//DONE: http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=0031-5362
    //Scientific areas are scraped wrong there
//DONE: http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=0377-5038
    //Classification is scraped wrong there
//DONE: http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=0044-4502
    //More info is scraped wrong there
public class Main {
    private final static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private final static DateFormat fileNameDf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

    public static void main(String[] args) {
        long start = Calendar.getInstance().getTimeInMillis();
        Collection<Journal> journals = new LinkedList<Journal>();
        System.out.println("Getting links to all journals...");

        Collection<String> journalLinks = JournalHelper.getLinksToJournals();
       /* Collection<String> journalLinks = new HashSet<String>();
        journalLinks.add("http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=0067-2238");
        journalLinks.add("http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=2391-7504");
        journalLinks.add("http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=1532-4494");
        journalLinks.add("http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=2249-4863");*/

        System.out.println(journalLinks.size() + " journals found. Started processing journals...");
        try {
            OutputStreamWriter pw = new OutputStreamWriter(
                    new FileOutputStream("journalseek_" + fileNameDf.format(Calendar.getInstance().getTime()) + ".txt"),
                    "UTF-8"
            );
            int  debug_counter = 0;
            for (String s : journalLinks) {
                //if (!"http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=1091-8728".equals(s)) continue;
                journals.add(JournalHelper.extractJournalFromURL(s));
                if (++debug_counter%1000 == 0) System.out.println(df.format(Calendar.getInstance().getTime()) + " : " + debug_counter + " journals processed.");
            }
            pw.write(Journal.tabDelimitedHeader()+"\r\n");
            for (Journal j : journals) pw.write(j.tabDelimitedString()+"\r\n");
            pw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.print("Done. Processing took " + (Calendar.getInstance().getTimeInMillis() - start)/1000 + " s.");
    }
}
