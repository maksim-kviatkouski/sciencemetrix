package com.scraping.journalseek;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 6/23/13
 * Time: 7:24 PM
 * To change this template use File | Settings | File Templates.
 */

//DONE: is ISSN in http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=pIssn&query=0389-9160 a printed or electronic one?
    //defaulting ISSN is considered to be a p-ISSN per conversation with Eric
//DONE: implement http://screencast.com/t/HyF8IsxB
//DONE: implement http://screencast.com/t/FhKj5DccQt
//DONE: make sure test for multivalued moreInfo property - it seems there are no cases when list "Further Information"
    //contains more than one list item
//DONE: test for "just ISSN"
public class Journal {
    private String title;
    private String shortTitle;
    private String publishedBy;
    private String publishedByLink;
    private String pIssn;
    private String eIssn;
    private String issn;
    private String description;
    private Map<String, String> moreInfo;
    private List<String> categories = new LinkedList<String>();
    private List<String> classification = new LinkedList<String>();
    private List<String> scientificAreas = new LinkedList<String>();
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public Collection<String> getClassification() {
        return classification;
    }

    public void setClassification(Collection<String> classification) {
        this.classification.addAll(classification);
    }

    public Collection<String> getScientificAreas() {
        return scientificAreas;
    }

    public void setScientificAreas(Collection<String> scientificAreas) {
        this.scientificAreas.addAll(scientificAreas);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public String getPublishedBy() {
        return publishedBy;
    }

    public void setPublishedBy(String publishedBy) {
        this.publishedBy = publishedBy;
    }

    public String getPublishedByLink() {
        return publishedByLink;
    }

    public void setPublishedByLink(String publishedByLink) {
        this.publishedByLink = publishedByLink;
    }

    public String getpIssn() {
        if (pIssn==null || pIssn.isEmpty()) return getIssn();
        else return pIssn;
    }

    public void setpIssn(String pIssn) {
        this.pIssn = pIssn;
    }

    public String geteIssn() {
        return eIssn;
    }

    public void seteIssn(String eIssn) {
        this.eIssn = eIssn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(Map<String, String> moreInfo) {
        this.moreInfo = moreInfo;
    }

    public Collection<String> getCategories() {
        return categories;
    }

    public void setCategories(Collection<String> categories) {
        this.categories.addAll(categories);
    }

    @Override
    public String toString() {
        return "Journal{" +
                "title='" + title + '\'' +
                ", shortTitle='" + shortTitle + '\'' +
                ", publishedBy='" + publishedBy + '\'' +
                ", publishedByLink='" + publishedByLink + '\'' +
                ", pIssn='" + pIssn + '\'' +
                ", eIssn='" + eIssn + '\'' +
                ", issn='" + issn + '\'' +
                ", description='" + description + '\'' +
                ", moreInfo=" + moreInfo +
                ", categories=" + categories +
                ", classification=" + classification +
                ", scientificAreas=" + scientificAreas +
                '}';
    }

    public String tabDelimitedString() {
        return getTitle() + "\t" +
                getShortTitle() + "\t" +
                getPublishedBy() + "\t" +
                getPublishedByLink() + "\t" +
                getpIssn() + "\t" +
                geteIssn() + "\t" +
                getDescription() + "\t" +
                printMap(moreInfo) + "\t" +
                printCollection(categories) + "\t" +
                printCollection(classification) + "\t" +
                printCollection(scientificAreas) + "\t" +
                getUrl();
    }

    public static String tabDelimitedHeader() {
        return "Title\t" +
                "Short title\t" +
                "Published by\t" +
                "Publisher link\t" +
                "p-ISSN\t" +
                "e-ISSN\t" +
                "Description\t" +
                "More info\t" +
                "Categories\t" +
                "Classification\t" +
                "Scientific areas\t" +
                "URL";
    }

    private String printMap(Map<String,String> map) {
        StringBuffer result = new StringBuffer();
        for (Map.Entry<String,String> e : map.entrySet()) result.append(e.getKey()+"->"+e.getValue()+"|");
        if (result.length() != 0) result.replace(result.length()-1, result.length(), ""); //to remove extra '|'
        return result.toString();
    }

    /**
     * Method outputs a multivalued field in a format appropriate for tab-delimited file
     * @param collection - collection of values to be printed
     * @return String that represents multivalued field
     */
    private String printCollection(List<String> collection) {
        StringBuffer result = new StringBuffer();
        if (collection.size() > 1) {
            for (String s : collection) result.append(s+"|");
            result.replace(result.length()-1, result.length(), ""); //to remove extra '|'
        } else {
            for (String s : collection) result.append(s);
        }

        return result.toString();
    }
}
