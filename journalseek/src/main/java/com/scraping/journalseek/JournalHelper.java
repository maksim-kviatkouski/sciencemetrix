package com.scraping.journalseek;

import com.scraping.helpers.ScrapeDownloader;
import com.scraping.helpers.ScrapeHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 6/23/13
 * Time: 6:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class JournalHelper {
    public static String ROOT_PAGE = "http://journalseek.net/";

    public static Collection<String> getLinksToJournals(){
        Collection<String> result = new HashSet<String>();
        for(String s : getLinksToCategories()){
                System.out.println("Recursing into category " + s);
            for (String ss : getLinksToSubcategories(s)){
                System.out.println("Getting journals from subcategory " + ss);
                result.addAll(getLinksToSubcategoryJournals(ss));
            }
        }
        System.out.println(result.size());
        return result;
    }

    private static Collection<String> getLinksToCategories(){
        Collection<String> result = new HashSet<String>();
        Document doc = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(ROOT_PAGE));
        List<String> hrefs = ScrapeHelper.queryValues("//TBODY/TR/TD[contains(.,'Category Browser')]/../../TR[2]/TD/A/@href", doc);
        for (String s : hrefs) {
            result.add(ROOT_PAGE + s);
        }
        return result;
    }

    private static Collection<String> getLinksToSubcategories(String linkToCategory){
        Collection<String> result = new HashSet<String>();
        Document doc = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(linkToCategory));
        List<String> hrefs = ScrapeHelper.queryValues("//A/@href[contains(.,'field=category')]", doc);
        result.addAll(hrefs);
        return result;
    }

    private static Collection<String> getLinksToSubcategoryJournals(String linkToSubcategory){
        Collection<String> result = new HashSet<String>();
        Document doc = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(linkToSubcategory));
        List<String> hrefs = ScrapeHelper.queryValues("//A/@href[contains(.,'field=issn')]", doc);
        result.addAll(hrefs);
        return result;
    }

    public static String extractTitle(Document doc){
        String result = "";
        Node node = ScrapeHelper.queryNode("//P[@class='heading2']/text()", doc);
        if (node != null) {
            result = node.getTextContent();
        }
        return clean(result);
    }

    public static String extractShortTitle(Document doc) {
        return clean(ScrapeHelper.queryText("//P[@class='heading2']/I", doc));
    }

    public static String extractPublishedHostedBy(Document doc) {
        String result = "";
        result = ScrapeHelper.queryText("//P[contains(.,'Published/Hosted by ')]", doc);
        Pattern publishedByPattern = Pattern.compile("Published/Hosted by ([^\\.]+)\\.");
        Matcher matcher = publishedByPattern.matcher(result);
        if (matcher.find()){
            result = matcher.group(1);
        }
        return clean(result);
    }

    public static String extractPublishedHostedByLink(Document doc) {
        return clean(ScrapeHelper.queryText("//P[contains(.,'Published/Hosted by ')]/A/@href", doc));
    }

    public static String extractPrintedISSN(Document doc) {
        String result = "";
        result = ScrapeHelper.queryText("//P[contains(., 'ISSN (printed): ')]", doc);
        Pattern printedISSNPattern = Pattern.compile("ISSN \\(printed\\): ([^\\.]+)\\.");
        Matcher matcher = printedISSNPattern.matcher(result);
        if (matcher.find()){
            result = matcher.group(1);
        }
        return clean(result);
    }

    public static String extractElectronicISSN(Document doc) {
        String result = "";
        result = ScrapeHelper.queryText("//P[contains(., 'ISSN (electronic): ')]", doc);
        Pattern electronicISSNPattern = Pattern.compile("ISSN \\(electronic\\): ([^\\.]+)\\.");
        Matcher matcher = electronicISSNPattern.matcher(result);
        if (matcher.find()){
            result = matcher.group(1);
        }
        return clean(result);
    }

    public static String extractISSN(Document doc) {
        String result = "";
        result = ScrapeHelper.queryText("//P[contains(., 'ISSN: ')]", doc);
        Pattern issnPattern = Pattern.compile("ISSN: ([^\\.]+)\\.");
        Matcher matcher = issnPattern.matcher(result);
        if (matcher.find()){
            result = matcher.group(1);
        }
        return clean(result);
    }

    public static String extractDescription(Document doc) {
        String result = "";
        NodeList descriptionCandidates = ScrapeHelper.queryNodes("//P[@class='heading2']/following-sibling::P", doc);
        for (int i = 0; i < descriptionCandidates.getLength(); i++){
            if (isDescription(descriptionCandidates.item(i).getTextContent())){
                result = descriptionCandidates.item(i).getTextContent();
                break;
            }
        }
        return clean(result);
    }

    private static boolean isDescription(String s) {
        String _s = clean(s);
        if (_s.startsWith("Published/Hosted by")) return false;
        if (_s.startsWith("Classification:")) return false;
        if (_s.startsWith("Scientific areas:")) return false;
        if (_s.startsWith("ISSN:")) return false;
        return true;
    }


    public static Map<String, String> extractMoreInfo(Document doc) {
        Map<String, String> result = new HashMap<String, String>();
        NodeList moreInfoNodes = ScrapeHelper.queryNodes("//DT[contains(.,'Further information')]/following-sibling::DD[1]//LI", doc);
        for (int i = 0; i < moreInfoNodes.getLength(); i++){
            String text = ScrapeHelper.queryText("./A/text()", moreInfoNodes.item(i));
            String link = ScrapeHelper.queryText("./A/@href", moreInfoNodes.item(i));
            text = clean(text);
            if (!text.isEmpty()) result.put(text, link);
        }
        return result;
    }

    public static Collection<String> extractCategories(Document doc) {
        Collection<String> result = new HashSet<String>();
        List<String> categories = ScrapeHelper.queryTextValues("//DT[contains(.,'Category Link')]/following-sibling::DD[1]//LI/A/text()", doc);
        for (String c : categories) result.add(clean(c));
        return result;
    }

    public static Collection<String> extractClassification(Document doc) {
        Collection<String> result = new HashSet<String>();

        Collection<String> textVals = ScrapeHelper.queryTextValues("//P[contains(.,'Classification: ')]//text()", doc);
        StringBuffer _text = new StringBuffer();
        for (String t : textVals) _text.append(t);
        String text = _text.toString();
        Pattern classificationPattern = Pattern.compile(".*Classification: (.*)");
        Matcher matcher1 = classificationPattern.matcher(text);
        if (matcher1.find()){
            text = matcher1.group(1);
            if (!"".equals(text)){
                String[] classes = text.split(";");
                for (int i = 0; i < classes.length; i++){
                    result.add(clean(classes[i]).replaceAll("\\.",""));
                }
            }
        }

        return result;
    }

    public static Collection<String> extractScientificAreas(Document doc) {
        Collection<String> result = new HashSet<String>();

        Collection<String> textVals = ScrapeHelper.queryTextValues("//P[contains(.,'Scientific areas: ')]//text()", doc);
        StringBuffer _text = new StringBuffer();
        for (String t : textVals) _text.append(t);
        String text = _text.toString();
        Pattern scientificAreasPattern = Pattern.compile(".*Scientific areas: (.*)");
        Matcher matcher1 = scientificAreasPattern.matcher(text);
        if (matcher1.find()){
            text = matcher1.group(1);
            if (!"".equals(text)){
                String[] classes = text.split(";");
                for (int i = 0; i < classes.length; i++){
                    result.add(clean(classes[i]).replaceAll("\\.",""));
                }
            }
        }

        return result;
    }

    public static Journal extractJournalFromURL(String link){
        Journal result = new Journal();

        Document doc = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(link));
        result.setTitle(extractTitle(doc));
        result.setShortTitle(extractShortTitle(doc));
        result.setpIssn(extractPrintedISSN(doc));
        result.seteIssn(extractElectronicISSN(doc));
        result.setIssn(extractISSN(doc));
        result.setPublishedBy(extractPublishedHostedBy(doc));
        result.setPublishedByLink(extractPublishedHostedByLink(doc));
        result.setDescription(extractDescription(doc));
        result.setMoreInfo(extractMoreInfo(doc));
        result.setCategories(extractCategories(doc));
        result.setClassification(extractClassification(doc));
        result.setScientificAreas(extractScientificAreas(doc));
        result.setUrl(link);

        return result;
    }

    public static String clean(String s){
        return s != null ? s.replaceAll("\r","").replaceAll("\n","").replaceAll("\t","").replaceAll("\u00a0","")
                .replaceAll("\u008e", "\u017d").trim() : "";
    }

}
