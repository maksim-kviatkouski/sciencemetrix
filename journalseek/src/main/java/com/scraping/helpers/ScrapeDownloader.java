package com.scraping.helpers;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;

import java.io.IOException;
import java.net.ProxySelector;

/**
 * User: Maksim_Kviatkouski
 * Date: 2/25/13 Time: 10:53 PM
 */
public class ScrapeDownloader {
    private static DefaultHttpClient httpClient = new DefaultHttpClient();
    static {
        //two lines below are written to have Apache HTTP Client respecting system proxy settings
        ProxySelectorRoutePlanner routePlanner = new ProxySelectorRoutePlanner(httpClient.getConnectionManager().getSchemeRegistry(), ProxySelector.getDefault());
        httpClient.setRoutePlanner(routePlanner);
    }

    public static String downloadLink(String url){
        String result = "";
        boolean done = false;
        int attempts = 0;

        HttpGet httpGet = new HttpGet(url);

        while (!done && attempts<=10) {
            try {
                //result = IOUtils.toString(URI.create(url));
                HttpResponse response = httpClient.execute(httpGet);
                result = IOUtils.toString(response.getEntity().getContent());
                done = true;
            } catch (Exception e) {
                System.out.println("Opps, something is wrong with link " + url + " Retrying...");
                e.printStackTrace(System.out);
                attempts++;
            }
        }
        return result;
    }

    public static String downloadLink(String url, HttpClient client){
        String result = "";
        HttpGet httpGet = new HttpGet(url);
        boolean done = false;
        int attempts = 0;

        while (!done && attempts++ < 10){
            try {
                HttpResponse response = client.execute(httpGet);
                result = IOUtils.toString(response.getEntity().getContent());
                done = true;
            } catch (IOException e) {
                System.out.println("Opps, something is wrong with link " + url + ". Retrying. " + e.getMessage());
            }
        }

        return result;
    }
}
