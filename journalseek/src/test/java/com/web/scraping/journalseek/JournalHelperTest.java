package com.web.scraping.journalseek;

import com.scraping.helpers.ScrapeDownloader;
import com.scraping.helpers.ScrapeHelper;
import com.scraping.journalseek.JournalHelper;
import org.junit.Test;
import org.w3c.dom.Document;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 6/23/13
 * Time: 6:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class JournalHelperTest {
    private static final String JOURNAL_LINK = "http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=1743-873X";
    private static final Document DOC = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(JOURNAL_LINK));

    private static final String CLASSIFICATION_TEST_JOURNAL_LINK = "http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=0389-9160";
    private static final Document CLASSIFICATION_TEST_DOC = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(CLASSIFICATION_TEST_JOURNAL_LINK));

    private static final String SCIENTIFIC_AREAS_TEST_JOURNAL_LINK = "http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=1846-4394";
    private static final Document SCIENTIFIC_AREAS_TEST_DOC = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(SCIENTIFIC_AREAS_TEST_JOURNAL_LINK));

    private static final String ADVANCED_DESCRIPTION_TEST_JOURNAL_LINK = "http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?query=1008-6994&field=issn";
    private static final Document ADVANCED_DESCRIPTION_TEST_DOC = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(ADVANCED_DESCRIPTION_TEST_JOURNAL_LINK));

    private static final String RESOUNDINGS_TEST_JOURNAL_LINK = "http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=1091-8728";
    private static final Document RESOUNDINGS_TEST_DOC = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(RESOUNDINGS_TEST_JOURNAL_LINK));

    public static final String SCIENTIFIC_AREAS_TEST_JOURNAL_LINK_2 = "http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=0031-5362";
    public static final Document SCIENTIFIC_AREAS_TEST_DOC_2 = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(SCIENTIFIC_AREAS_TEST_JOURNAL_LINK_2));

    public static final String CLASSIFICATION_TEST_JOURNAL_LINK_2 = "http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=0377-5038";
    public static final Document CLASSIFICATION_TEST_DOC_2 = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(CLASSIFICATION_TEST_JOURNAL_LINK_2));

    public static final String MORE_INFO_TEST_JOURNAL_LINK = "http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=0044-4502";
    public static final Document MORE_INFO_TEST_DOC = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(MORE_INFO_TEST_JOURNAL_LINK));

    public static final String MISSING_TITLE_TEST_JOURNAL_LINK = "http://journalseek.net/cgi-bin/journalseek/journalsearch.cgi?field=issn&query=1532-4494";
    public static final Document MISSING_TITLE_TEST_DOC = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(MISSING_TITLE_TEST_JOURNAL_LINK));

    @Test
    public void testGetLinksToJournals() throws Exception {
        //System.out.println(JournalHelper.getLinksToJournals());
    }

    @Test
    public void testExtractTitle(){
        assertEquals("Journal of Heritage Tourism", JournalHelper.extractTitle(DOC));
    }

    @Test
    public void testExtractShortTitle(){
        assertEquals("J Herit Tourism", JournalHelper.extractShortTitle(DOC));
    }

    @Test
    public void testPublishedHostedBy(){
        assertEquals("Multilingual Matters and Channel View Publications", JournalHelper.extractPublishedHostedBy(DOC));
    }

    @Test
    public void testPublishedHostedByLink(){
        assertEquals("http://www.multilingual-matters.com/", JournalHelper.extractPublishedHostedByLink(DOC));
    }

    @Test
    public void testPrintedISSN(){
        assertEquals("1743-873X", JournalHelper.extractPrintedISSN(DOC));
    }

    @Test
    public void testElectronicISSN(){
        assertEquals("1747-6631", JournalHelper.extractElectronicISSN(DOC));
    }

    @Test
    public void testDescrpition(){
        assertEquals("The Journal of Heritage Tourism (JHT) is a peer-reviewed, international transdisciplinary journal. JHT focuses on exploring the many facets of one of the most notable and widespread types of tourism. Heritage Tourism is among the very oldest forms of travel. Activities such as visits to sites of historical importance, including built environments and urban areas, rural and agricultural landscapes, natural regions, locations where historic events occurred and places where interesting and significant living cultures dominate are all forms of Heritage Tourism. As such, this form of tourism dominates the industry in many parts of the world and involves millions of people. During the past 20 years, the study of tourism has become highly fragmented and specialised into various theme areas, or concentrations. Within this context, heritage tourism is one of the most commonly investigated forms of tourism, and hundreds of scholars and industry workers are involved in researching its dynamics and concepts. This academic attention has resulted in the publication of hundreds of refereed articles in various scholarly media, yet, until now there has been no journal devoted specifically to heritage tourism. Now Channel View Publications, a market-leading publisher in the field, is launching the Journal of Heritage Tourism to fill this gap. JHT will seek to critically examine all aspects of Heritage Tourism. Some of the topics to be explored within the context of Heritage Tourism will include colonial heritage, commodification, interpretation, urban renewal, religious tourism, genealogy, patriotism, nostalgia, folklore, power, funding, contested heritage, historic sites, identity, industrial heritage, marketing, conservation, ethnicity, education and indigenous heritage. Journal of Heritage Tourism will begin in early 2006. Volume 1 will consist of two issues with 4 issues per volume from Volume 2 (2007) onwards.",
                JournalHelper.extractDescription(DOC));
    }

    @Test
    public void testMoreInfo(){
        Map<String, String> expected = new HashMap<String, String>(){{put("Journal of Heritage Tourism website", "http://www.multilingual-matters.net/jht/default.htm");}};
        assertEquals(expected, JournalHelper.extractMoreInfo(DOC));
    }

    @Test
    public void testCategories(){
        Collection<String> expected = new HashSet<String>(){{add("Business Administration  - Marketing"); add("Sports and Recreation - Tourism");
            add("Environmental Sciences  - Environmental Conservation"); add("Arts and Literature - Folklore");}};
        assertEquals(expected, JournalHelper.extractCategories(DOC));
    }

    @Test
    public void testClassification(){
        Collection<String> expected = new HashSet<String>(){{add("Architecture"); add("City planning");}};
        assertEquals(expected, JournalHelper.extractClassification(CLASSIFICATION_TEST_DOC));
    }

    @Test
    public void testScientificAreas(){
        Collection<String> expected = new HashSet<String>(){{add("Architecture and urban planning"); add("Public health and health care");
            add("Social sciences"); add("History"); add("Archeology"); add("Philosophy"); add("Art sciences"); add("Ethnology and anthropology");
            add("History of art");}};
        assertEquals(expected, JournalHelper.extractScientificAreas(SCIENTIFIC_AREAS_TEST_DOC));
    }

    @Test
    public void testISSN(){
        String expected = "1846-4394";
        assertEquals(expected, JournalHelper.extractISSN(SCIENTIFIC_AREAS_TEST_DOC));
    }

    @Test
    public void setAdvancedDescriptionTest() {
        String expected = "";
        assertEquals(expected, JournalHelper.extractDescription(ADVANCED_DESCRIPTION_TEST_DOC));
    }

    @Test
    public void resoundingsISSNTest(){
        String expected = "1091-8728";
        assertEquals(expected, JournalHelper.extractISSN(RESOUNDINGS_TEST_DOC));
    }

    @Test
    public void resoundingsCategoryTest(){
        Collection<String> expected = new HashSet<String>(){{
            add("Humanities  - Languages and Literature");
        }};
        assertEquals(expected, JournalHelper.extractCategories(RESOUNDINGS_TEST_DOC));
    }

    @Test
    public void scientificTest(){
        Collection<String> expected = new HashSet<String>(){{
            add("Biology");
            add("Biomedicine and health");
            add("Basic medical sciences");
            add("Clinical medicine");
            add("Public health and health care");
            add("Forestry");
            add("Biotechnology");
        }};
        assertEquals(expected, JournalHelper.extractScientificAreas(SCIENTIFIC_AREAS_TEST_DOC_2));
    }

    @Test
    public void classificationTest(){
        Collection<String> expected = new HashSet<String>(){{
            add("Abnormalities, Human");
            add("Cytology");
            add("Embryology");
            add("Embryology, Human");
            add("Human anatomy");
            add("Pathology");
        }};
        assertEquals(expected, JournalHelper.extractClassification(CLASSIFICATION_TEST_DOC_2));
    }

    @Test
    public void moreInfoTest(){
        Map<String,String> expected = new HashMap<String, String>(){{
            put("Žurnal Analiticeskoj Himii website", "http://www.maik.rssi.ru/cgi-bin/list.pl?page=ankhim");
        }};
        Map<String, String> actual = JournalHelper.extractMoreInfo(MORE_INFO_TEST_DOC);
        assertEquals(expected, actual);
    }

    @Test
    public void missingTitleTest() {
        String expected = "Today's Chemist at Work";
        assertEquals(expected, JournalHelper.extractTitle( MISSING_TITLE_TEST_DOC ));
    }
}
