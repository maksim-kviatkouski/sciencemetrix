@echo off
setlocal enableextensions enabledelayedexpansion
set /a "x = 0"
:while1
    if %x% leq 9716 (
        echo Downloading journal #%x%
        curl -x http://localhost:80 http://journals.indexcopernicus.com/passport.php?id=%x% > tmp.tmp
        set /a "x = x + 1"
        goto :while1
    )
endlocal