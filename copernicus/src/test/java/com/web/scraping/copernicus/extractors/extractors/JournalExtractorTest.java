package com.web.scraping.copernicus.extractors.extractors;

import com.web.scraping.copernicus.extractors.JournalExtractor;
import com.web.scraping.helpers.ScrapeHelper;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/21/13 Time: 8:29 PM
 */
public class JournalExtractorTest {
    private static Document doc;
    @Before
    public void setUp() {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("test/java/com/web/scraping/copernicus/extractors/extractors/firstJournal.html");
        StringWriter sw = new StringWriter();
        try {
            IOUtils.copy(is, sw);
            doc = ScrapeHelper.getDocument(sw.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void extractMainTitleTest() throws Exception {
        String test_mainTitle = "DOGU ARASTIRMALARI";
        String mainTitle = JournalExtractor.getMainTitle(doc);
        assertTrue(test_mainTitle.equals(mainTitle));
    }

    @Test
    public void extractOtherTitleTest() throws Exception {
        String test_otherTitle = "A JOURNAL OF ORIENTAL STUDIES";
        String otherTitle = JournalExtractor.getOtherTitle(doc);
        assertTrue(test_otherTitle.equals(otherTitle));
    }

    @Test
    public void getEISSNTest() throws Exception {
        String test_eISSN = "1307-6256";
        String eISSN = JournalExtractor.getEISSN(doc);
        assertTrue(test_eISSN.equals(eISSN));
    }

    @Test
    public void getURL() throws Exception {
        String test_url = "http://www.doguedebiyati.com/doguarastirmalari.htm";
        String url = JournalExtractor.getURL(doc);
        assertTrue(test_url.equals(url));
    }

    @Test
    public void getCountry() throws Exception {
        String test_country = "Turkey";
        String country = JournalExtractor.getCountry(doc);
        assertTrue(test_country.equals(country));
    }

    @Test
    public void getFrequency() throws Exception {
        String test_frequency = "Half-yearly";
        String frequency = JournalExtractor.getFrequency(doc);
        assertTrue(test_frequency.equals(frequency));
    }

    @Test
    public void getInsertToIC() throws Exception {
        String test_insertToIC = "2012-01-30";
        String insertToIC = JournalExtractor.getInsertToIC(doc);
        assertTrue(test_insertToIC.equals(insertToIC));
    }

    @Test
    public void getPublicationYear() throws Exception {
        String test_pubYear = "2008";
        String pubYear = JournalExtractor.getPublicationYear(doc);
        assertTrue(test_pubYear.equals(pubYear));
    }

    @Test
    public void getEPublicationYear() throws Exception {
        String test_ePubYear = "2008";
        String ePubYear = JournalExtractor.getEPublicationYear(doc);
        assertTrue(test_ePubYear.equals(ePubYear));
    }

    @Test
    public void getChiefEditor() throws Exception {
        String test_chiefEditor = "ALI GUZELYUZ";
        String chiefEditor = JournalExtractor.getChiefEditor(doc);
        assertTrue(test_chiefEditor.equals(chiefEditor));
    }

    @Test
    public void getFulltextLanguages() throws Exception {
        List<String> test_fulltextLanguages = new LinkedList<String>(){{add("Farsi"); add("Turkish");}};
        List<String> fulltextLanguages = JournalExtractor.getFulltextLanguages(doc);
        assertTrue(test_fulltextLanguages.equals(fulltextLanguages));
    }

    @Test
    public void getAbstractLanguages() throws Exception {
        List<String> test_abstractLanguages = new LinkedList<String>(){{add("English"); add("Turkish");}};
        List<String> abstractLanguages = JournalExtractor.getAbstractLanguage(doc);
        assertTrue(test_abstractLanguages.equals(abstractLanguages));
    }

    @Test
    public void getEPublisher() throws Exception {
        String test_ePublisher = "ISTANBUL UNIVERSITY FACULTY OF LETTERS";
        String ePublisher = JournalExtractor.getEPublisher(doc);
        assertTrue(test_ePublisher.equals(ePublisher));
    }

    @Test
    public void getJournalDescriptions() throws Exception {
        Map<String,String> test_description = new HashMap<String, String>(){{
            put("English","Studies of oriental languages, literature, history, geography, philosophy, culture and art.");
            put("Turkish","Doğu, dil, edebiyat, tarih, coğrafya, felsefe, kültür ve sanat araştırmaları.");}};
        Map<String,String> descriptions = JournalExtractor.getDescriptions(doc);
        System.out.println(descriptions);
        assertTrue(test_description.equals(descriptions));
    }

    @Test
    public void getICValue() throws Exception {
        String test_ICValue = "not indexed";
        String icValue = JournalExtractor.getICValue(doc);
        assertTrue(test_ICValue.equals(icValue));
    }

    @Test
    public void getICCurrentStatus() throws  Exception {
        String test_ICCurrentStatus = "not indexed no copies from last year";
        String icCurrentStatus = JournalExtractor.getICCurrentStatus(doc);
        assertTrue(test_ICCurrentStatus.equals(icCurrentStatus));
    }

    @Test
    public void getICVHistory() throws Exception {
        List<String> test_ICVHistory = new LinkedList<String>(){{
            add("no historical data");
        }};
        List<String> icvHistory = JournalExtractor.getICVHistory(doc);
        assertTrue(test_ICVHistory.equals(icvHistory));
    }

    @Test
    public void getEditorialOfficeAddr() throws  Exception {
        List<String> test_addr = new LinkedList<String>(){{
            add("ISTANBUL UNIVERSITY FACULTY OF LETTERS");
            add("34450 ISTANBUL");
            add("ORDU STREET");
            add("Turkey");
        }};

        List<String> addr = JournalExtractor.getEditorialOfficeAddr(doc);
        assertTrue(test_addr.equals(addr));
    }

    @Test
    public void getEditorInChief() throws  Exception {
        List<String> test_editorInChief = new LinkedList<String>(){{
            add("ALI GUZELYUZ");
            add("E-mail: guzelyuz@gmail.com");
            add("Telefon: 90-5323923678-");
        }};

        List<String> editorInChief = JournalExtractor.getEditorInChief(doc);
        assertTrue(test_editorInChief.equals(editorInChief));
    }

    @Test
    public void getPublishedByElectronic() throws Exception {
        List<String> test_publishedElectronic = new LinkedList<String>(){{
            add("ISTANBUL UNIVERSITY FACULTY OF LETTERS");
            add("34450 ISTANBUL");
            add("ORDU STREET");
            add("Turkey");
            add("E-mail: admin@doguedebiyati.com");
            add("Telefon: 90-532-3923678");
        }};

        List<String> publishedElectronic = JournalExtractor.getPublishedByElectronic(doc);
        assertTrue(test_publishedElectronic.equals(publishedElectronic));
    }

    @Test
    public void getCharacterOfInfo() throws Exception {
        String test_char = "Scientifically  Information";
        String character = JournalExtractor.getCharacterOfInfo(doc);
        assertTrue(test_char.equals(character));
    }

    @Test
    public void getIsiJournalMasterList() throws Exception {
        String test_isi = "No";
        String isi = JournalExtractor.getIsiJournalMasterList(doc);
        assertTrue(test_isi.equals(isi));
    }

    @Test
    public void getScienceCitationIndexExpanded() throws Exception {
        String test = "No";
        String citation = JournalExtractor.getScienceCitationIndexExpanded(doc);
        assertTrue(test.equals(citation));
    }

    @Test
    public void getBiobase() throws Exception {
        String test_bio = "No";
        String bio = JournalExtractor.getBiobase(doc);
        assertTrue(test_bio.equals(bio));
    }

    @Test
    public void getCAS() throws Exception {
        String test_cas = "No";
        String cas = JournalExtractor.getCAS(doc);
        assertTrue(test_cas.equals(cas));
    }

    @Test
    public void getERIH() throws Exception {
        String test_erih = "No";
        String erih = JournalExtractor.getERIH(doc);
        assertTrue(test_erih.equals(erih));
    }

    @Test
    public void getScopus() throws Exception {
        String test_scopus = "No";
        String scopus = JournalExtractor.getScopus(doc);
        assertTrue(test_scopus.equals(scopus));
    }

    @Test
    public void getCurrentContents() throws Exception {
        String test_current = "Yes";
        String current = JournalExtractor.getCurrentContents(doc);
        assertTrue(test_current.equals(current));
    }

    @Test
    public void getIndexMedicus() throws Exception {
        String test_index = "No";
        String index = JournalExtractor.getIndexMedicus(doc);
        assertTrue(test_index.equals(index));
    }

    @Test
    public void getExpertaMedica() throws Exception {
        String test_experta = "No";
        String experta = JournalExtractor.getExpertaMedica(doc);
        assertTrue(test_experta.equals(experta));
    }

    @Test
    public void getPublishedBy() throws Exception {
        String test_publishedBy = "";
        String publishedBy = JournalExtractor.getPublishedBy(doc);
        assertTrue(test_publishedBy.equals(publishedBy));
    }

    @Test
    public void getScientificDisciplines() throws Exception {
        List<String> test_s = new LinkedList<String>(){{
            add("Social sciences:Education");
            add("Social sciences:History");
            add("Social sciences:Philosophy");
            add("Social sciences:Linguistics");
            add("Social sciences:Ethics");
            add("Geography:Human geography");
        }};
        List<String> s = JournalExtractor.getScientificDesciplines(doc);
        assertEquals(test_s, s);
    }
}
