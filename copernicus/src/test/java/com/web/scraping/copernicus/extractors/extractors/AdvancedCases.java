package com.web.scraping.copernicus.extractors.extractors;

import com.web.scraping.copernicus.extractors.JournalBuilder;
import com.web.scraping.copernicus.model.Journal;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 6/27/13
 * Time: 12:46 AM
 * To change this template use File | Settings | File Templates.
 */
public class AdvancedCases {
    private final static String FIRST_CASE_URL = "http://journals.indexcopernicus.com/passport.php?id=295";

    @Test
    public void firstCaseTest(){
        Journal j = new JournalBuilder(FIRST_CASE_URL).buildJournal();
    }
}
