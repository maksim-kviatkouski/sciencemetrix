package com.web.scraping.copernicus.extractors.extractors;

import com.web.scraping.copernicus.extractors.JournalListExtractor;
import com.web.scraping.copernicus.model.JournalListing;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/20/13 Time: 7:23 PM
 */
public class JournalListExtractorTest {
    @Test
    public void testExtractListOfJournals() throws Exception {
        List<String> listOfJournals = JournalListExtractor.extractListOfJournals("http://journals.indexcopernicus.com/masterlist.php?name=Master&litera=A&start=0&skok=5");
        List<String> test_list = new LinkedList<String>();
        test_list.add("karta.php?action=masterlist&id=7544");
        test_list.add("karta.php?action=masterlist&id=2159");
        test_list.add("karta.php?action=masterlist&id=8177");
        test_list.add("karta.php?action=masterlist&id=8387");
        test_list.add("karta.php?action=masterlist&id=7374");
        Assert.assertTrue(test_list.equals(listOfJournals));
    }

    @Test
    public void testGetJournalListing() throws Exception {
        JournalListing journalListing = JournalListExtractor.getJournalListing();
        System.out.println(journalListing.getLinks());
    }
}
