package com.web.scraping.copernicus.extractors.extractors;

import com.web.scraping.copernicus.extractors.JournalBuilder;
import com.web.scraping.copernicus.model.Journal;
import org.junit.Test;

import java.io.PrintWriter;

/**
 * User: Maksim_Kviatkouski
 * Date: 4/3/13 Time: 1:03 AM
 */
public class MainTest {
    @Test
    public void test() throws Exception {
        JournalBuilder jBuilder = new JournalBuilder("http://journals.indexcopernicus.com/passport.php?id=1774");
        Journal j = jBuilder.buildJournal();
        PrintWriter pw = new PrintWriter("test_out.txt");
        pw.write(Journal.tabDelimitedHeader() + "\r\n");
        pw.write(j.tabDelimitedFields());
        pw.close();
    }
}
