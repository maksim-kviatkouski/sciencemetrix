package com.web.scraping.copernicus.extractors.extractors;

import com.web.scraping.copernicus.extractors.JournalExtractor;
import com.web.scraping.helpers.ScrapeHelper;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * User: Maksim_Kviatkouski
 * Date: 4/1/13 Time: 10:40 PM
 */
public class JournalExtractorTest2 {
    private static Document doc;
    @Before
    public void setUp() {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("test/java/com/web/scraping/copernicus/extractors/extractors/secondJournal.html");
        StringWriter sw = new StringWriter();
        try {
            IOUtils.copy(is, sw);
            doc = ScrapeHelper.getDocument(sw.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPublisedBy() throws Exception {
        String testPublishedBy = "Acta  Agrestia  Sinica";
        String publishedBy = JournalExtractor.getPublishedBy(doc);
        assertTrue(testPublishedBy.equals(publishedBy));
    }

    @Test
    public void testPublishedByFull() throws Exception {
        List<String> test_publishedByFull = new LinkedList<String>(){{
            add("Acta  Agrestia  Sinica");
            add("100193 Beijing");
            add("No.2 Yuanmingyuan West Road, Haidian District");
            add("China (mainland)");
        }};
        List<String> publishedByFull = JournalExtractor.getPublishedByFull(doc);
        assertTrue(test_publishedByFull.equals(publishedByFull));
    }

    @Test
    public void testExecutiveEditor() throws Exception {
        String testExecutiveEdotir = "Jie Cai";
        String executiveEditor = JournalExtractor.getExecutiveEditor(doc);
        assertTrue(testExecutiveEdotir.equals(executiveEditor));
    }

    @Test
    public void testExecutiveEditorFull() throws Exception {
        List<String> testExecutiveEditorFull = new LinkedList<String>(){{
            add("Jie Cai");
            add("E-mail: cdxb@cau.edu.cn");
            add("Telefon: 86-10-62733894");
        }};
        List<String> executiveEditorFull = JournalExtractor.getExecutiveEditorFull(doc);
        assertEquals(testExecutiveEditorFull, executiveEditorFull);
    }

    @Test
    public void testScientificDisciplines() throws Exception {
        List<String> test_s = new LinkedList<String>(){{
            add("Biology:");
        }};
        List<String> s = JournalExtractor.getScientificDesciplines(doc);
        assertEquals(test_s, s);
    }

    @Test
    public void getPISSN() throws Exception {
        String test_pISSN = "1007-0435";
        String pISSN = JournalExtractor.getPISSN(doc);
        assertTrue(test_pISSN.equals(pISSN));
    }
}
