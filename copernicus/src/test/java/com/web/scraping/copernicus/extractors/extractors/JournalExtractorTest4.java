package com.web.scraping.copernicus.extractors.extractors;

import com.web.scraping.copernicus.extractors.JournalExtractor;
import com.web.scraping.helpers.ScrapeHelper;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import static org.junit.Assert.assertTrue;

/**
 * User: Maksim_Kviatkouski
 * Date: 4/2/13 Time: 12:43 AM
 */
public class JournalExtractorTest4 {
    private static Document doc;

    @Before
    public void setUp() {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("test/java/com/web/scraping/copernicus/extractors/extractors/fourthJournal.html");
        StringWriter sw = new StringWriter();
        try {
            IOUtils.copy(is, sw);
            doc = ScrapeHelper.getDocument(sw.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getOtherIndexes(){
        String test_indexes = "ASOS Index; ";
        String indexes = JournalExtractor.getOtherIndexes(doc);
        assertTrue(test_indexes.equals(indexes));
    }
}
