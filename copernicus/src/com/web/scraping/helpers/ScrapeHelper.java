package com.web.scraping.helpers;

import org.cyberneko.html.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import javax.xml.xpath.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * User: Maksim_Kviatkouski
 * Date: 2/25/13 Time: 10:46 PM
 */
public class ScrapeHelper {

    private static final XPathFactory xPathFactory = XPathFactory.newInstance();
    private static final XPath xPath= xPathFactory.newXPath();
    private static final Map<String,XPathExpression> xPathCache = new HashMap<String, XPathExpression>();

    public static List<String> queryValues(String xPathQuery, Document doc) {
        List<String> result = new LinkedList<String>();
        XPathExpression xPathExpression = null;
        NodeList nodeList = null;
        try {
            xPathExpression = getXPath(xPathQuery);
            nodeList = (NodeList) xPathExpression.evaluate(doc, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < nodeList.getLength(); i++){
            result.add(nodeList.item(i).getNodeValue());
        }
        return result;
    }

    private static XPathExpression getXPath(String xPathQuery) {
        XPathExpression xPathExpression;
        if (!xPathCache.containsKey(xPathQuery)) {
            try {
                xPathCache.put(xPathQuery, xPath.compile(xPathQuery));
            } catch (XPathExpressionException e) {
                e.printStackTrace();
            }
        }
        return xPathCache.get(xPathQuery);
    }

    public static List<String> queryTextValues(String xPathQuery, Document doc) {
        List<String> result = new LinkedList<String>();
        XPathExpression xPathExpression = null;
        NodeList nodeList = null;
        try {
            xPathExpression = getXPath(xPathQuery);
            nodeList = (NodeList) xPathExpression.evaluate(doc, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }

        if (nodeList != null) {
            for (int i = 0; i < nodeList.getLength(); i++){
                result.add(nodeList.item(i).getTextContent());
            }
        }
        return result;
    }

    public static String queryText(String xPathQuery, Node doc){
        String result = "";
        XPathExpression xPathExpression = null;
        try {
            xPathExpression = getXPath(xPathQuery);
            result = (String) xPathExpression.evaluate(doc, XPathConstants.STRING);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Node queryNode(String xPathQuery, Node node){
        Node result = null;
        XPathExpression xPathExpression = null;
        try {
            xPathExpression = getXPath(xPathQuery);
            result = (Node) xPathExpression.evaluate(node, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static NodeList queryNodes(String xPathQuery, Node node){
        NodeList result = null;
        XPathExpression xPathExpression = null;
        try {
            xPathExpression = getXPath(xPathQuery);
            result = (NodeList) xPathExpression.evaluate(node, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Parses html page source into a DOM tree which can be queried then with XPath
     * @param html - html source of the page
     * @return DOM
     */
    public static Document getDocument(String html) {
        Document doc;
        DOMParser parser = new DOMParser();
        try {
            parser.setFeature("http://xml.org/sax/features/namespaces", false);
            parser.setFeature("http://cyberneko.org/html/features/scanner/ignore-specified-charset", true);
            parser.setProperty("http://cyberneko.org/html/properties/default-encoding", "UTF-8");
            parser.parse(new InputSource(new ByteArrayInputStream(html.getBytes())));
        } catch (SAXNotRecognizedException e) {
            e.printStackTrace();
        } catch (SAXNotSupportedException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        doc = parser.getDocument();
        return doc;
    }
}
