package com.web.scraping.helpers;

import com.web.scraping.copernicus.Constants;
import com.web.scraping.copernicus.extractors.JournalExtractor;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpConnection;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.DefaultHttpClientConnection;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpRequest;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.DefaultedHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestExecutor;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Calendar;

/**
 * User: Maksim_Kviatkouski
 * Date: 2/25/13 Time: 10:53 PM
 */
public class ScrapeDownloader {
    private final static int MAX_DOWNLOAD_ATTEMPTS = 50;

    private static HttpClient httpClient = new DefaultHttpClient();
    private static HttpContext httpContext = new BasicHttpContext();
    private static CookieStore cookieStore = new BasicCookieStore();
    static {
        httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BEST_MATCH);
        httpContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
    }

    public static void setEnglishLanguage(){
        HttpGet httpGet = new HttpGet(Constants.SITE_DOMAIN + Constants.SWITCH_TO_ENGLISH_PAGE);
        try {
            IOUtils.toString(httpClient.execute(httpGet, httpContext).getEntity().getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String downloadLink(String url){
        String result = "";
        boolean done = false;
        int attempts = 0;

        HttpGet httpGet = new HttpGet(url);
        /*httpGet.setHeader("Accept-Language","en-US,en;q=0.8");
        httpGet.setHeader("Connection","keep-alive");
        httpGet.setHeader("Referer","http://journals.indexcopernicus.com");
        httpGet.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22");
        httpGet.setHeader("Host", "journals.indexcopernicus.com");
        httpGet.setHeader("Cookie","PHPSESSID=hude2iv8op2a57kkeustces106; __utma=17803426.1050508655.1371734955.1373282452.1373374056.18; __utmb=17803426.6.10.1373374056; __utmc=17803426; __utmz=17803426.1371734955.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _pk_id.1.b48f=685e31a762695561.1371734955.19.1373374060.1373282509.; _pk_ses.1.b48f=*");*/

        while (!done && attempts <= MAX_DOWNLOAD_ATTEMPTS) {
            try {
                //result = IOUtils.toString(URI.create(url), Charset.forName("UTF-8"));
                HttpResponse response = httpClient.execute(httpGet, httpContext);
                result = IOUtils.toString(response.getEntity().getContent());
                //
                String id = Long.toString(Calendar.getInstance().getTimeInMillis());
                if (!"".equals(JournalExtractor.extractId(url))) id = JournalExtractor.extractId(url);
                OutputStreamWriter dump = new OutputStreamWriter(new FileOutputStream("dump"+ id), "UTF-8");
                dump.write(result);
                dump.close();
                //
                //TODO: replace with HTTP status check if possible
                if (!result.contains("Access forbidden!")) { done = true;}
                else { System.out.println("Access forbiden!"); Thread.sleep(2000);}
            } catch (Exception e) {
                System.out.println("Opps, something is wrong with link " + url + " Retrying...");
                //e.printStackTrace(System.out);
                attempts++;
            }
        }
        if (attempts > 0){
            System.out.println("[INFO]: Downloaded " + url + " successfully after " + attempts + " attempts.");
        }
        return result;
    }

    public static String downloadLink(String url, HttpClient client){
        String result = "";
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Referer","http://journals.indexcopernicus.com");
        httpGet.setHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22");
        httpGet.setHeader("Host", "journals.indexcopernicus.com");
        httpGet.setHeader("Referer","http://journals.indexcopernicus.com/karta.php?action=masterlist&id=7544");
        httpGet.setHeader("Cookie","PHPSESSID=hude2iv8op2a57kkeustces106; __utma=17803426.1050508655.1371734955.1373240898.1373244016.16; __utmb=17803426.10.10.1373244016; __utmc=17803426; __utmz=17803426.1371734955.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _pk_id.1.b48f=685e31a762695561.1371734955.17.1373244114.1373240978.; _pk_ses.1.b48f=*");
        boolean done = false;
        int attempts = 0;

        while (!done && attempts++ < 10){
            try {
                HttpResponse response = client.execute(httpGet);
                result = IOUtils.toString(response.getEntity().getContent());
                done = true;
            } catch (IOException e) {
                System.out.println("Opps, something is wrong with link " + url + ". Retrying. " + e.getMessage());
            }
        }

        return result;
    }
}
