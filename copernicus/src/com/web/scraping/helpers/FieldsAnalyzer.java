package com.web.scraping.helpers;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 2/28/13
 * Time: 11:08 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * Helper class which can accumulate info about available fields of each journal to tell
 * then what is a maximum cardinality of each field to help me to decided how many output files I should have:
 * one file for all single-valued fields and one file for each of multivalued fields
 */
public class FieldsAnalyzer {
    private static Map<String, Integer> cardinalityMap = new HashMap<String, Integer>();

    public static void loadJournalFields(List<String> fields){
        for (String f : fields){
            int count = Collections.frequency(fields, f);
            if (!cardinalityMap.containsKey(f) || cardinalityMap.get(f) < count){
                cardinalityMap.put(f, count);
            }
        }
    }

    public static Map<String, Integer> getCardinalityMap() {
        return cardinalityMap;
    }

    public static void setCardinalityMap(Map<String, Integer> cardinalityMap) {
        FieldsAnalyzer.cardinalityMap = cardinalityMap;
    }
}
