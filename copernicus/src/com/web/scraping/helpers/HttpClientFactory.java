package com.web.scraping.helpers;

import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * User: Maksim_Kviatkouski
 * Date: 2/28/13 Time: 11:31 PM
 */

/**
 * Helper class that provides internal logic of HttpClient objects pool
 */
public class HttpClientFactory implements PoolableObjectFactory<HttpClient> {
    @Override
    public HttpClient makeObject() throws Exception {
        return new DefaultHttpClient();
    }

    @Override
    public void destroyObject(HttpClient obj) throws Exception {
        return;
    }

    @Override
    public boolean validateObject(HttpClient obj) {
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void activateObject(HttpClient obj) throws Exception {
        return;
    }

    @Override
    public void passivateObject(HttpClient obj) throws Exception {
        return;
    }
}
