package com.web.scraping.copernicus.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/20/13 Time: 7:15 PM
 */
public class JournalListing {
    private Collection<String> links = new HashSet<String>();

    public Collection<String> getLinks() {
        return links;
    }

    public void addLinks(Collection<String> moreLinks){
        links.addAll(moreLinks);
    }
}
