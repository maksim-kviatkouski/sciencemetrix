package com.web.scraping.copernicus.model;

import java.util.*;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/21/13 Time: 8:28 PM
 */
public class Journal {
    private String mainTitle;
    private String mainTitleLanguage;
    private String otherTitle;
    private String eISSN;
    private String url;
    private String country;
    private String frequency;
    private String insertToIC;
    private String publicationYear;
    private String ePublicationYear;
    private String chiefEditor;
    private List<String> fulltextLanguages;
    private List<String> abstractLanguage;
    private String ePublisher;
    private Map<String, String> descriptions;
    private String icValue;
    //there is only "Current status 2012" field (no 2013 or 2011)
    private String icCurrentStatus;
    private List<String> icvHistory;
    private List<String> editorialOfficeAddr;
    private List<String> editorInChief;
    private List<String> publishedByElectronic;
    private String characterOfInfo;
    private String isiJournalMasterList;
    private String scienceCitationIndexExpanded;
    private String biobase;
    private String cas;
    private String erih;
    private String scopus;
    private String currentContents;
    private String indexMedicus;
    private String expertaMedica;
    private String publishedBy;
    private List<String> publishedByFull;
    private String executiveEditor;
    private List<String> scientificDisciplines;
    private String pISSN;
    private String affiliation;
    private String otherIndexes;
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private static final Set<String> descriptionLanguages = new TreeSet<String>();
    public static int maxNumberOfDescriptions = 0;

    private String linkAtCopernicus;

    public String getLinkAtCopernicus() {
        return linkAtCopernicus;
    }

    public void setLinkAtCopernicus(String linkAtCopernicus) {
        this.linkAtCopernicus = linkAtCopernicus;
    }

    public static Set<String> getDescriptionLanguages() {
        return descriptionLanguages;
    }

    public String getAbbreviationTitle() {
        return abbreviationTitle;
    }

    public void setAbbreviationTitle(String abbreviationTitle) {
        this.abbreviationTitle = abbreviationTitle;
    }

    private String abbreviationTitle;

    public String getOtherIndexes() {
        return otherIndexes;
    }

    public void setOtherIndexes(String otherIndexes) {
        this.otherIndexes = otherIndexes;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getpISSN() {
        return pISSN;
    }

    public void setpISSN(String pISSN) {
        this.pISSN = pISSN;
    }

    public List<String> getScientificDisciplines() {
        return scientificDisciplines;
    }

    public void setScientificDisciplines(List<String> scientificDisciplines) {
        this.scientificDisciplines = scientificDisciplines;
    }

    public List<String> getExecutiveEditorFull() {
        return executiveEditorFull;
    }

    public void setExecutiveEditorFull(List<String> executiveEditorFull) {
        this.executiveEditorFull = executiveEditorFull;
    }

    public String getExecutiveEditor() {
        return executiveEditor;
    }

    public void setExecutiveEditor(String executiveEditor) {
        this.executiveEditor = executiveEditor;
    }

    private List<String> executiveEditorFull;

    public String getPublishedBy() {
        return publishedBy;
    }

    public void setPublishedBy(String publishedBy) {
        this.publishedBy = publishedBy;
    }

    public List<String> getPublishedByFull() {
        return publishedByFull;
    }

    public void setPublishedByFull(List<String> publishedByFull) {
        this.publishedByFull = publishedByFull;
    }

    public String getMainTitle() {
        return mainTitle;
    }

    public void setMainTitle(String mainTitle) {
        this.mainTitle = mainTitle;
    }

    public String getOtherTitle() {
        return otherTitle;
    }

    public void setOtherTitle(String otherTitle) {
        this.otherTitle = otherTitle;
    }

    public String geteISSN() {
        return eISSN;
    }

    public void seteISSN(String eISSN) {
        this.eISSN = eISSN;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getInsertToIC() {
        return insertToIC;
    }

    public void setInsertToIC(String insertToIC) {
        this.insertToIC = insertToIC;
    }

    public String getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(String publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getePublicationYear() {
        return ePublicationYear;
    }

    public void setePublicationYear(String ePublicationYear) {
        this.ePublicationYear = ePublicationYear;
    }

    public String getChiefEditor() {
        return chiefEditor;
    }

    public void setChiefEditor(String chiefEditor) {
        this.chiefEditor = chiefEditor;
    }

    public List<String> getFulltextLanguages() {
        return fulltextLanguages;
    }

    public void setFulltextLanguages(List<String> fulltextLanguages) {
        this.fulltextLanguages = fulltextLanguages;
    }

    public List<String> getAbstractLanguage() {
        return abstractLanguage;
    }

    public void setAbstractLanguage(List<String> abstractLanguage) {
        this.abstractLanguage = abstractLanguage;
    }

    public String getePublisher() {
        return ePublisher;
    }

    public void setePublisher(String ePublisher) {
        this.ePublisher = ePublisher;
    }

    public Map<String, String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(Map<String, String> descriptions) {
        this.descriptions = descriptions;
    }

    public String getIcValue() {
        return icValue;
    }

    public void setIcValue(String icValue) {
        this.icValue = icValue;
    }

    public String getIcCurrentStatus() {
        return icCurrentStatus;
    }

    public void setIcCurrentStatus(String icCurrentStatus) {
        this.icCurrentStatus = icCurrentStatus;
    }

    public List<String> getIcvHistory() {
        return icvHistory;
    }

    public void setIcvHistory(List<String> icvHistory) {
        this.icvHistory = icvHistory;
    }

    public List<String> getEditorialOfficeAddr() {
        return editorialOfficeAddr;
    }

    public void setEditorialOfficeAddr(List<String> editorialOfficeAddr) {
        this.editorialOfficeAddr = editorialOfficeAddr;
    }

    public List<String> getEditorInChief() {
        return editorInChief;
    }

    public void setEditorInChief(List<String> editorInChief) {
        this.editorInChief = editorInChief;
    }

    public List<String> getPublishedByElectronic() {
        return publishedByElectronic;
    }

    public void setPublishedByElectronic(List<String> publishedByElectronic) {
        this.publishedByElectronic = publishedByElectronic;
    }

    public String getCharacterOfInfo() {
        return characterOfInfo;
    }

    public void setCharacterOfInfo(String characterOfInfo) {
        this.characterOfInfo = characterOfInfo;
    }

    public String getIsiJournalMasterList() {
        return isiJournalMasterList;
    }

    public void setIsiJournalMasterList(String isiJournalMasterList) {
        this.isiJournalMasterList = isiJournalMasterList;
    }

    public String getScienceCitationIndexExpanded() {
        return scienceCitationIndexExpanded;
    }

    public void setScienceCitationIndexExpanded(String scienceCitationIndexExpanded) {
        this.scienceCitationIndexExpanded = scienceCitationIndexExpanded;
    }

    public String getBiobase() {
        return biobase;
    }

    public void setBiobase(String biobase) {
        this.biobase = biobase;
    }

    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    public String getErih() {
        return erih;
    }

    public void setErih(String erih) {
        this.erih = erih;
    }

    public String getScopus() {
        return scopus;
    }

    public void setScopus(String scopus) {
        this.scopus = scopus;
    }

    public String getCurrentContents() {
        return currentContents;
    }

    public void setCurrentContents(String currentContents) {
        this.currentContents = currentContents;
    }

    public String getIndexMedicus() {
        return indexMedicus;
    }

    public void setIndexMedicus(String indexMedicus) {
        this.indexMedicus = indexMedicus;
    }

    public String getExpertaMedica() {
        return expertaMedica;
    }

    public void setExpertaMedica(String expertaMedica) {
        this.expertaMedica = expertaMedica;
    }

    @Override
    public String toString() {
        return "Journal{" +
                "mainTitle='" + mainTitle + '\'' +
                ", otherTitle='" + otherTitle + '\'' +
                ", descriptions=" + descriptions +
                ", eISSN='" + eISSN + '\'' +
                ", url='" + url + '\'' +
                ", country='" + country + '\'' +
                ", frequency='" + frequency + '\'' +
                ", insertToIC='" + insertToIC + '\'' +
                ", publicationYear='" + publicationYear + '\'' +
                ", ePublicationYear='" + ePublicationYear + '\'' +
                ", chiefEditor='" + chiefEditor + '\'' +
                ", fulltextLanguages=" + fulltextLanguages +
                ", abstractLanguage=" + abstractLanguage +
                ", ePublisher='" + ePublisher + '\'' +
                ", icValue='" + icValue + '\'' +
                ", icCurrentStatus='" + icCurrentStatus + '\'' +
                ", icvHistory='" + icvHistory + '\'' +
                ", editorialOfficeAddr=" + editorialOfficeAddr +
                ", editorInChief=" + editorInChief +
                ", publishedByElectronic=" + publishedByElectronic +
                ", characterOfInfo='" + characterOfInfo + '\'' +
                ", isiJournalMasterList='" + isiJournalMasterList + '\'' +
                ", scienceCitationIndexExpanded='" + scienceCitationIndexExpanded + '\'' +
                ", biobase='" + biobase + '\'' +
                ", cas='" + cas + '\'' +
                ", erih='" + erih + '\'' +
                ", scopus='" + scopus + '\'' +
                ", currentContents='" + currentContents + '\'' +
                ", indexMedicus='" + indexMedicus + '\'' +
                ", expertaMedica='" + expertaMedica + '\'' +
                ", publishedBy='" + publishedBy + '\'' +
                ", publishedByFull=" + publishedByFull +
                ", executiveEditor='" + executiveEditor + '\'' +
                ", scientificDisciplines=" + scientificDisciplines +
                ", pISSN='" + pISSN + '\'' +
                ", affiliation='" + affiliation + '\'' +
                ", otherIndexes='" + otherIndexes + '\'' +
                ", abbreviationTitle='" + abbreviationTitle + '\'' +
                ", executiveEditorFull=" + executiveEditorFull +
                '}';
    }

    public static String tabDelimitedHeader(){
        StringBuilder strOfDesc = new StringBuilder();
        for (String s : descriptionLanguages) strOfDesc.append("Description:"+s+"   ");
        return "ID\t" + "Main Title\t" +
                "Main Title Language\t" +
                "Other Title\t" +
                printDescriptionsHeader() + "\t" +
                "eISSN\t" +
                "URL\t" +
                "Country\t" +
                "Frequency\t" +
                "Insert to IC\t" +
                "Year publication\t" +
                "Year publication electronic\t" +
                "Chief editor\t" +
                "Fulltext languages\t" +
                "Abstract languages\t" +
                "Published by electronic\t" +
                "IC Value\t" +
                "IC Current status\t" +
                "ICV History\t" +
                "Editorial office address\t" +
                "Chief editor [Full]\t" +
                "Published by electronic [Full]\t" +
                "Character of info\t" +
                "ISI Journal master list\t" +
                "Science citation index expanded\t" +
                "Biobase\t" +
                "CAS\t" +
                "ERIH\t" +
                "Scopus\t" +
                "Current contents\t" +
                "Index medicus\t" +
                "Experta medica\t" +
                "Published by\t" +
                "Published by [Full]\t" +
                "Executive editor\t" +
                "Scientific disciplines\t" +
                "pISSN\t" +
                "Affiliation\t" +
                "Other indexes\t" +
                "Abbreviation title\t" +
                "Executive editor [Full]";
    }

    private static String printDescriptionsHeader() {
        StringBuilder s = new StringBuilder();
        for (int i = 1; i <= Journal.maxNumberOfDescriptions; i++){
            s.append("Description_"+(i)+"\t");
            s.append("Description Language_"+(i)+"\t");
        }
        if (s.length()!=0) s.deleteCharAt(s.length()-1);
        return s.toString();
    }

    public String getMainTitleLanguage() {
        return mainTitleLanguage;
    }

    public void setMainTitleLanguage(String mainTitleLanguage) {
        this.mainTitleLanguage = mainTitleLanguage;
    }

    public String tabDelimitedFields(){
        return  id + '\t' +
                mainTitle + '\t' +
                mainTitleLanguage + '\t' +
                otherTitle + '\t' +
                printDescriptions() +
                eISSN + '\t' +
                url + '\t' +
                country + '\t' +
                frequency + '\t' +
                insertToIC + '\t' +
                publicationYear + '\t' +
                ePublicationYear + '\t' +
                chiefEditor + '\t' +
                printList(fulltextLanguages) + '\t' +
                printList(abstractLanguage) + '\t' +
                ePublisher + '\t' +
                icValue + '\t' +
                icCurrentStatus + '\t' +
                printList(icvHistory) + '\t' +
                printList(editorialOfficeAddr) + '\t' +
                printList(editorInChief) + '\t' +
                printList(publishedByElectronic) + '\t' +
                characterOfInfo + '\t' +
                isiJournalMasterList + '\t' +
                scienceCitationIndexExpanded + '\t' +
                biobase + '\t' +
                cas + '\t' +
                erih + '\t' +
                scopus + '\t' +
                currentContents + '\t' +
                indexMedicus + '\t' +
                expertaMedica + '\t' +
                publishedBy + '\t' +
                printList(publishedByFull) + '\t' +
                executiveEditor + '\t' +
                printList(scientificDisciplines) + '\t' +
                pISSN + '\t' +
                affiliation + '\t' +
                otherIndexes + '\t' +
                abbreviationTitle + '\t' +
                printList(executiveEditorFull);
    }

    private String printList(List<String> values) {
        StringBuilder result = new StringBuilder();
        for (String s : values) {
            result.append(s + "|");
        }
        if (result.length()!=0){
            result.deleteCharAt(result.length()-1);
        }
        return result.toString();
    }

    public String printDescriptions() {
        StringBuilder d = new StringBuilder();
        int counter = 0;
        for (Map.Entry<String,String> desc : descriptions.entrySet()){
            d.append(desc.getValue()+"\t");
            d.append(desc.getKey()+"\t");
            counter++;
        }
        for (int i = counter; i < Journal.maxNumberOfDescriptions; i++){
            d.append("\t\t");
        }
        return d.toString();
    }
}
