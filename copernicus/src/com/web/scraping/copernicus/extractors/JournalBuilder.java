package com.web.scraping.copernicus.extractors;

import com.web.scraping.copernicus.model.Journal;
import com.web.scraping.helpers.ScrapeDownloader;
import com.web.scraping.helpers.ScrapeHelper;
import static com.web.scraping.copernicus.extractors.JournalExtractor.*;

import org.w3c.dom.Document;

import java.util.Map;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/30/13 Time: 3:27 PM
 */
//TODO: May need to implement check for 'Access denied' response
public class JournalBuilder {
    private final int SLEEP_TIME = 15;

    private String link;
    private Document doc;

    public JournalBuilder(String link) {
        this.link = link;
        boolean downloaded = false;
        String html = "";
        while (!downloaded) {
            html = ScrapeDownloader.downloadLink(link);
            if (!dbError(html)) {
                downloaded = true;
            } else {
                System.out.println("[WARN]: Error downloading link " + link + " trying to wait " + SLEEP_TIME + " seconds and retry then.");
                try {
                    Thread.sleep(SLEEP_TIME * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        doc = ScrapeHelper.getDocument(html);
    }

    public static boolean dbError(String html) {
        return (html!=null && html.contains("Brak bazy"));
    }

    public Journal buildJournal() {
        Journal journal = new Journal();

        journal.setId(extractId(link));

        journal.setAbstractLanguage(getAbstractLanguage(doc));
        journal.setBiobase(getBiobase(doc));
        journal.setCas(getCAS(doc));
        journal.setCharacterOfInfo(getCharacterOfInfo(doc));
        journal.setChiefEditor(getChiefEditor(doc));
        journal.setCountry(getCountry(doc));
        journal.setCurrentContents(getCurrentContents(doc));

        Map<String,String> descriptions = getDescriptions(doc);
        journal.setDescriptions(descriptions);
        Journal.getDescriptionLanguages().addAll(descriptions.keySet());
        if (descriptions.size() > Journal.maxNumberOfDescriptions) Journal.maxNumberOfDescriptions = descriptions.size();

        journal.setEditorialOfficeAddr(getEditorialOfficeAddr(doc));
        journal.setEditorInChief(getEditorInChief(doc));
        journal.seteISSN(getEISSN(doc));
        journal.setePublicationYear(getEPublicationYear(doc));
        journal.setePublisher(getEPublisher(doc));
        journal.setErih(getERIH(doc));
        journal.setExpertaMedica(getExpertaMedica(doc));
        journal.setFrequency(getFrequency(doc));
        journal.setFulltextLanguages(getFulltextLanguages(doc));
        journal.setIcCurrentStatus(getICCurrentStatus(doc));
        journal.setIcValue(getICValue(doc));
        journal.setIcvHistory(getICVHistory(doc));
        journal.setIndexMedicus(getIndexMedicus(doc));
        journal.setInsertToIC(getInsertToIC(doc));
        journal.setIsiJournalMasterList(getIsiJournalMasterList(doc));
        journal.setMainTitle(getMainTitle(doc));
        journal.setMainTitleLanguage(getMainTitleLanguage(doc));
        journal.setOtherTitle(getOtherTitle(doc));
        journal.setPublicationYear(getPublicationYear(doc));
        journal.setPublishedByElectronic(getPublishedByElectronic(doc));
        journal.setScienceCitationIndexExpanded(getScienceCitationIndexExpanded(doc));
        journal.setScopus(getScopus(doc));
        journal.setUrl(getURL(doc));
        journal.setPublishedBy(getPublishedBy(doc));
        journal.setPublishedByFull(getPublishedByFull(doc));
        journal.setExecutiveEditor(getExecutiveEditor(doc));
        journal.setExecutiveEditorFull(getExecutiveEditorFull(doc));
        journal.setScientificDisciplines(getScientificDesciplines(doc));
        journal.setpISSN(getPISSN(doc));
        journal.setAffiliation(getAffiliation(doc));
        journal.setOtherIndexes(getOtherIndexes(doc));
        journal.setAbbreviationTitle(getAbbreviationTitle(doc));

        getFieldNames(doc);

        journal.setLinkAtCopernicus(link);

        return journal;
    }

}
