package com.web.scraping.copernicus.extractors;

import com.web.scraping.copernicus.Constants;
import com.web.scraping.helpers.FieldsAnalyzer;
import com.web.scraping.helpers.FieldsFrequencyAnalyzer;
import com.web.scraping.helpers.ScrapeHelper;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/21/13 Time: 8:29 PM
 */
//[ANSWERED] what should I do about really long fields (like description in ĮŽVALGOS)
    //::they should be pasted into resulting file "as is"
//TODO: what to do about cases like http://screencast.com/t/EsL1YGole0

/**
 * TODO: write test cases and check what's wrong with following journals
 * http://journals.indexcopernicus.com/passport.php?id=3291
 * http://journals.indexcopernicus.com/passport.php?id=9025
 * http://journals.indexcopernicus.com/passport.php?id=7804
 * http://journals.indexcopernicus.com/passport.php?id=4751
 * http://journals.indexcopernicus.com/passport.php?id=3741
 * http://journals.indexcopernicus.com/passport.php?id=3769
 * http://journals.indexcopernicus.com/passport.php?id=295
 */
public class JournalExtractor {
    public static String getMainTitle(Document doc){
        String result;
        result = ScrapeHelper.queryText("//TR/TD[1][contains(.,'Main title')]/../TD[2]", doc);
        return clean(result);
    }

    public static String getMainTitleLanguage(Document doc){
        String result;
        result = ScrapeHelper.queryText("//TR/TD[1][contains(.,'Main title')]", doc);
        Pattern languagePattern = Pattern.compile(".*\\[(.*)\\]");
        Matcher matcher = languagePattern.matcher(result);
        if (matcher.find()){
            result = matcher.group(1);
        } else {
            result = "";
        }
        return clean(result);
    }

    public static String getOtherTitle(Document doc){
        String result;
        result = ScrapeHelper.queryText("//TR/TD[1][contains(.,'Other title')]/../TD[2]", doc);
        return clean(result);
    }

    public static String getEISSN(Document doc){
        String result;
        result = ScrapeHelper.queryText("//TR/TD[1][contains(.,'e-ISSN')]/../TD[2]", doc);
        return clean(result);
    }

    public static String getURL(Document doc){
        String result;
        result = ScrapeHelper.queryText("//TR/TD[1][contains(.,'URL')]/../TD[2]/A/@href", doc);
        return clean(result);
    }

    public static String getCountry(Document doc){
        String result;
        result = ScrapeHelper.queryText("//TR/TD[1][contains(.,'Country')]/../TD[2]", doc);
        return clean(result);
    }

    public static String getFrequency(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Frequency')]/../TD[2]",doc));
    }

    public static String getInsertToIC(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Insert to IC')]/../TD[2]", doc));
    }

    public static String getPublicationYear(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Year publication') and not(contains(.,'Year publication electronic'))]/../TD[2]", doc));
    }

    public static String getEPublicationYear(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Year publication electronic')]/../TD[2]", doc));
    }

    public static String getChiefEditor(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Editor in Chief')]/../TD[2]", doc));
    }

    public static List<String> getFulltextLanguages(Document doc){
        return ScrapeHelper.queryTextValues("//TR/TD[1][contains(.,'Fulltext language')]/../TD[2]", doc);
    }

    public static List<String> getAbstractLanguage(Document doc){
        return ScrapeHelper.queryTextValues("//TR/TD[1][contains(.,'Abstract language')]/../TD[2]", doc);
    }

    public static String getEPublisher(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Published by electronic')]/../TD[2]", doc));
    }

    public static Map<String,String> getDescriptions(Document doc) {
        Map<String, String> result = new HashMap<String, String>();
        NodeList nList = ScrapeHelper.queryNodes("//TR/TD[1][contains(.,'Journal description')]/..", doc);
        Pattern languagePattern = Pattern.compile(".*\\[(.*)\\]");
        if (nList!=null) {
            for (int i = 0; i < nList.getLength(); i++) {
                String fieldName = ScrapeHelper.queryText("./TD[1]", nList.item(i));
                String fieldValue = ScrapeHelper.queryText("./TD[2]", nList.item(i));
                fieldValue = clean(fieldValue);
                Matcher matcher = languagePattern.matcher(fieldName);
                if (matcher.find()){
                    fieldName = matcher.group(1);
                }
                result.put(fieldName, fieldValue);
            }
        }
        return result;
    }

    //TODO: check. in test case it sounds like "IC Value 2011". can be multivaled field
    public static String getICValue(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'IC Value')]/../TD[2]", doc));
    }

    public static String getICCurrentStatus(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Current status')]/../TD[2]", doc));
    }

    public static List<String> getICVHistory(Document doc ){
        List<String> result = new LinkedList<String>();
        NodeList nList = ScrapeHelper.queryNodes("//TR/TD[1][contains(.,'ICV history')]/../TD[2]/text()", doc);
        if (nList!=null) {
            for (int i = 0; i < nList.getLength(); i++){
                String str = nList.item(i).getTextContent();
                str = clean(str);
                if (str!=null && !str.isEmpty()) {
                    String tmp = new String(str);
                    result.add(tmp);
                }
            }
        }
        return result;
    }

    public static List<String> getEditorialOfficeAddr(Document doc){
        List<String> result = new LinkedList<String>();
        NodeList nList = ScrapeHelper.queryNodes("//TR/TD[1][contains(.,'Editorial office address')]/../TD[2]/text()", doc);
        if (nList != null) {
            for (int i = 0; i < nList.getLength(); i++){
                String str = nList.item(i).getTextContent();
                str = clean(str);
                if (str!=null && !str.isEmpty()) result.add(str);
            }
        }
        return result;
    }

    public static List<String> getEditorInChief(Document doc) {
        List<String> result = new LinkedList<String>();
        NodeList nList = ScrapeHelper.queryNodes("//TR/TD[1][contains(.,'Editor in Chief')]/../TD[2]/text()", doc);
        String lastPartOfName = ScrapeHelper.queryText("//TR/TD[1][contains(.,'Editor in Chief')]/../TD[2]/FONT", doc);
        if (nList != null) {
            for (int i = 0; i < nList.getLength(); i++){
                String str = nList.item(i).getTextContent();
                if (i == 0 && lastPartOfName!=null) str+=" " + lastPartOfName;
                str = clean(str);
                if (str!=null && !str.isEmpty()) {
                    String tmp = new String(str);
                    result.add(tmp);
                }
            }
        }
        return result;
    }

    public static List<String> getPublishedByElectronic(Document doc) {
        List<String> result = new LinkedList<String>();
        NodeList nList = ScrapeHelper.queryNodes("//TR[contains(./TD,'Contacts and Addresses')]/following-sibling::TR/TD[contains(.,'Published by electronic')]/../TD[2]/text()", doc);
        if (nList!=null) {
            for (int i = 0; i < nList.getLength(); i++){
                String str = nList.item(i).getTextContent();
                str = clean(str);
                if (str!=null && !str.isEmpty()) {
                    String tmp = new String(str);
                    result.add(tmp);
                }
            }
        }
        return result;
    }

    public static String getCharacterOfInfo(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Character of the publications')]/../TD[2]/text()", doc));
    }

    public static List<String> getScientificDesciplines(Document doc) {
        List<String> tmp_result = new LinkedList<String>();
        NodeList nList = ScrapeHelper.queryNodes("//TR[contains(./TD,'Scientific profile')]/following-sibling::TR/TD[contains(.,'Scientific disciplines')]/../TD[2]/text()", doc);
        if (nList!=null) {
            for (int i = 0; i < nList.getLength(); i++){
                String str = nList.item(i).getTextContent();
                str = clean(str);
                if (str!=null && !str.isEmpty()) {
                    String tmp = new String(str);
                    tmp_result.add(tmp);
                }
            }
        }

        for (int i = 0; i < tmp_result.size(); i++) {
            if (((i+1)==tmp_result.size() && !tmp_result.get(i).startsWith("»")) ||
                    (!tmp_result.get(i).startsWith("»") && !tmp_result.get(i+1).startsWith("»")))
                tmp_result.add(++i, "»");
        }

        List<String> result = new LinkedList<String>();
        String section = "";
        String subSection = "";
        for (String s : tmp_result) {
            if (!s.startsWith("»")) {
                section = s;
            } else {
                result.add(section+":"+s.substring(1));
            }
        }

        return result;
    }

    public static String getIsiJournalMasterList(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'ISI Journal Master List')]/../TD[2]/text()", doc));
    }

    public static String getScienceCitationIndexExpanded(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Science Citation Index Expanded')]/../TD[2]/text()", doc));
    }

    public static String getBiobase(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'BIOBASE')]/../TD[2]/text()", doc));
    }

    public static String getCAS(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Chemical Abstracts CAS')]/../TD[2]/text()", doc));
    }

    public static String getERIH(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'ERIH')]/../TD[2]/text()", doc));
    }

    public static String getScopus(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Scopus')]/../TD[2]/text()", doc));
    }

    public static String getCurrentContents(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Current Contents')]/../TD[2]/text()", doc));
    }

    public static String getIndexMedicus(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Index Medicus/MEDLINE')]/../TD[2]/text()", doc));
    }

    public static String getExpertaMedica(Document doc){
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'EMBASE/Excerpta Medica')]/../TD[2]/text()", doc));
    }

    public static List<String> getFieldNames(Document doc){
        List<String> result = new LinkedList<String>();
        NodeList nList = ScrapeHelper.queryNodes("//TBODY/TR/TD[contains(.,'Main title')]/../../TR/TD[1]/text()", doc);
        if (nList != null) {
            for (int i=0; i < nList.getLength(); i++){
                String f = nList.item(i).getNodeValue();
                f = clean(f);
                f += ";";
                result.add(f);
            }
        }
        nList = ScrapeHelper.queryNodes("//TBODY/TR/TD[contains(.,'Main title')]/../../TR/TD[1]/B/text()", doc);
        if (nList != null) {
            for (int i=0; i < nList.getLength(); i++){
                String f = nList.item(i).getNodeValue();
                f = clean(f);
                f += ";";
                result.add(f);
            }
        }
        return result;
    }

    public static String getPublishedBy(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Published by') and not(contains(.,'Published by electronic'))]/../TD[2]/text()", doc));
    }


    public static List<String> getPublishedByFull(Document doc) {
        List<String> result = new LinkedList<String>();
        NodeList nList = ScrapeHelper.queryNodes("//TR[contains(./TD,'Contacts and Addresses')]/following-sibling::TR/TD[contains(.,'Published by') and not(contains(.,'Published by electronics'))]/../TD[2]/text()", doc);
        if (nList!=null) {
            for (int i = 0; i < nList.getLength(); i++){
                String str = nList.item(i).getTextContent();
                str = clean(str);
                if (str!=null && !str.isEmpty()) {
                    String tmp = new String(str);
                    result.add(tmp);
                }
            }
        }
        return result;
    }

    public static String getExecutiveEditor(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Executive Editor')]/../TD[2]", doc));
    }

    public static List<String> getExecutiveEditorFull(Document doc) {
        List<String> result = new LinkedList<String>();
        NodeList nList = ScrapeHelper.queryNodes("//TR[contains(./TD,'Contacts and Addresses')]/following-sibling::TR/TD[contains(.,'Executive Editor')]/../TD[2]/text()", doc);
        String lastPartOfName = ScrapeHelper.queryText("//TR[contains(./TD,'Contacts and Addresses')]/following-sibling::TR/TD[contains(.,'Executive Editor')]/../TD[2]/FONT", doc);
        if (nList!=null) {
            for (int i = 0; i < nList.getLength(); i++){
                String str = nList.item(i).getTextContent();
                if (i == 0 && lastPartOfName!=null) str+=" " + lastPartOfName;
                str = clean(str);
                if (str!=null && !str.isEmpty()) {
                    String tmp = new String(str);
                    result.add(tmp);
                }
            }
        }
        return result;
    }

    public static String getPISSN(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'p-ISSN')]/../TD[2]", doc));
    }

    public static String getAffiliation(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Affiliation')]/../TD[2]", doc));
    }

    public static String getOtherIndexes(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'other indexes')]/../TD[2]", doc));
    }

    public static String getAbbreviationTitle(Document doc) {
        return clean(ScrapeHelper.queryText("//TR/TD[1][contains(.,'Abbreviation title')]/../TD[2]", doc));
    }

    public static String clean(String s){
        return s.replaceAll("\r","").replaceAll("\n","").replaceAll("\t","").replaceAll("\u00a0","").trim();
    }

    public static String extractId(String s) {
        String result = "";
        Pattern idPattern = Pattern.compile("id=([0-9]+)");
        Matcher idMatcher = idPattern.matcher(s);
        if (idMatcher.find()){
            result = idMatcher.group(1);
        }
        return result;
    }
}
