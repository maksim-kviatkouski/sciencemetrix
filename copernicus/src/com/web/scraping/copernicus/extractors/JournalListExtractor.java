package com.web.scraping.copernicus.extractors;

import com.web.scraping.copernicus.Constants;
import com.web.scraping.copernicus.model.JournalListing;
import com.web.scraping.helpers.ScrapeDownloader;
import com.web.scraping.helpers.ScrapeHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.List;

/**
 * User: Maksim_Kviatkouski
 * Date: 3/20/13 Time: 7:17 PM
 */
public class JournalListExtractor {
    private static final int JOURNALS_IN_LIST = 9999;
    private static final String MASTER_LIST_LINK_TEMPLATE = "http://journals.indexcopernicus.com/masterlist.php?name=Master&litera=%s&start=0&skok=" + JOURNALS_IN_LIST;
    private static final String[] LETTERS_TO_CHECK = new String[] {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "W", "V", "Z", "Y"};

    public static List<String> extractListOfJournals(String url){
        List<String> result;
        Document doc = ScrapeHelper.getDocument(ScrapeDownloader.downloadLink(url));
        result = ScrapeHelper.queryValues("//A[contains(@href, 'action=masterlist')]/@href", doc);
        for (int i = 0; i < result.size(); i++){
            result.set(i, Constants.SITE_DOMAIN + "/passport.php?id=" + JournalExtractor.extractId(result.get(i)));
        }
        return result;
    }

    public static JournalListing getJournalListing(){
        JournalListing journalListing = new JournalListing();

        for (int i = 0; i < LETTERS_TO_CHECK.length; i++){
            String listingLink = String.format(MASTER_LIST_LINK_TEMPLATE, LETTERS_TO_CHECK[i]);
            journalListing.addLinks(extractListOfJournals(listingLink));
        }

        return journalListing;
    }

    public static void printNodeContent(Node node, int indent){
        //in case indent is 0 zero is treated by printf wrong then so it should be positive
        if (indent <= 0){
            indent = 2;
        }
        int length = node.getChildNodes().getLength();
        System.out.printf("%1$"+Integer.toString(indent)+"s %2$s", " ", node.getNodeName()+" : ");
        if (node.getAttributes() != null){
            int l = node.getAttributes().getLength();
            for (int i = 0; i < length; i++){
                if (node.getAttributes().item(i) == null) {
                    continue;
                }
                String name = node.getAttributes().item(i).getNodeName() != null ? node.getAttributes().item(i).getNodeName() : "";
                String val = node.getAttributes().item(i).getNodeValue() != null ? node.getAttributes().item(i).getNodeValue() : "";
                System.out.print(name + "=" + val + " ");
            }
            System.out.print(" | " + node.getTextContent().substring(0, Math.min(20, node.getTextContent().length())));
        }
        System.out.println();
        for (int i = 0; i < length; i++) {
            printNodeContent(node.getChildNodes().item(i), indent + 4);
        }
    }
}
