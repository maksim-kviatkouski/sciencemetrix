package com.web.scraping.copernicus;

/**
 * Created with IntelliJ IDEA.
 * User: Maksim_Kviatkouski
 * Date: 7/3/13
 * Time: 10:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class Constants {
    public static final String SITE_DOMAIN = "http://journals.indexcopernicus.com";
    public static final String SWITCH_TO_ENGLISH_PAGE = "/index.php?lang=en_US";
}
