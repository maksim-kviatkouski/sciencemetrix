import com.web.scraping.copernicus.extractors.JournalBuilder;
import com.web.scraping.copernicus.extractors.JournalListExtractor;
import com.web.scraping.copernicus.model.Journal;
import com.web.scraping.copernicus.model.JournalListing;
import com.web.scraping.helpers.ScrapeDownloader;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

//TODO: what to do with "abstracts" http://screencast.com/t/SwsNnkCTUYWq
//DONE: do UTF-8
public class Main {
    private static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static ExecutorService eService = Executors.newFixedThreadPool(24);

    public static void main(String[] args) throws InterruptedException, IOException {
        System.out.println("Set English language for site");
        ScrapeDownloader.setEnglishLanguage();
        System.out.println("Obtaining links to all journals");
        JournalListing jListing = JournalListExtractor.getJournalListing();
        System.out.println(df.format(Calendar.getInstance().getTime()) + " " + jListing.getLinks().size() + " links found");
        final List<Journal> journals = Collections.synchronizedList(new LinkedList<Journal>());
        try {
            OutputStreamWriter pw = new OutputStreamWriter(new FileOutputStream("copernicus_journals.txt"), "UTF-8");
            final AtomicInteger debug = new AtomicInteger(0);
            long start = Calendar.getInstance().getTimeInMillis();
            for (final String link : jListing.getLinks()) {
                //if (!"http://journals.indexcopernicus.com/passport.php?id=5943".equals(link)) continue;
                JournalBuilder jBuilder = new JournalBuilder(link);
                Journal j = jBuilder.buildJournal();
                journals.add(j);
                System.out.println("\r" + debug.incrementAndGet());
            }
            long dt = Calendar.getInstance().getTimeInMillis() - start;
            pw.write(Journal.tabDelimitedHeader() + "\r\n");
            System.out.println("Header printed");
            for (Journal j : journals) {
                System.out.println(j.toString());
                pw.write(j.tabDelimitedFields() + "\r\n");
            }
            pw.close();
            System.out.println("Processing took " + dt/1000 + " ms.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
